﻿using System;
using Newtonsoft.Json;

namespace EW.Navigator.SCM.RestfulGit.Entities
{
    public abstract class GitEntry : IEquatable<GitEntry>
    {
        protected GitEntry() { }

        [JsonProperty("url")]
        public virtual string Url { get; set; }

        /// <summary>
        /// sh1
        /// </summary>
        [JsonProperty("sha")]
        public virtual string Sha { get; set; }

        public bool Equals(GitEntry other)
        {
            if ((other == null) || GetType() != other.GetType())
            {
                return false;
            }
            else
            {
                return (Url == other.Url) && (Sha == other.Sha);
            }
        }

        public override int GetHashCode()
        {
            return (Sha.GetHashCode() << 2) ^ 2;
        }

        public override bool Equals(object obj) => Equals(obj as GitEntry);

        /*public static bool operator == (GitEntry left, GitEntry right)
        {
            if (left == null)
            {
                return right == null;
            }
            return left.Equals(right);
        }

        public static bool operator != (GitEntry left, GitEntry right) => !(left == right);*/

    }
}

