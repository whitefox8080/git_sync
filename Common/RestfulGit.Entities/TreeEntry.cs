﻿using System;

namespace EW.Navigator.SCM.RestfulGit.Entities
{
    public class TreeEntry : IEquatable<TreeEntry>
    {
        protected TreeEntry() { }

        public virtual GitEntry GitEntry { get; set; }
        public bool Equals(TreeEntry other)
        {
            if ((other == null) || GetType() != other.GetType())
            {
                return false;
            }
            else
            {
                return GitEntry.Equals(other.GitEntry);
            }
        }


        public override int GetHashCode()
        {
            return 0;
        }

        public override bool Equals(object obj) => Equals(obj as TreeEntry);

        public static bool operator ==(TreeEntry left, TreeEntry right)
        {
            if (left == null)
            {
                return right == null;
            }
            return left.Equals(right);
        }

        public static bool operator !=(TreeEntry left, TreeEntry right) => !(left == right);
    }
}
