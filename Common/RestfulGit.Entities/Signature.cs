﻿using System;
using Newtonsoft.Json;

namespace EW.Navigator.SCM.RestfulGit.Entities
{
    /// <summary>
    /// Committer identity in Git
    /// </summary>
    public sealed class Signature : IEquatable<Signature>
    {
        public Signature(string name, string email, DateTimeOffset when)
        {
            Name = name;
            Email = email;
            Date = when;
        }

        /// <summary>
        /// Date
        /// </summary>
        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        /// <summary>
        /// Committer name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Committer email
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        public bool Equals(Signature other)
        {
            if ((other == null) || GetType() != other.GetType())
            {
                return false;
            }
            else
            {
                return (Name == other.Name) && (Date == other.Date) && (Email == other.Email);
            }
        }

        public override bool Equals(object obj) => Equals(obj as Signature);

        public override int GetHashCode()
        {
            return ((int)Date.ToUnixTimeSeconds() << 2) ^ 2;
        }

        public static bool operator ==(Signature left, Signature right)
        {
            if (left == null)
            {
                return right == null;
            }
            return left.Equals(right);
        }

        public static bool operator !=(Signature left, Signature right) => !(left == right);
    }
}

