﻿
/* ----------------------------------------------------------------------------
   Этот файл содержит скрипт, который необходимо выполнить после 
   DistributionEntities.edmx для завершения развертывания базы USSDNavigator.

   ВНИМАНИЕ:
   Файл создан из-за отсутствия в EntityFramework некоторых нужных нам фишек.   
   Мы, вероятно, будем и дальше придерживаться этой схемы - обращайте внимание
   на файлы *.estension.sql рядом соответствующими файлами *.sql.
*/ ----------------------------------------------------------------------------

USE [NavigatorDistribution]
GO
BEGIN TRAN T1;


ALTER TABLE [dbo].[PayloadItemStates] DROP CONSTRAINT [FK_PayloadItemPayloadItemState]
GO


ALTER TABLE [dbo].[PayloadItemStates] DROP CONSTRAINT [PK_PayloadItemStates]
GO


/****** Object:  Index [PK_PayloadItemStates]    Script Date: 28.06.2016 17:39:25 ******/

ALTER TABLE [dbo].[PayloadItems] DROP CONSTRAINT [PK_PayloadItems]
GO


--DROP INDEX [IX_FK_TaskItem] ON [dbo].[PayloadItems]
--GO


ALTER TABLE [dbo].[PayloadItems] DROP CONSTRAINT [FK_TaskItem]
GO


if ( object_id( 'PayloadItems' ) is not null )
  drop table PayloadItems
--
go

if ( object_id( 'PayloadItemStates' ) is not null )
  drop table PayloadItemStates
--
go

-- создаем partition function и scheme
if exists ( select null from sys.partition_schemes where [name] = 'sc_partition_ranges' )
  drop partition scheme sc_partition_ranges
--
go

if exists ( select null from sys.partition_functions where [name] = 'fn_partition_ranges' )
  drop partition function fn_partition_ranges
--
go

create partition function fn_partition_ranges (datetime)
as range right for values ( cast( getdate() as date ) )
--
go

create partition scheme sc_partition_ranges
  as partition fn_partition_ranges
  all to ( [PRIMARY] )
 --
go

-- создаем триггер для автосоздания секций
create trigger tr_Tasks_on_insert on dbo.Tasks
  for insert
as

  declare @calldatetime datetime

  select @calldatetime = max( Created ) from inserted

  if not exists ( select null 
                  from sys.partition_functions pf with ( nolock )
                    left join sys.partition_range_values prv with ( nolock )
                      on pf.function_id = prv.function_id
                  where pf.name = 'fn_partition_ranges'
                    and prv.value > @calldatetime )
  begin
    alter partition function fn_partition_ranges()
      split range ( dateadd( dy, 1, cast( @calldatetime as date) ) )

    alter partition scheme sc_partition_ranges next used [PRIMARY]
  end
GO


-- Creating table 'PayloadItems'
CREATE TABLE [dbo].[PayloadItems] (
    [TaskId] uniqueidentifier  NOT NULL,
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [SubscriberNumber] nvarchar(15)  NOT NULL,
    [StrInArguments] nvarchar(max)  NULL,
    [SourceId] nvarchar(80)  NULL,
    [TaskCreated] datetime NOT NULL
) on sc_partition_ranges(TaskCreated);
GO


-- Creating table 'PayloadItemStates'
CREATE TABLE [dbo].[PayloadItemStates] (
    [TaskId] uniqueidentifier  NOT NULL,
    [PayloadItemId] bigint  NOT NULL,
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Created] datetime  NOT NULL,
    [Status] tinyint  NOT NULL,
    [BeginCallTime] datetime  NULL,
    [EndCallTime] datetime  NULL,
    [RealizedAttempts] int  NOT NULL,
    [StrOutArguments] nvarchar(max)  NULL,
    [ResultCode] int  NULL,
    [TaskCreated] datetime NOT NULL
) on sc_partition_ranges(TaskCreated);
GO


/****** Object:  Index [PK_PayloadItems]    Script Date: 28.06.2016 17:40:33 ******/
/*
create clustered index [PK_PayloadItems] on PayloadItems
(
	[TaskId] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON sc_partition_ranges(TaskCreated)
GO
*/

/****** Object:  Index [PK_PayloadItems]    Script Date: 28.06.2016 17:40:33 ******/
ALTER TABLE [dbo].[PayloadItems] ADD  CONSTRAINT [PK_PayloadItems] PRIMARY KEY CLUSTERED 
(
	[TaskCreated],
	[TaskId] ASC,
	[Id] ASC
) ON sc_partition_ranges(TaskCreated)
GO

/****** Object:  Index [PK_PayloadItemStates]    Script Date: 28.06.2016 17:39:25 ******/
/*
create clustered index [PK_PayloadItemStates] on PayloadItemStates
(
	[TaskId] ASC,
	[PayloadItemId] ASC,
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON sc_partition_ranges(TaskCreated)
GO
*/

/****** Object:  Index [PK_PayloadItems]    Script Date: 28.06.2016 17:40:33 ******/
ALTER TABLE [dbo].[PayloadItemStates] ADD  CONSTRAINT PK_PayloadItemStates PRIMARY KEY CLUSTERED 
(
	[TaskCreated],
	[TaskId] ASC,
	[PayloadItemId] ASC,
	[Id] DESC
) ON sc_partition_ranges(TaskCreated)
GO

/****** Object:  Index [IX_FK_PayloadItemPayloadItemState]    Script Date: 28.06.2016 17:39:18 ******/
/*
CREATE NONCLUSTERED INDEX [IX_FK_PayloadItemPayloadItemState] ON [dbo].[PayloadItemStates]
(
	[TaskId] ASC,
	[PayloadItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON sc_partition_ranges(TaskCreated)
GO
*/


ALTER TABLE [dbo].[PayloadItems]  WITH CHECK ADD  CONSTRAINT [FK_TaskItem] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Tasks] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PayloadItems] CHECK CONSTRAINT [FK_TaskItem]


ALTER TABLE [dbo].[PayloadItemStates]  WITH CHECK ADD  CONSTRAINT [FK_PayloadItemPayloadItemState] FOREIGN KEY([TaskCreated],[TaskId],[PayloadItemId])
REFERENCES [dbo].[PayloadItems] ([TaskCreated],[TaskId],[Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PayloadItemStates] CHECK CONSTRAINT [FK_PayloadItemPayloadItemState]
GO

DROP INDEX [IX_TaskPatterns_TemplateName] ON [dbo].[TaskPatterns]
GO
-- Создаем уникальный индекс по наименованию шаблона рассылки.
CREATE UNIQUE NONCLUSTERED INDEX [IX_TaskPatterns_TemplateName] ON [dbo].[TaskPatterns] ( [TemplateName] ASC )
GO

/****** Object:  Index [RuleNameUniqueIndex]    Script Date: 05.02.2016 19:08:30 ******/
DROP INDEX [RuleNameUniqueIndex] ON [dbo].[Rules]
GO

CREATE UNIQUE NONCLUSTERED INDEX [RuleNameUniqueIndex] ON [dbo].[Rules]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



--DROP PROCEDURE [dbo].[GetPayloadItemsPage]
--GO

--/****** Object:  StoredProcedure [dbo].[GetPayloadItemsPage]    Script Date: 05.02.2016 18:53:02 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


--CREATE PROCEDURE [dbo].[GetPayloadItemsPage] 
-- (
-- @SkipIndex int,
--  @TakeIndex int,
--  @TaskId uniqueidentifier,
--  @AttemptCount int,
--  @bar dateTime
--  )
--AS
--BEGIN
--	SELECT 
--[Project2].[Id] AS [Id], 
--[Project2].[Changed] AS [Changed], 
--[Project2].[TaskId] AS [TaskId], 
--[Project2].[SubscriberNumber] AS [SubscriberNumber], 
--[Project2].[Status] AS [Status],
--[Project2].[ResultCode] AS [ResultCode], 
--[Project2].[BeginCallTime] AS [BeginCallTime], 
--[Project2].[EndCallTime] AS [EndCallTime], 
--[Project2].[RealizedAttempts] AS [RealizedAttempts],
--[Project2].[StrInArguments] AS [StrInArguments],
--[Project2].[StrOutArguments] AS [StrOutArguments],
--[Project2].[SourceId] AS [SourceId]



--FROM ( SELECT 
--       [Limit1].[Id] AS [Id], 
--       [Limit1].[Changed] AS [Changed], 
--       [Limit1].[TaskId] AS [TaskId], 
--       [Limit1].[SubscriberNumber] AS [SubscriberNumber], 
--       [Limit1].[Status] AS [Status],
--       [Limit1].[ResultCode] AS [ResultCode],
--       [Limit1].[BeginCallTime] AS [BeginCallTime], 
--       [Limit1].[EndCallTime] AS [EndCallTime], 
--       [Limit1].[RealizedAttempts] AS [RealizedAttempts],
--	   [Limit1].[StrInArguments] AS [StrInArguments],
--	   [Limit1].[StrOutArguments] AS [StrOutArguments],
--	   [Limit1].[SourceId] AS [SourceId]
	   
--       FROM ( SELECT TOP (@TakeIndex) [Project1].[Id] AS [Id],
--	    [Project1].[Changed] AS [Changed], [Project1].[TaskId] AS [TaskId], 
--		[Project1].[SubscriberNumber] AS [SubscriberNumber], 
--		[Project1].[Status] AS [Status],
--	    [Project1].[ResultCode] AS [ResultCode],
--		[Project1].[BeginCallTime] AS [BeginCallTime],
--		[Project1].[EndCallTime] AS [EndCallTime],
--		[Project1].[RealizedAttempts] AS [RealizedAttempts],
--	    [Project1].[StrInArguments] AS [StrInArguments],
--		[Project1].[StrOutArguments] AS [StrOutArguments],

--	    [Project1].[SourceId] AS [SourceId]

--             FROM ( SELECT [Project1].[Id] AS [Id],
--			  [Project1].[Changed] AS [Changed], [Project1].[TaskId] AS [TaskId],
--			   [Project1].[SubscriberNumber] AS [SubscriberNumber],
--			   [Project1].[Status] AS [Status], 
--			   [Project1].[ResultCode] AS [ResultCode],
--				[Project1].[BeginCallTime] AS [BeginCallTime],
--			   [Project1].[EndCallTime] AS [EndCallTime], 
--			   [Project1].[RealizedAttempts] AS [RealizedAttempts],
--			    [Project1].[StrInArguments] AS [StrInArguments],
--			    [Project1].[StrOutArguments] AS [StrOutArguments],
--				 [Project1].[SourceId] AS [SourceId],
 
--  row_number() OVER (ORDER BY [Project1].[Id] ASC) AS [row_number]
--                    FROM ( SELECT 
--                          [Extent1].[Id] AS [Id], 
--                          [Extent1].[Changed] AS [Changed], 
--                          [Extent1].[TaskId] AS [TaskId], 
--                          [Extent1].[SubscriberNumber] AS [SubscriberNumber], 
--                          [Extent1].[Status] AS [Status], 
--						   [Extent1].[ResultCode] AS [ResultCode],
--                          [Extent1].[BeginCallTime] AS [BeginCallTime], 
--                          [Extent1].[EndCallTime] AS [EndCallTime], 
--                          [Extent1].[RealizedAttempts] AS [RealizedAttempts],
--						  [Extent1].[StrInArguments] AS [StrInArguments],
--						  [Extent1].[StrOutArguments] AS [StrOutArguments],
--						  [Extent1].[SourceId] AS [SourceId]
--                          FROM [dbo].[PayloadItems] AS [Extent1]
--                          WHERE [Extent1].[TaskId] = @TaskId
--                    )  AS [Project1]
--             )  AS [Project1]
--             WHERE [Project1].[row_number] > @SkipIndex
--                   )  AS [Limit1]
--       WHERE (5 <> [Limit1].[Status]) AND (1 <> [Limit1].[Status]) AND ([Limit1].[RealizedAttempts] < @AttemptCount) AND
--	    (([Limit1].[BeginCallTime] IS NULL) OR ([Limit1].[BeginCallTime] <= @bar))
--)  AS [Project2]

--END
--COMMIT TRAN T1; 

