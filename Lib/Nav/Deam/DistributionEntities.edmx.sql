
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/13/2017 17:44:18
-- Generated from EDMX file: C:\Projects\USSD Navigator\Development\Main\Servers\DEAMServer\Sources\EntityRepository\Schema.mssql\DistributionEntities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [NavigatorDistribution];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_TaskItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PayloadItems] DROP CONSTRAINT [FK_TaskItem];
GO
IF OBJECT_ID(N'[dbo].[FK_TaskTaskHistoryRecord]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TaskHistoryRecords] DROP CONSTRAINT [FK_TaskTaskHistoryRecord];
GO
IF OBJECT_ID(N'[dbo].[FK_TaskPatternPayloadItemPattern]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PayloadItemPatterns] DROP CONSTRAINT [FK_TaskPatternPayloadItemPattern];
GO
IF OBJECT_ID(N'[dbo].[FK_EventEventSubscription]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EventSubscriptions] DROP CONSTRAINT [FK_EventEventSubscription];
GO
IF OBJECT_ID(N'[dbo].[FK_EventSubscriptionTaskPattern]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EventSubscriptions] DROP CONSTRAINT [FK_EventSubscriptionTaskPattern];
GO
IF OBJECT_ID(N'[dbo].[FK_TaskPatternRule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TaskPatterns] DROP CONSTRAINT [FK_TaskPatternRule];
GO
IF OBJECT_ID(N'[dbo].[FK_FeasibleScheduleScheduleItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ScheduleItems] DROP CONSTRAINT [FK_FeasibleScheduleScheduleItem];
GO
IF OBJECT_ID(N'[dbo].[FK_RuleFeasibleSchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Rules] DROP CONSTRAINT [FK_RuleFeasibleSchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_RuleTask]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tasks] DROP CONSTRAINT [FK_RuleTask];
GO
IF OBJECT_ID(N'[dbo].[FK_PayloadItemPayloadItemState]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PayloadItemStates] DROP CONSTRAINT [FK_PayloadItemPayloadItemState];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Tasks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tasks];
GO
IF OBJECT_ID(N'[dbo].[Rules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rules];
GO
IF OBJECT_ID(N'[dbo].[PayloadItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PayloadItems];
GO
IF OBJECT_ID(N'[dbo].[TaskHistoryRecords]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TaskHistoryRecords];
GO
IF OBJECT_ID(N'[dbo].[TaskPatterns]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TaskPatterns];
GO
IF OBJECT_ID(N'[dbo].[PayloadItemPatterns]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PayloadItemPatterns];
GO
IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[EventSubscriptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EventSubscriptions];
GO
IF OBJECT_ID(N'[dbo].[ScheduleItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ScheduleItems];
GO
IF OBJECT_ID(N'[dbo].[FeasibleSchedules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FeasibleSchedules];
GO
IF OBJECT_ID(N'[dbo].[PayloadItemStates]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PayloadItemStates];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Tasks'
CREATE TABLE [dbo].[Tasks] (
    [Id] uniqueidentifier  NOT NULL,
    [Created] datetime  NOT NULL,
    [Changed] datetime  NULL,
    [Name] nvarchar(50)  NULL,
    [EntryPointId] uniqueidentifier  NOT NULL,
    [CallParameters] nvarchar(80)  NULL,
    [Progress] int  NOT NULL,
    [ItemCount] int  NOT NULL,
    [OwnerId] nvarchar(50)  NULL,
    [StrInArguments] nvarchar(max)  NULL,
    [SourceId] nvarchar(80)  NULL,
    [Status] tinyint  NOT NULL,
    [RuleId] uniqueidentifier  NULL,
    [LastStartTime] datetime  NULL,
    [LastStopTime] datetime  NULL,
    [NextStartTime] datetime  NULL,
    [RealizedAttempts] int  NOT NULL,
    [StartTime] datetime  NOT NULL,
    [StopTime] datetime  NOT NULL
);
GO

-- Creating table 'Rules'
CREATE TABLE [dbo].[Rules] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(256)  NULL,
    [Attempts] int  NOT NULL,
    [AttemptTime] time  NULL,
    [EntryPointId] uniqueidentifier  NULL,
    [BinaryFinalResultCodes] varbinary(max)  NULL,
    [ScheduleId] uniqueidentifier  NULL,
    [Created] datetime  NOT NULL,
    [Changed] datetime  NULL,
    [CallServiceNames] nvarchar(50)  NULL,
    [Priority] tinyint  NOT NULL,
    [Default] bit  NOT NULL,
    [DelayedConnect] bit  NOT NULL,
    [CprAttemptTime] time  NULL,
    [CallsPerTimeUnit] int  NULL,
    [TimeUnit] time  NULL,
    [BinaryAttemptRejectedResultCodes] varbinary(max)  NULL,
    [SessionTime] time  NULL,
    [RateMode] tinyint  NULL,
    [GroupName] nvarchar(50)  NULL
);
GO

-- Creating table 'PayloadItems'
CREATE TABLE [dbo].[PayloadItems] (
    [TaskId] uniqueidentifier  NOT NULL,
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [SubscriberNumber] nvarchar(15)  NOT NULL,
    [StrInArguments] nvarchar(max)  NULL,
    [SourceId] nvarchar(80)  NULL
);
GO

-- Creating table 'TaskHistoryRecords'
CREATE TABLE [dbo].[TaskHistoryRecords] (
    [Id] uniqueidentifier  NOT NULL,
    [Created] datetime  NOT NULL,
    [Status] tinyint  NOT NULL,
    [TaskId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'TaskPatterns'
CREATE TABLE [dbo].[TaskPatterns] (
    [Id] uniqueidentifier  NOT NULL,
    [Created] datetime  NOT NULL,
    [Changed] datetime  NULL,
    [TemplateName] nvarchar(50)  NOT NULL,
    [GroupName] nvarchar(50)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [EntryPointId] uniqueidentifier  NOT NULL,
    [CallParameters] nvarchar(max)  NULL,
    [StrInArguments] nvarchar(max)  NULL,
    [StartAfterTime] time  NULL,
    [DurationTime] time  NULL,
    [RuleId] uniqueidentifier  NULL
);
GO

-- Creating table 'PayloadItemPatterns'
CREATE TABLE [dbo].[PayloadItemPatterns] (
    [Id] uniqueidentifier  NOT NULL,
    [TaskPatternId] uniqueidentifier  NOT NULL,
    [SubscriberNumber] nvarchar(15)  NOT NULL,
    [StrInArguments] nvarchar(max)  NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [Id] uniqueidentifier  NOT NULL,
    [Created] datetime  NOT NULL,
    [Changed] datetime  NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(256)  NULL,
    [GroupName] nvarchar(50)  NOT NULL,
    [AllowRaise] bit  NOT NULL,
    [BufferSize] int  NULL,
    [BufferTime] time  NULL,
    [EnableBuffering] bit  NULL
);
GO

-- Creating table 'EventSubscriptions'
CREATE TABLE [dbo].[EventSubscriptions] (
    [Id] uniqueidentifier  NOT NULL,
    [Created] datetime  NOT NULL,
    [Changed] datetime  NULL,
    [EventId] uniqueidentifier  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [TaskPatternId] uniqueidentifier  NOT NULL,
    [Enable] bit  NOT NULL
);
GO

-- Creating table 'ScheduleItems'
CREATE TABLE [dbo].[ScheduleItems] (
    [Id] uniqueidentifier  NOT NULL,
    [Allow] bit  NOT NULL,
    [Enable] bit  NOT NULL,
    [DayNumber] smallint  NULL,
    [ScheduleId] uniqueidentifier  NULL,
    [FromTime] time  NOT NULL,
    [UntilTime] time  NOT NULL,
    [WeekNumber] smallint  NULL,
    [MonthNumber] smallint  NULL,
    [Type] tinyint  NOT NULL
);
GO

-- Creating table 'FeasibleSchedules'
CREATE TABLE [dbo].[FeasibleSchedules] (
    [Id] uniqueidentifier  NOT NULL,
    [IsGlobal] bit  NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(256)  NULL,
    [Created] datetime  NOT NULL,
    [Changed] datetime  NULL
);
GO

-- Creating table 'PayloadItemStates'
CREATE TABLE [dbo].[PayloadItemStates] (
    [TaskId] uniqueidentifier  NOT NULL,
    [PayloadItemId] bigint  NOT NULL,
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Created] datetime  NOT NULL,
    [Status] tinyint  NOT NULL,
    [BeginCallTime] datetime  NULL,
    [EndCallTime] datetime  NULL,
    [RealizedAttempts] int  NOT NULL,
    [StrOutArguments] nvarchar(max)  NULL,
    [ResultCode] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Tasks'
ALTER TABLE [dbo].[Tasks]
ADD CONSTRAINT [PK_Tasks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Rules'
ALTER TABLE [dbo].[Rules]
ADD CONSTRAINT [PK_Rules]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [TaskId], [Id] in table 'PayloadItems'
ALTER TABLE [dbo].[PayloadItems]
ADD CONSTRAINT [PK_PayloadItems]
    PRIMARY KEY CLUSTERED ([TaskId], [Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaskHistoryRecords'
ALTER TABLE [dbo].[TaskHistoryRecords]
ADD CONSTRAINT [PK_TaskHistoryRecords]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TaskPatterns'
ALTER TABLE [dbo].[TaskPatterns]
ADD CONSTRAINT [PK_TaskPatterns]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PayloadItemPatterns'
ALTER TABLE [dbo].[PayloadItemPatterns]
ADD CONSTRAINT [PK_PayloadItemPatterns]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EventSubscriptions'
ALTER TABLE [dbo].[EventSubscriptions]
ADD CONSTRAINT [PK_EventSubscriptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ScheduleItems'
ALTER TABLE [dbo].[ScheduleItems]
ADD CONSTRAINT [PK_ScheduleItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FeasibleSchedules'
ALTER TABLE [dbo].[FeasibleSchedules]
ADD CONSTRAINT [PK_FeasibleSchedules]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [TaskId], [PayloadItemId], [Id] in table 'PayloadItemStates'
ALTER TABLE [dbo].[PayloadItemStates]
ADD CONSTRAINT [PK_PayloadItemStates]
    PRIMARY KEY CLUSTERED ([TaskId], [PayloadItemId], [Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [TaskId] in table 'PayloadItems'
ALTER TABLE [dbo].[PayloadItems]
ADD CONSTRAINT [FK_TaskItem]
    FOREIGN KEY ([TaskId])
    REFERENCES [dbo].[Tasks]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TaskId] in table 'TaskHistoryRecords'
ALTER TABLE [dbo].[TaskHistoryRecords]
ADD CONSTRAINT [FK_TaskTaskHistoryRecord]
    FOREIGN KEY ([TaskId])
    REFERENCES [dbo].[Tasks]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskTaskHistoryRecord'
CREATE INDEX [IX_FK_TaskTaskHistoryRecord]
ON [dbo].[TaskHistoryRecords]
    ([TaskId]);
GO

-- Creating foreign key on [TaskPatternId] in table 'PayloadItemPatterns'
ALTER TABLE [dbo].[PayloadItemPatterns]
ADD CONSTRAINT [FK_TaskPatternPayloadItemPattern]
    FOREIGN KEY ([TaskPatternId])
    REFERENCES [dbo].[TaskPatterns]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskPatternPayloadItemPattern'
CREATE INDEX [IX_FK_TaskPatternPayloadItemPattern]
ON [dbo].[PayloadItemPatterns]
    ([TaskPatternId]);
GO

-- Creating foreign key on [EventId] in table 'EventSubscriptions'
ALTER TABLE [dbo].[EventSubscriptions]
ADD CONSTRAINT [FK_EventEventSubscription]
    FOREIGN KEY ([EventId])
    REFERENCES [dbo].[Events]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventEventSubscription'
CREATE INDEX [IX_FK_EventEventSubscription]
ON [dbo].[EventSubscriptions]
    ([EventId]);
GO

-- Creating foreign key on [TaskPatternId] in table 'EventSubscriptions'
ALTER TABLE [dbo].[EventSubscriptions]
ADD CONSTRAINT [FK_EventSubscriptionTaskPattern]
    FOREIGN KEY ([TaskPatternId])
    REFERENCES [dbo].[TaskPatterns]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EventSubscriptionTaskPattern'
CREATE INDEX [IX_FK_EventSubscriptionTaskPattern]
ON [dbo].[EventSubscriptions]
    ([TaskPatternId]);
GO

-- Creating foreign key on [RuleId] in table 'TaskPatterns'
ALTER TABLE [dbo].[TaskPatterns]
ADD CONSTRAINT [FK_TaskPatternRule]
    FOREIGN KEY ([RuleId])
    REFERENCES [dbo].[Rules]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TaskPatternRule'
CREATE INDEX [IX_FK_TaskPatternRule]
ON [dbo].[TaskPatterns]
    ([RuleId]);
GO

-- Creating foreign key on [ScheduleId] in table 'ScheduleItems'
ALTER TABLE [dbo].[ScheduleItems]
ADD CONSTRAINT [FK_FeasibleScheduleScheduleItem]
    FOREIGN KEY ([ScheduleId])
    REFERENCES [dbo].[FeasibleSchedules]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FeasibleScheduleScheduleItem'
CREATE INDEX [IX_FK_FeasibleScheduleScheduleItem]
ON [dbo].[ScheduleItems]
    ([ScheduleId]);
GO

-- Creating foreign key on [ScheduleId] in table 'Rules'
ALTER TABLE [dbo].[Rules]
ADD CONSTRAINT [FK_RuleFeasibleSchedule]
    FOREIGN KEY ([ScheduleId])
    REFERENCES [dbo].[FeasibleSchedules]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RuleFeasibleSchedule'
CREATE INDEX [IX_FK_RuleFeasibleSchedule]
ON [dbo].[Rules]
    ([ScheduleId]);
GO

-- Creating foreign key on [RuleId] in table 'Tasks'
ALTER TABLE [dbo].[Tasks]
ADD CONSTRAINT [FK_RuleTask]
    FOREIGN KEY ([RuleId])
    REFERENCES [dbo].[Rules]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RuleTask'
CREATE INDEX [IX_FK_RuleTask]
ON [dbo].[Tasks]
    ([RuleId]);
GO

-- Creating foreign key on [TaskId], [PayloadItemId] in table 'PayloadItemStates'
ALTER TABLE [dbo].[PayloadItemStates]
ADD CONSTRAINT [FK_PayloadItemPayloadItemState]
    FOREIGN KEY ([TaskId], [PayloadItemId])
    REFERENCES [dbo].[PayloadItems]
        ([TaskId], [Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------