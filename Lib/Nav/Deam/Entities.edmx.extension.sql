/* ----------------------------------------------------------------------------
   ���� ���� �������� ������, ������� ���������� ��������� ����� Entities.edmx 
   ��� ���������� ������������� ���� USSDNavigator.

   ��������:
   ���� ������ ��-�� ���������� � EntityFramework ��������� ������ ��� �����.   
   ��, ��������, ����� � ������ �������������� ���� ����� - ��������� ��������
   �� ����� *.estension.sql ����� ���������������� ������� *.sql.
*/ ----------------------------------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [USSDNavigator]
GO

-- ������� ���������� ������ ��� ����� ���������.
CREATE UNIQUE NONCLUSTERED INDEX [IX_EntryPoints_Name] ON [dbo].[EntryPoints] ( [Name] ASC )
GO

-- ������� ���������� ������ ��� ����� ���������.
CREATE UNIQUE NONCLUSTERED INDEX [IX_Categories_Name] ON [dbo].[Categories] ( [Name] ASC )
GO

-- ������� ���������� ������ �� �������� ������ � ���� ������� ��� ���������� ������.
CREATE UNIQUE NONCLUSTERED INDEX [IX_SN_StorageServiceType] ON [dbo].[ServiceCallNumbers] 
(
	[Number] ASC,
	[StorageServiceType] ASC
)
GO


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntryPointServiceCallNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[ServiceCallNumbers]'))
ALTER TABLE [dbo].[ServiceCallNumbers] DROP CONSTRAINT [FK_EntryPointServiceCallNumber]
GO

ALTER TABLE [dbo].[ServiceCallNumbers]  WITH CHECK ADD  CONSTRAINT [FK_EntryPointServiceCallNumber] FOREIGN KEY([EntryPointId])
REFERENCES [dbo].[EntryPoints] ([Id])
ON DELETE SET NULL
GO