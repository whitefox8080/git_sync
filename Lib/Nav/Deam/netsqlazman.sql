/****** Object:  ForeignKey [FK__aspnet_Me__Appli__3F115E1A]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__3F115E1A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__Appli__3F115E1A]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__2B0A656D]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [FK__aspnet_Us__Appli__2B0A656D]
GO
/****** Object:  ForeignKey [FK_ApplicationAttributes_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationAttributes_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationAttributesTable] DROP CONSTRAINT [FK_ApplicationAttributes_Applications]
GO
/****** Object:  ForeignKey [FK_ApplicationGroupMembers_ApplicationGroup]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationGroupMembers_ApplicationGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable] DROP CONSTRAINT [FK_ApplicationGroupMembers_ApplicationGroup]
GO
/****** Object:  ForeignKey [FK_ApplicationGroups_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationGroups_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupsTable] DROP CONSTRAINT [FK_ApplicationGroups_Applications]
GO
/****** Object:  ForeignKey [FK_ApplicationPermissions_ApplicationsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationPermissions_ApplicationsTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationPermissionsTable] DROP CONSTRAINT [FK_ApplicationPermissions_ApplicationsTable]
GO
/****** Object:  ForeignKey [FK_Applications_Stores]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationsTable] DROP CONSTRAINT [FK_Applications_Stores]
GO
/****** Object:  ForeignKey [FK_AuthorizationAttributes_Authorizations]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuthorizationAttributes_Authorizations]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationAttributesTable] DROP CONSTRAINT [FK_AuthorizationAttributes_Authorizations]
GO
/****** Object:  ForeignKey [FK_Authorizations_Items]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Authorizations_Items]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] DROP CONSTRAINT [FK_Authorizations_Items]
GO
/****** Object:  ForeignKey [FK_ItemAttributes_Items]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItemAttributes_Items]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemAttributesTable] DROP CONSTRAINT [FK_ItemAttributes_Items]
GO
/****** Object:  ForeignKey [FK_Items_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Items_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable] DROP CONSTRAINT [FK_Items_Applications]
GO
/****** Object:  ForeignKey [FK_Items_BizRules]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Items_BizRules]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable] DROP CONSTRAINT [FK_Items_BizRules]
GO
/****** Object:  ForeignKey [FK_StoreAttributes_Stores]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreAttributes_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreAttributesTable] DROP CONSTRAINT [FK_StoreAttributes_Stores]
GO
/****** Object:  ForeignKey [FK_StoreGroupMembers_StoreGroup]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreGroupMembers_StoreGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupMembersTable] DROP CONSTRAINT [FK_StoreGroupMembers_StoreGroup]
GO
/****** Object:  ForeignKey [FK_StoreGroups_Stores]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreGroups_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupsTable] DROP CONSTRAINT [FK_StoreGroups_Stores]
GO
/****** Object:  ForeignKey [FK_StorePermissions_StoresTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StorePermissions_StoresTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_StorePermissionsTable] DROP CONSTRAINT [FK_StorePermissions_StoresTable]
GO
/****** Object:  Check [CK_WhereDefinedNotValid]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedNotValid]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedNotValid]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable] DROP CONSTRAINT [CK_WhereDefinedNotValid]

END
GO
/****** Object:  Check [CK_ApplicationGroups_GroupType_Check]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupsTable] DROP CONSTRAINT [CK_ApplicationGroups_GroupType_Check]

END
GO
/****** Object:  Check [CK_ApplicationPermissions]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationPermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationPermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationPermissionsTable] DROP CONSTRAINT [CK_ApplicationPermissions]

END
GO
/****** Object:  Check [CK_AuthorizationTypeCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuthorizationTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuthorizationTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] DROP CONSTRAINT [CK_AuthorizationTypeCheck]

END
GO
/****** Object:  Check [CK_objectSidWhereDefinedCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_objectSidWhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_objectSidWhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] DROP CONSTRAINT [CK_objectSidWhereDefinedCheck]

END
GO
/****** Object:  Check [CK_ownerSidWhereDefined]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ownerSidWhereDefined]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ownerSidWhereDefined]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] DROP CONSTRAINT [CK_ownerSidWhereDefined]

END
GO
/****** Object:  Check [CK_ValidFromToCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ValidFromToCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ValidFromToCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] DROP CONSTRAINT [CK_ValidFromToCheck]

END
GO
/****** Object:  Check [CK_Items_ItemTypeCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Items_ItemTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Items_ItemTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable] DROP CONSTRAINT [CK_Items_ItemTypeCheck]

END
GO
/****** Object:  Check [CK_Log]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Log]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Log]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]'))
ALTER TABLE [dbo].[netsqlazman_LogTable] DROP CONSTRAINT [CK_Log]

END
GO
/****** Object:  Check [CK_Settings]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Settings]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_Settings]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Settings]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_Settings]'))
ALTER TABLE [dbo].[netsqlazman_Settings] DROP CONSTRAINT [CK_Settings]

END
GO
/****** Object:  Check [CK_WhereDefinedCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupMembersTable] DROP CONSTRAINT [CK_WhereDefinedCheck]

END
GO
/****** Object:  Check [CK_StoreGroups_GroupType_Check]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StoreGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StoreGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupsTable] DROP CONSTRAINT [CK_StoreGroups_GroupType_Check]

END
GO
/****** Object:  Check [CK_StorePermissions]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StorePermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StorePermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_StorePermissionsTable] DROP CONSTRAINT [CK_StorePermissions]

END
GO
/****** Object:  View [dbo].[netsqlazman_ApplicationAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesView]'))
DROP VIEW [dbo].[netsqlazman_ApplicationAttributesView]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributeUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationAttributeUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributeDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationAttributeDelete]
GO
/****** Object:  View [dbo].[netsqlazman_StoreAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesView]'))
DROP VIEW [dbo].[netsqlazman_StoreAttributesView]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributeUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreAttributeUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributeDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreAttributeDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributeInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreAttributeInsert]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StoreAttributes]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_StoreAttributes]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StorePermissionDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StorePermissionDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StorePermissionInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StorePermissionInsert]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StorePermissions]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_StorePermissions]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_RevokeApplicationAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_RevokeApplicationAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_RevokeApplicationAccess]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributeInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationAttributeInsert]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationAttributes]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_ApplicationAttributes]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationPermissionDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationPermissionDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationPermissionInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationPermissionInsert]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationPermissions]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_ApplicationPermissions]
GO
/****** Object:  View [dbo].[netsqlazman_ApplicationsView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsView]'))
DROP VIEW [dbo].[netsqlazman_ApplicationsView]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GrantApplicationAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GrantApplicationAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_GrantApplicationAccess]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationGroupInsert]
GO
/****** Object:  View [dbo].[netsqlazman_ApplicationGroupMembersView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersView]'))
DROP VIEW [dbo].[netsqlazman_ApplicationGroupMembersView]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BuildUserPermissionCache]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BuildUserPermissionCache]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_BuildUserPermissionCache]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_DirectCheckAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DirectCheckAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_DirectCheckAccess]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemInsert]
GO
/****** Object:  View [dbo].[netsqlazman_ItemsHierarchyView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyView]'))
DROP VIEW [dbo].[netsqlazman_ItemsHierarchyView]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GrantStoreAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GrantStoreAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_GrantStoreAccess]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BizRuleDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_BizRuleDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_RevokeStoreAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_RevokeStoreAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_RevokeStoreAccess]
GO
/****** Object:  View [dbo].[netsqlazman_StoreGroupMembersView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersView]'))
DROP VIEW [dbo].[netsqlazman_StoreGroupMembersView]
GO
/****** Object:  View [dbo].[netsqlazman_DatabaseUsers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DatabaseUsers]'))
DROP VIEW [dbo].[netsqlazman_DatabaseUsers]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationGroupDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupMemberDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMemberDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationGroupMemberDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupMemberInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMemberInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationGroupMemberInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupMemberUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMemberUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationGroupMemberUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ApplicationGroupUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributeDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_AuthorizationAttributeDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributeInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_AuthorizationAttributeInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributeUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_AuthorizationAttributeUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_AuthorizationDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_AuthorizationInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_AuthorizationUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BizRuleUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_BizRuleUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ClearBizRule]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ClearBizRule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ClearBizRule]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_CreateDelegate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CreateDelegate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_CreateDelegate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_DeleteDelegate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DeleteDelegate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_DeleteDelegate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributeDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemAttributeDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributeInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemAttributeInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributeUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemAttributeUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemsHierarchyDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemsHierarchyDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemsHierarchyInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemsHierarchyInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ItemUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ReloadBizRule]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ReloadBizRule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ReloadBizRule]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreGroupDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreGroupInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupMemberDelete]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMemberDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreGroupMemberDelete]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupMemberInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMemberInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreGroupMemberInsert]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupMemberUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMemberUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreGroupMemberUpdate]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupUpdate]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreGroupUpdate]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_CreateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_SetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_AnyDataInTables]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_CreateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_DeleteUser]
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ApplicationAttributesTable]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
DROP VIEW [dbo].[vw_aspnet_MembershipUsers]
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
DROP VIEW [dbo].[vw_aspnet_Users]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_StoreInsert]
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
DROP VIEW [dbo].[vw_aspnet_Applications]
GO
/****** Object:  Table [dbo].[netsqlazman_StoreAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_StoreAttributesTable]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BizRuleInsert]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_BizRuleInsert]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_SchemaVersions]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_helplogins]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_helplogins]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_helplogins]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_IAmAdmin]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_IAmAdmin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_IAmAdmin]
GO
/****** Object:  Table [dbo].[netsqlazman_Settings]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Settings]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_Settings]
GO
/****** Object:  Table [dbo].[netsqlazman_LogTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_LogTable]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_CheckAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CheckAccess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_CheckAccess]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_MergeAuthorizations]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_MergeAuthorizations]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_MergeAuthorizations]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_DBVersion]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DBVersion]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_DBVersion]
GO
/****** Object:  Table [dbo].[UsersDemo]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersDemo]') AND type in (N'U'))
DROP TABLE [dbo].[UsersDemo]
GO
/****** Object:  View [dbo].[netsqlazman_BizRuleView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleView]'))
DROP VIEW [dbo].[netsqlazman_BizRuleView]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_BizRules]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRules]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_BizRules]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_IsAMemberOfGroup]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_IsAMemberOfGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_IsAMemberOfGroup]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GetApplicationGroupSidMembers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetApplicationGroupSidMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_GetApplicationGroupSidMembers]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GetStoreGroupSidMembers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetStoreGroupSidMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_GetStoreGroupSidMembers]
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ExecuteLDAPQuery]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ExecuteLDAPQuery]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[netsqlazman_ExecuteLDAPQuery]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StoreGroupMembers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_StoreGroupMembers]
GO
/****** Object:  Table [dbo].[netsqlazman_StoreGroupMembersTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_StoreGroupMembersTable]
GO
/****** Object:  View [dbo].[netsqlazman_ItemAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesView]'))
DROP VIEW [dbo].[netsqlazman_ItemAttributesView]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ItemAttributes]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_ItemAttributes]
GO
/****** Object:  Table [dbo].[netsqlazman_ItemAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ItemAttributesTable]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ItemsHierarchy]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchy]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_ItemsHierarchy]
GO
/****** Object:  Table [dbo].[netsqlazman_ItemsHierarchyTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ItemsHierarchyTable]
GO
/****** Object:  View [dbo].[netsqlazman_AuthorizationAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesView]'))
DROP VIEW [dbo].[netsqlazman_AuthorizationAttributesView]
GO
/****** Object:  View [dbo].[netsqlazman_AuthorizationView]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationView]'))
DROP VIEW [dbo].[netsqlazman_AuthorizationView]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_GetNameFromSid]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetNameFromSid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_GetNameFromSid]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StoreGroups]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroups]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_StoreGroups]
GO
/****** Object:  Table [dbo].[netsqlazman_StoreGroupsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_StoreGroupsTable]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_GetDBUsers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetDBUsers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_GetDBUsers]
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Users]
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Membership]
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Applications]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Stores]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Stores]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_Stores]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_CheckStorePermissions]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CheckStorePermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_CheckStorePermissions]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_AuthorizationAttributes]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_AuthorizationAttributes]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Authorizations]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Authorizations]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_Authorizations]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Items]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Items]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_Items]
GO
/****** Object:  Table [dbo].[netsqlazman_AuthorizationAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_AuthorizationAttributesTable]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationGroupMembers]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_ApplicationGroupMembers]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationGroups]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroups]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_ApplicationGroups]
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationGroupsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ApplicationGroupsTable]
GO
/****** Object:  Table [dbo].[netsqlazman_AuthorizationsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_AuthorizationsTable]
GO
/****** Object:  Table [dbo].[netsqlazman_ItemsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ItemsTable]
GO
/****** Object:  Table [dbo].[netsqlazman_BizRulesTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRulesTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_BizRulesTable]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Applications]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_Applications]
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_CheckApplicationPermissions]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CheckApplicationPermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[netsqlazman_CheckApplicationPermissions]
GO
/****** Object:  Table [dbo].[netsqlazman_StorePermissionsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_StorePermissionsTable]
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationPermissionsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ApplicationPermissionsTable]
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ApplicationsTable]
GO
/****** Object:  Table [dbo].[netsqlazman_StoresTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoresTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_StoresTable]
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationGroupMembersTable]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]') AND type in (N'U'))
DROP TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable]
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__2645B050]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__2645B050]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ap__Appli__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] DROP CONSTRAINT [DF__aspnet_Ap__Appli__2645B050]
END


End
GO
/****** Object:  Default [DF__aspnet_Me__Passw__40F9A68C]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__40F9A68C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Me__Passw__40F9A68C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [DF__aspnet_Me__Passw__40F9A68C]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__UserI__2BFE89A6]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__2BFE89A6]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__UserI__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__UserI__2BFE89A6]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__2CF2ADDF]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__2CF2ADDF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__Mobil__2CF2ADDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__Mobil__2CF2ADDF]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__2DE6D218]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__2DE6D218]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__IsAno__2DE6D218]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__IsAno__2DE6D218]
END


End
GO
/****** Object:  Default [DF_Log_SqlIdentity]    Script Date: 12/04/2012 16:38:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Log_SqlIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Log_SqlIdentity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[netsqlazman_LogTable] DROP CONSTRAINT [DF_Log_SqlIdentity]
END


End
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_BasicAccess')
DROP SCHEMA [aspnet_Membership_BasicAccess]
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_FullAccess')
DROP SCHEMA [aspnet_Membership_FullAccess]
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_ReportingAccess')
DROP SCHEMA [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Schema [NetSqlAzMan_Administrators]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Administrators')
DROP SCHEMA [NetSqlAzMan_Administrators]
GO
/****** Object:  Schema [NetSqlAzMan_Managers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Managers')
DROP SCHEMA [NetSqlAzMan_Managers]
GO
/****** Object:  Schema [NetSqlAzMan_Readers]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Readers')
DROP SCHEMA [NetSqlAzMan_Readers]
GO
/****** Object:  Schema [NetSqlAzMan_Users]    Script Date: 12/04/2012 16:38:50 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Users')
DROP SCHEMA [NetSqlAzMan_Users]
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_BasicAccess]
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_FullAccess]
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Role [NetSqlAzMan_Administrators]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'NetSqlAzMan_Administrators'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Administrators' AND type = 'R')
DROP ROLE [NetSqlAzMan_Administrators]
GO
/****** Object:  Role [NetSqlAzMan_Managers]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'NetSqlAzMan_Managers'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Managers' AND type = 'R')
DROP ROLE [NetSqlAzMan_Managers]
GO
/****** Object:  Role [NetSqlAzMan_Readers]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'NetSqlAzMan_Readers'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Readers' AND type = 'R')
DROP ROLE [NetSqlAzMan_Readers]
GO
/****** Object:  Role [NetSqlAzMan_Users]    Script Date: 12/04/2012 16:38:50 ******/
DECLARE @RoleName sysname
set @RoleName = N'NetSqlAzMan_Users'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Users' AND type = 'R')
DROP ROLE [NetSqlAzMan_Users]
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [NetSqlAzMan_Administrators]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Administrators')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Administrators' AND type = 'R')
CREATE ROLE [NetSqlAzMan_Administrators] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [NetSqlAzMan_Managers]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Managers')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Managers' AND type = 'R')
CREATE ROLE [NetSqlAzMan_Managers] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [NetSqlAzMan_Readers]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Readers')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Readers' AND type = 'R')
CREATE ROLE [NetSqlAzMan_Readers] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [NetSqlAzMan_Users]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Users')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'NetSqlAzMan_Users' AND type = 'R')
CREATE ROLE [NetSqlAzMan_Users] AUTHORIZATION [dbo]

END
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]'
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]'
GO
/****** Object:  Schema [NetSqlAzMan_Administrators]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Administrators')
EXEC sys.sp_executesql N'CREATE SCHEMA [NetSqlAzMan_Administrators] AUTHORIZATION [NetSqlAzMan_Administrators]'
GO
/****** Object:  Schema [NetSqlAzMan_Managers]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Managers')
EXEC sys.sp_executesql N'CREATE SCHEMA [NetSqlAzMan_Managers] AUTHORIZATION [NetSqlAzMan_Managers]'
GO
/****** Object:  Schema [NetSqlAzMan_Readers]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Readers')
EXEC sys.sp_executesql N'CREATE SCHEMA [NetSqlAzMan_Readers] AUTHORIZATION [NetSqlAzMan_Readers]'
GO
/****** Object:  Schema [NetSqlAzMan_Users]    Script Date: 12/04/2012 16:38:50 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'NetSqlAzMan_Users')
EXEC sys.sp_executesql N'CREATE SCHEMA [NetSqlAzMan_Users] AUTHORIZATION [NetSqlAzMan_Users]'
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationGroupMembersTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable](
	[ApplicationGroupMemberId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationGroupId] [int] NOT NULL,
	[objectSid] [varbinary](85) NOT NULL,
	[WhereDefined] [tinyint] NOT NULL,
	[IsMember] [bit] NOT NULL,
 CONSTRAINT [PK_GroupMembers] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupMemberId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]') AND name = N'ApplicationGroupMembers_ApplicationGroupId_ObjectSid_IsMember_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [ApplicationGroupMembers_ApplicationGroupId_ObjectSid_IsMember_Unique_Index] ON [dbo].[netsqlazman_ApplicationGroupMembersTable] 
(
	[ApplicationGroupId] ASC,
	[objectSid] ASC,
	[IsMember] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]') AND name = N'IX_ApplicationGroupMembers')
CREATE NONCLUSTERED INDEX [IX_ApplicationGroupMembers] ON [dbo].[netsqlazman_ApplicationGroupMembersTable] 
(
	[ApplicationGroupId] ASC,
	[objectSid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[netsqlazman_StoresTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoresTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_StoresTable](
	[StoreId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Description] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoresTable]') AND name = N'Stores_Name_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [Stores_Name_Unique_Index] ON [dbo].[netsqlazman_StoresTable] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [dbo].[netsqlazman_StoresTable] ON
INSERT [dbo].[netsqlazman_StoresTable] ([StoreId], [Name], [Description]) VALUES (1, N'AuthStore', N'USSD Navigator Security Store')
SET IDENTITY_INSERT [dbo].[netsqlazman_StoresTable] OFF
/****** Object:  Table [dbo].[netsqlazman_ApplicationsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ApplicationsTable](
	[ApplicationId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Name] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Description] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]') AND name = N'Applications_StoreId_Name_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [Applications_StoreId_Name_Unique_Index] ON [dbo].[netsqlazman_ApplicationsTable] 
(
	[Name] ASC,
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]') AND name = N'IX_Applications')
CREATE NONCLUSTERED INDEX [IX_Applications] ON [dbo].[netsqlazman_ApplicationsTable] 
(
	[ApplicationId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [dbo].[netsqlazman_ApplicationsTable] ON
INSERT [dbo].[netsqlazman_ApplicationsTable] ([ApplicationId], [StoreId], [Name], [Description]) VALUES (1, 1, N'USSDNavigator', N'')
SET IDENTITY_INSERT [dbo].[netsqlazman_ApplicationsTable] OFF
/****** Object:  Table [dbo].[netsqlazman_ApplicationPermissionsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ApplicationPermissionsTable](
	[ApplicationPermissionId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[SqlUserOrRole] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[IsSqlRole] [bit] NOT NULL,
	[NetSqlAzManFixedServerRole] [tinyint] NOT NULL,
 CONSTRAINT [PK_ApplicationPermissions] PRIMARY KEY CLUSTERED 
(
	[ApplicationPermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]') AND name = N'IX_ApplicationPermissions')
CREATE NONCLUSTERED INDEX [IX_ApplicationPermissions] ON [dbo].[netsqlazman_ApplicationPermissionsTable] 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]') AND name = N'IX_ApplicationPermissions_1')
CREATE NONCLUSTERED INDEX [IX_ApplicationPermissions_1] ON [dbo].[netsqlazman_ApplicationPermissionsTable] 
(
	[ApplicationId] ASC,
	[SqlUserOrRole] ASC,
	[NetSqlAzManFixedServerRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[netsqlazman_StorePermissionsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_StorePermissionsTable](
	[StorePermissionId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[SqlUserOrRole] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[IsSqlRole] [bit] NOT NULL,
	[NetSqlAzManFixedServerRole] [tinyint] NOT NULL,
 CONSTRAINT [PK_StorePermissions] PRIMARY KEY CLUSTERED 
(
	[StorePermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]') AND name = N'IX_StorePermissions')
CREATE NONCLUSTERED INDEX [IX_StorePermissions] ON [dbo].[netsqlazman_StorePermissionsTable] 
(
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]') AND name = N'IX_StorePermissions_1')
CREATE NONCLUSTERED INDEX [IX_StorePermissions_1] ON [dbo].[netsqlazman_StorePermissionsTable] 
(
	[StoreId] ASC,
	[SqlUserOrRole] ASC,
	[NetSqlAzManFixedServerRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_CheckApplicationPermissions]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CheckApplicationPermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/* 
   @ROLEID = { 0 READERS, 1 USERS, 2 MANAGERS}
*/
CREATE FUNCTION [dbo].[netsqlazman_CheckApplicationPermissions](@ApplicationId int, @ROLEID tinyint)
RETURNS bit
AS
BEGIN
DECLARE @RESULT bit
IF @ApplicationId IS NULL OR @ROLEID IS NULL
	SET @RESULT = 0	
ELSE
BEGIN
	IF EXISTS (
		SELECT     dbo.[netsqlazman_ApplicationPermissionsTable].ApplicationId
		FROM         dbo.[netsqlazman_ApplicationsTable] INNER JOIN
		                      dbo.[netsqlazman_StoresTable] ON dbo.[netsqlazman_ApplicationsTable].StoreId = dbo.[netsqlazman_StoresTable].StoreId LEFT OUTER JOIN
		                      dbo.[netsqlazman_StorePermissionsTable] ON dbo.[netsqlazman_StoresTable].StoreId = dbo.[netsqlazman_StorePermissionsTable].StoreId LEFT OUTER JOIN
		                      dbo.[netsqlazman_ApplicationPermissionsTable] ON dbo.[netsqlazman_ApplicationsTable].ApplicationId = dbo.[netsqlazman_ApplicationPermissionsTable].ApplicationId
		WHERE
		IS_MEMBER(''db_owner'')=1 OR IS_MEMBER(''NetSqlAzMan_Administrators'')=1 OR 
		(@ROLEID = 0 AND IS_MEMBER(''NetSqlAzMan_Readers'')=1 OR 
		@ROLEID = 1 AND IS_MEMBER(''NetSqlAzMan_Users'')=1 OR 
		@ROLEID = 2 AND IS_MEMBER(''NetSqlAzMan_Managers'')=1) AND
		(
		(dbo.[netsqlazman_ApplicationPermissionsTable].ApplicationId = @ApplicationId AND dbo.[netsqlazman_ApplicationPermissionsTable].NetSqlAzManFixedServerRole >= @ROLEID AND 
		(SUSER_SNAME(SUSER_SID())=[netsqlazman_ApplicationPermissionsTable].SqlUserOrRole AND [netsqlazman_ApplicationPermissionsTable].IsSqlRole = 0
		OR IS_MEMBER([netsqlazman_ApplicationPermissionsTable].SqlUserOrRole)=1 AND [netsqlazman_ApplicationPermissionsTable].IsSqlRole = 1)) OR
	
		dbo.[netsqlazman_ApplicationsTable].ApplicationId = @ApplicationId AND 
		(dbo.[netsqlazman_StorePermissionsTable].StoreId = dbo.[netsqlazman_ApplicationsTable].StoreId AND dbo.[netsqlazman_StorePermissionsTable].NetSqlAzManFixedServerRole >= @ROLEID AND 
		(SUSER_SNAME(SUSER_SID())=[netsqlazman_StorePermissionsTable].SqlUserOrRole AND [netsqlazman_StorePermissionsTable].IsSqlRole = 0 OR
		IS_MEMBER([netsqlazman_StorePermissionsTable].SqlUserOrRole)=1 AND [netsqlazman_StorePermissionsTable].IsSqlRole = 1))

))
	
	SET @RESULT = 1
	ELSE
	SET @RESULT = 0
END
RETURN @RESULT
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Applications]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Applications]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_Applications] ()
RETURNS TABLE
AS
RETURN
	SELECT * FROM dbo.[netsqlazman_ApplicationsTable]
	WHERE dbo.[netsqlazman_CheckApplicationPermissions](ApplicationId, 0) = 1
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_BizRulesTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRulesTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_BizRulesTable](
	[BizRuleId] [int] IDENTITY(1,1) NOT NULL,
	[BizRuleSource] [text] COLLATE Cyrillic_General_CI_AS NOT NULL,
	[BizRuleLanguage] [tinyint] NOT NULL,
	[CompiledAssembly] [image] NOT NULL,
 CONSTRAINT [PK_BizRules] PRIMARY KEY CLUSTERED 
(
	[BizRuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[netsqlazman_ItemsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ItemsTable](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Name] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Description] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[ItemType] [tinyint] NOT NULL,
	[BizRuleId] [int] NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]') AND name = N'Items_ApplicationId_Name_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [Items_ApplicationId_Name_Unique_Index] ON [dbo].[netsqlazman_ItemsTable] 
(
	[Name] ASC,
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]') AND name = N'IX_Items')
CREATE NONCLUSTERED INDEX [IX_Items] ON [dbo].[netsqlazman_ItemsTable] 
(
	[ApplicationId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [dbo].[netsqlazman_ItemsTable] ON
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (243, 1, N'AddDistributionItemsRawData', N'Загрузка элементов задачи рассылки в виде текстового файла в память сервера', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (244, 1, N'Administrator', N'Администратор', 0, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (245, 1, N'CallDispatcherConnectAs', N'Соединение с сервисом эмуляции вызовов', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (246, 1, N'CallEmulationService', N'Доступ к сервису эмуляции вызовов', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (248, 1, N'Content Provider', N'Контент провайдер', 0, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (249, 1, N'ControlCPRServers', N'Управление CPR-серверами', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (250, 1, N'CPRContinueReceivingCalls', N'Возобновление приема вызовов CPR-сервером', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (251, 1, N'CPRGetActiveCPRApplicationInfos', N'Получение информации об активных CPR-приложениях', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (252, 1, N'CPRGetCountProperties', N'Получение кол-ва свойств CPR', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (253, 1, N'CPRGetCPRApplicationInfos', N'Получение информации о CPR-приложениях', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (254, 1, N'CPRGetProperties', N'Получение свойств CPR', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (255, 1, N'CPRGetStatus', N'Получение статуса CPR', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (256, 1, N'CPRPauseReceivingCalls', N'Остановка приема вызовов CPR-сервером', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (257, 1, N'CPRRegister', N'Зарегистрировать CPR-сервер', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (258, 1, N'CPRService', N'CPR-служба', 0, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (259, 1, N'CPRSetProperties', N'Установка свойств CPR', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (260, 1, N'CPRStart', N'Запуск CPR-сервера', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (261, 1, N'CPRStop', N'Остановка CPR-сервера', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (262, 1, N'CPRUpdate', N'Обновление списка CPR-серверов', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (263, 1, N'CreateEntryPoints', N'Создание своих программ (точек входа)', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (265, 1, N'CreateUpdateAndDeleteServiceCallNumbers', N'Создание, измение или удаление сервисных номеров', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (267, 1, N'Distribution Manager', N'Менеджер рассылок', 0, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (268, 1, N'EditCPRServers', N'Редактирование списка CPR-серверов', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (270, 1, N'EditEntryPoint', N'Редактирование программы', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (271, 1, N'EditOwnEntryPoints', N'Управление своими программами (точками входа)', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (272, 1, N'EditOwnProjects', N'Управление сообственными проектами', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (273, 1, N'EditServiceCallNumber', N'Редактирование сервисного номера', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (274, 1, N'EditServiceCallNumbers', N'Редактирование сервисных номеров', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (275, 1, N'ExecuteCallEmulatorModule', N'Запуск модуля эмуляции вызовов', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (276, 1, N'ExecuteDistributionManagementModule', N'Просмотр списка управления рассылками', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (277, 1, N'ExecuteEntryPointManagementModule', N'Запуск модуля управления программами', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (278, 1, N'ExecuteReportingModule', N'Запуск модуля отчетности', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (279, 1, N'ExecuteSystemManagementModule', N'Управление системой', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (280, 1, N'GetAllDistributionTasks', N'Получение всех рассылок', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (281, 1, N'GetAllEntryPoints', N'Получение всех программ с включениями', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (283, 1, N'GetDistributionStrategies', N'Получение списка стратегий рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (284, 1, N'GetDistributionTasks', N'Получение списка задач рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (285, 1, N'GetServiceCallNumbers', N'Получение списка сервисных номеров', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (286, 1, N'Operator', N'Оператор', 0, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (287, 1, N'ProjectServiceDeleteAnyProjectRevisions', N'Удаление ревизий любого проекта', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (288, 1, N'ProjectServiceDeleteOwnProjectsRevisions', N'Удаление ревизий своего проекта', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (289, 1, N'ProjectServiceGetActivedRevisionsByEntryPointIds', N'Получение активных ревизий по идентификатору программы (точки входа)', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (290, 1, N'ProjectServiceGetChangeSets', N'Получение наборов изменений', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (291, 1, N'ProjectServiceGetLastestRevisionByEntryPointId', N'Получение последней ревизии по идентификатору программы (точки входа)', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (292, 1, N'ProjectServiceGetProjectsByRevisionIds', N'Получение проектов по идентификаторам ревизий', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (293, 1, N'ProjectServicePublishProjects', N'Публикация проектов', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (294, 1, N'ProjectServiceSetAnyProjectRevisionStatus', N'Установка статуса ревизии любого проекта', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (295, 1, N'ProjectServiceSetOwnProjectsRevisionStatus', N'Установка статуса ревизии своего проекта', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (296, 1, N'Supervisor', N'Супервизор', 0, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (297, 1, N'UpdateAndDeleteAnyEntryPoint', N'Создание, измение или удаление любых программ (точек входа)', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (298, 1, N'UpdateAndDeleteOwnEntryPoints', N'Изменение и удаление своих программ (точек входа)', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (299, 1, N'UserServiceGetTopMostRoleDescriptions', N'Получение описаний ролей пользователя', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (300, 1, N'UserServiceGetUserProfile', N'Получение профиля пользователя', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (301, 1, N'UserServiceGetUsers', N'Получение списка пользователей', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (302, 1, N'UserServiceGetUsersBySIds', N'Получение списка пользователей по их системным идентификаторам', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (303, 1, N'ViewAllUsers', N'Пользователь может получать информацию обо всех пользователях', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (304, 1, N'ViewCPRServers', N'Просмотр списка CPR-серверов', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (305, 1, N'ViewAllDistributionTasks', N'Просмотр всех USSD-рассылок', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (306, 1, N'ViewEntryPointList', N'Просмотр списка программ', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (307, 1, N'ViewAllEntryPoints', N'Просмотр программ (точек входа)', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (308, 1, N'ViewAllProjects', N'Просмотр проктов', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (309, 1, N'ViewReports', N'Запуск модуля отчетности', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (310, 1, N'ViewServiceCallNumberList', N'Просмотр списка программ', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (311, 1, N'ViewServiceCallNumbers', N'Просмотр сервисных номеров', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (312, 1, N'ViewUserOwnInfo', N'Пользователь может получать информацию о себе (необходимое условие для работы в системе)', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (313, 1, N'EditAllDistributionTasks', N'Редактирование USSD-рассылок', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (314, 1, N'EditAllEntryPoints', N'Редактирование программ (точек входа)', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (315, 1, N'EditAllProjects', N'Редактирование проктов', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (316, 1, N'AddDistributionItemsToAnyTask', N'Добавление элементов рассылки в любую задачу рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (317, 1, N'AddDistributionItemsToOwnTasks', N'Добавление элементов рассылки в собственные задачи рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (318, 1, N'CommitDistributionItemsRawDataToAnyTask', N'Сохранение загруженных в память сервера элементов в любую задачу рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (319, 1, N'CommitDistributionItemsRawDataToOwnTasks', N'Сохранение загруженных в память сервера элементов в собственные задачи рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (320, 1, N'CreateDistributionTasks', N'Создание задач рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (321, 1, N'DeleteAnyDistributionItems', N'Удаление элементов любой рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (322, 1, N'DeleteOwnDistributionItems', N'Удаление элементов собственных рассылок', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (323, 1, N'EditAnyDistributionItems', N'Редактирование элементов любой рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (324, 1, N'EditOwnDistributionItems', N'Редактирование элементов собственных рассылок', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (325, 1, N'EditOwnDistributionTasks', N'Изменение собственных задач рассылки', 1, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (326, 1, N'GetDistributionItemsByAnyTask', N'Получение списка элементов любой задачи рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (327, 1, N'GetDistributionItemsByOwnTasks', N'Получение списка элементов собственных задач рассылок', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (328, 1, N'UpdateAndDeleteAnyDistributionTask', N'Измение или удаление любой задачи рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (329, 1, N'UpdateAndDeleteOwnDistributionTasks', N'Измение или удаление собственных задач рассылки', 2, NULL)
INSERT [dbo].[netsqlazman_ItemsTable] ([ItemId], [ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (330, 1, N'ViewOwnDistributionTasks', N'Просмотр своих задач рассылки', 1, NULL)
SET IDENTITY_INSERT [dbo].[netsqlazman_ItemsTable] OFF
/****** Object:  Table [dbo].[netsqlazman_AuthorizationsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_AuthorizationsTable](
	[AuthorizationId] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NOT NULL,
	[ownerSid] [varbinary](85) NOT NULL,
	[ownerSidWhereDefined] [tinyint] NOT NULL,
	[objectSid] [varbinary](85) NOT NULL,
	[objectSidWhereDefined] [tinyint] NOT NULL,
	[AuthorizationType] [tinyint] NOT NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
 CONSTRAINT [PK_Authorizations] PRIMARY KEY CLUSTERED 
(
	[AuthorizationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]') AND name = N'IX_Authorizations')
CREATE NONCLUSTERED INDEX [IX_Authorizations] ON [dbo].[netsqlazman_AuthorizationsTable] 
(
	[ItemId] ASC,
	[objectSid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]') AND name = N'IX_Authorizations_1')
CREATE NONCLUSTERED INDEX [IX_Authorizations_1] ON [dbo].[netsqlazman_AuthorizationsTable] 
(
	[ItemId] ASC,
	[objectSid] ASC,
	[objectSidWhereDefined] ASC,
	[AuthorizationType] ASC,
	[ValidFrom] ASC,
	[ValidTo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationGroupsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ApplicationGroupsTable](
	[ApplicationGroupId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[objectSid] [varbinary](85) NOT NULL,
	[Name] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Description] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[LDapQuery] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NULL,
	[GroupType] [tinyint] NOT NULL,
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]') AND name = N'ApplicationGroups_ApplicationId_Name_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [ApplicationGroups_ApplicationId_Name_Unique_Index] ON [dbo].[netsqlazman_ApplicationGroupsTable] 
(
	[ApplicationId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]') AND name = N'IX_ApplicationGroups')
CREATE NONCLUSTERED INDEX [IX_ApplicationGroups] ON [dbo].[netsqlazman_ApplicationGroupsTable] 
(
	[ApplicationId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]') AND name = N'IX_ApplicationGroups_1')
CREATE NONCLUSTERED INDEX [IX_ApplicationGroups_1] ON [dbo].[netsqlazman_ApplicationGroupsTable] 
(
	[objectSid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [dbo].[netsqlazman_ApplicationGroupsTable] ON
INSERT [dbo].[netsqlazman_ApplicationGroupsTable] ([ApplicationGroupId], [ApplicationId], [objectSid], [Name], [Description], [LDapQuery], [GroupType]) VALUES (2, 1, 0xD43F7ED1D34B554485881E3B3DFA9931, N'Main', N'', N'', 0)
SET IDENTITY_INSERT [dbo].[netsqlazman_ApplicationGroupsTable] OFF
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationGroups]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroups]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_ApplicationGroups] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_ApplicationGroupsTable].*
	FROM         dbo.[netsqlazman_ApplicationGroupsTable] INNER JOIN
	                      dbo.[netsqlazman_Applications]() Applications ON dbo.[netsqlazman_ApplicationGroupsTable].ApplicationId = Applications.ApplicationId
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationGroupMembers]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_ApplicationGroupMembers] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_ApplicationGroupMembersTable].*
	FROM         dbo.[netsqlazman_ApplicationGroups]() ApplicationGroups INNER JOIN
	                      dbo.[netsqlazman_ApplicationGroupMembersTable] ON ApplicationGroups.ApplicationGroupId = dbo.[netsqlazman_ApplicationGroupMembersTable].ApplicationGroupId
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_AuthorizationAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_AuthorizationAttributesTable](
	[AuthorizationAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[AuthorizationId] [int] NOT NULL,
	[AttributeKey] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[AttributeValue] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_AuthorizationAttributes] PRIMARY KEY CLUSTERED 
(
	[AuthorizationAttributeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]') AND name = N'AuthorizationAttributes_AuhorizationId_AttributeKey_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [AuthorizationAttributes_AuhorizationId_AttributeKey_Unique_Index] ON [dbo].[netsqlazman_AuthorizationAttributesTable] 
(
	[AuthorizationId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]') AND name = N'IX_AuthorizationAttributes')
CREATE NONCLUSTERED INDEX [IX_AuthorizationAttributes] ON [dbo].[netsqlazman_AuthorizationAttributesTable] 
(
	[AuthorizationId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Items]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Items]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_Items] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_ItemsTable].*
	FROM         dbo.[netsqlazman_ItemsTable] INNER JOIN
	                      dbo.[netsqlazman_Applications]() Applications ON dbo.[netsqlazman_ItemsTable].ApplicationId = Applications.ApplicationId
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Authorizations]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Authorizations]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_Authorizations]()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_AuthorizationsTable].*
	FROM         dbo.[netsqlazman_AuthorizationsTable] INNER JOIN
	                      dbo.[netsqlazman_Items]() Items ON dbo.[netsqlazman_AuthorizationsTable].ItemId = Items.ItemId
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_AuthorizationAttributes]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_AuthorizationAttributes] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_AuthorizationAttributesTable].*
	FROM         dbo.[netsqlazman_AuthorizationAttributesTable] INNER JOIN
	                      dbo.[netsqlazman_Authorizations]() as Authorizations ON dbo.[netsqlazman_AuthorizationAttributesTable].AuthorizationId = Authorizations.AuthorizationId
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_CheckStorePermissions]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CheckStorePermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/* 
   @ROLEID = { 0 READERS, 1 USERS, 2 MANAGERS}
*/
CREATE FUNCTION [dbo].[netsqlazman_CheckStorePermissions](@STOREID int, @ROLEID tinyint)
RETURNS bit
AS
BEGIN
DECLARE @RESULT bit
IF @STOREID IS NULL OR @ROLEID IS NULL
	SET @RESULT = 0	
ELSE
BEGIN
	IF EXISTS (
		SELECT     dbo.[netsqlazman_StorePermissionsTable].StoreId
		FROM         dbo.[netsqlazman_ApplicationsTable] RIGHT OUTER JOIN
		                      dbo.[netsqlazman_StoresTable] ON dbo.[netsqlazman_ApplicationsTable].StoreId = dbo.[netsqlazman_StoresTable].StoreId LEFT OUTER JOIN
		                      dbo.[netsqlazman_StorePermissionsTable] ON dbo.[netsqlazman_StoresTable].StoreId = dbo.[netsqlazman_StorePermissionsTable].StoreId LEFT OUTER JOIN
		                      dbo.[netsqlazman_ApplicationPermissionsTable] ON dbo.[netsqlazman_ApplicationsTable].ApplicationId = dbo.[netsqlazman_ApplicationPermissionsTable].ApplicationId
		WHERE 
		IS_MEMBER(''db_owner'')=1 OR IS_MEMBER(''NetSqlAzMan_Administrators'')=1 OR 
		(@ROLEID = 0 AND IS_MEMBER(''NetSqlAzMan_Readers'')=1 OR 
		@ROLEID = 1 AND IS_MEMBER(''NetSqlAzMan_Users'')=1 OR 
		@ROLEID = 2 AND IS_MEMBER(''NetSqlAzMan_Managers'')=1) AND
		(
		(dbo.[netsqlazman_StorePermissionsTable].StoreId = @STOREID AND dbo.[netsqlazman_StorePermissionsTable].NetSqlAzManFixedServerRole >= @ROLEID AND 
		(SUSER_SNAME(SUSER_SID())=[netsqlazman_StorePermissionsTable].SqlUserOrRole AND [netsqlazman_StorePermissionsTable].IsSqlRole = 0 OR
		IS_MEMBER([netsqlazman_StorePermissionsTable].SqlUserOrRole)=1 AND [netsqlazman_StorePermissionsTable].IsSqlRole = 1)) OR
	
		(@ROLEID = 0 AND dbo.[netsqlazman_StoresTable].StoreId = @STOREID AND dbo.[netsqlazman_ApplicationPermissionsTable].NetSqlAzManFixedServerRole >= @ROLEID AND 
		(SUSER_SNAME(SUSER_SID())=[netsqlazman_ApplicationPermissionsTable].SqlUserOrRole AND [netsqlazman_ApplicationPermissionsTable].IsSqlRole = 0
		OR IS_MEMBER([netsqlazman_ApplicationPermissionsTable].SqlUserOrRole)=1 AND [netsqlazman_ApplicationPermissionsTable].IsSqlRole = 1))))
	SET @RESULT = 1
	ELSE
	SET @RESULT = 0
END
RETURN @RESULT
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_Stores]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Stores]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_Stores] ()
RETURNS TABLE 
AS
RETURN
	SELECT dbo.[netsqlazman_StoresTable].* FROM dbo.[netsqlazman_StoresTable]
	WHERE dbo.[netsqlazman_CheckStorePermissions]([netsqlazman_StoresTable].StoreId, 0) = 1
' 
END
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[LoweredApplicationName] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
 CONSTRAINT [PK__aspnet_A__C93A4C981EA48E88] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [UQ__aspnet_A__17477DE42180FB33] UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [UQ__aspnet_A__30910331245D67DE] UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND name = N'aspnet_Applications_Index')
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_Applications] ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES (N'USSDNavigator', N'ussdnavigator', N'b4558855-4965-43f5-9d33-5b09c25ecd63', NULL)
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[MobilePIN] [nvarchar](16) COLLATE Cyrillic_General_CI_AS NULL,
	[Email] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[LoweredEmail] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[PasswordQuestion] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[PasswordAnswer] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] COLLATE Cyrillic_General_CI_AS NULL,
 CONSTRAINT [PK__aspnet_M__1788CC4D3D2915A8] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND name = N'aspnet_Membership_index')
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] 
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'bc3b7de1-1909-45c0-b14f-1fd2bc90ce11', N'DLrmuYYsTbDTwPDqVbCQ+ej4vEg=', 1, N'5ihM+nVZNzxqTSWF0NDS8A==', NULL, N'', N'', NULL, NULL, 1, 0, CAST(0x0000A10800695988 AS DateTime), CAST(0x0000A11500D55C27 AS DateTime), CAST(0x0000A10800695988 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), N'')
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'b9c9997d-ed88-4242-aba2-6b3ebfbbc2c9', N'Fshy2WMNsO3QAK5LS0FLppmmqJs=', 1, N'JB4pH9RUmSWQNPrHSslgSA==', NULL, N'', N'', NULL, NULL, 1, 0, CAST(0x0000A10800697DDC AS DateTime), CAST(0x0000A11500C62EE9 AS DateTime), CAST(0x0000A10800697DDC AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), N'Оператор рассылки')
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'1fba1cfe-b5ed-45ce-bed6-78cd0b031651', N'pX3uQAT/9ATzPJFuV/LmghwEFOE=', 1, N'e8O4V6ouDEik5cmRd5vCDw==', NULL, N'', N'', NULL, NULL, 1, 0, CAST(0x0000A1080069D110 AS DateTime), CAST(0x0000A11200610A31 AS DateTime), CAST(0x0000A1080069D110 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), N'Контент провайдер')
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'1fa40273-f8e7-4037-8eea-897b3cb4de54', N'oQNDzU5BMwkUN4g8is7H8XDQRAg=', 1, N'VvDolTbZw5ZjJA2IhezlKg==', NULL, N'abc@def.ru', N'abc@def.ru', NULL, NULL, 1, 0, CAST(0x0000A0D3007D0E74 AS DateTime), CAST(0x0000A0D3007D0E74 AS DateTime), CAST(0x0000A0D3007D0E74 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), N'Пользователь')
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'9c1ae9ed-6944-48de-9b1e-ca8a8bb1f03d', N'lmALYf/PcmwTV+xVSYPEEljQICs=', 1, N'oYbvFpQbLo4iQrAhqUP/dw==', NULL, N'd', N'd', NULL, NULL, 1, 0, CAST(0x0000A05400884C94 AS DateTime), CAST(0x0000A05C00CE0D0C AS DateTime), CAST(0x0000A05400884C94 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'65773f46-0d49-4d35-8700-34af5b93d317', N'OSHn5PM3QXjgH1CUEiMTf6qFVCI=', 1, N'jhX/eJWKZLpTb3c7yA709g==', NULL, N'd1', N'd1', NULL, NULL, 1, 1, CAST(0x0000A054008B9E30 AS DateTime), CAST(0x0000A05A009EEE42 AS DateTime), CAST(0x0000A054008B9E30 AS DateTime), CAST(0x0000A05A00A16BA9 AS DateTime), 5, CAST(0x0000A05A00A16BA9 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'f6183a9e-7834-415c-b787-6606c7b58d20', N'KR9tqTnMZCgUJfpycyf68WEl07w=', 1, N'BIzYroTenfjDUMSRgoTWLQ==', NULL, N'd2', N'd2', NULL, NULL, 1, 0, CAST(0x0000A054008CF460 AS DateTime), CAST(0x0000A054008CF460 AS DateTime), CAST(0x0000A054008CF460 AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), NULL)
INSERT [dbo].[aspnet_Membership] ([ApplicationId], [UserId], [Password], [PasswordFormat], [PasswordSalt], [MobilePIN], [Email], [LoweredEmail], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [Comment]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'206d0287-f481-4624-abdf-a1a2c943af9a', N'An33Xfz1qzKRfGd4K77TPSVQxcM=', 1, N'+5jEu+2wligQvZ9HLvjH2w==', NULL, N'd3', N'd3', NULL, NULL, 1, 0, CAST(0x0000A054008D78CC AS DateTime), CAST(0x0000A05C00CDE6E8 AS DateTime), CAST(0x0000A054008D78CC AS DateTime), CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), 0, CAST(0xFFFF2FB300000000 AS DateTime), N'Дмитрий Кузнецов')
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[LoweredUserName] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[MobileAlias] [nvarchar](16) COLLATE Cyrillic_General_CI_AS NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
	[UserSid] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__aspnet_U__1788CC4D29221CFB] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index2')
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [dbo].[aspnet_Users] ON
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate], [UserSid]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'b9c9997d-ed88-4242-aba2-6b3ebfbbc2c9', N'Operator', N'operator', NULL, 0, CAST(0x0000A11500C62EE9 AS DateTime), 2)
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate], [UserSid]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'1fba1cfe-b5ed-45ce-bed6-78cd0b031651', N'Provider', N'provider', NULL, 0, CAST(0x0000A11200610A31 AS DateTime), 3)
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate], [UserSid]) VALUES (N'b4558855-4965-43f5-9d33-5b09c25ecd63', N'bc3b7de1-1909-45c0-b14f-1fd2bc90ce11', N'Supervisor', N'supervisor', NULL, 0, CAST(0x0000A11500D55C27 AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[aspnet_Users] OFF
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_GetDBUsers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetDBUsers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'/* 
    NetSqlAzMan GetDBUsers TABLE Function
    ************************************************************************
    Creation Date: August, 23  2006
    Purpose: Retrieve from a DB a list of custom Users (DBUserSid, DBUserName)
    Author: Andrea Ferendeles 
    Revision: 1.0.0.0
    Updated by: <put here your name>
    Parameters: 
	use: 
		1)     SELECT * FROM dbo.GetDBUsers(<storename>, <applicationname>, NULL, NULL)            -- to retrieve all DB Users
		2)     SELECT * FROM dbo.GetDBUsers(<storename>, <applicationname>, <customsid>, NULL)  -- to retrieve DB User with specified <customsid>
		3)     SELECT * FROM dbo.GetDBUsers(<storename>, <applicationname>, NULL, <username>)  -- to retrieve DB User with specified <username>

    Remarks: 
	- Update this Function with your CUSTOM CODE
	- Returned DBUserSid must be unique
	- Returned DBUserName must be unique
*/
CREATE FUNCTION [dbo].[netsqlazman_GetDBUsers] (@StoreName nvarchar(255), @ApplicationName nvarchar(255), @DBUserSid VARBINARY(85) = NULL, @DBUserName nvarchar(255) = NULL)  
RETURNS TABLE 
AS  
RETURN 
	SELECT TOP 100 PERCENT CONVERT(VARBINARY(85), Users.UserSid) AS DBUserSid, Users.UserName AS DBUserName,Membership.Comment AS FullName FROM dbo.aspnet_Users as Users,dbo.aspnet_Membership as Membership
	WHERE Users.UserId=Membership.UserId AND (
		(@DBUserSid IS NOT NULL AND CONVERT(VARBINARY(85), Users.UserSid) = @DBUserSid OR @DBUserSid  IS NULL)
		AND
		(@DBUserName IS NOT NULL AND Users.UserName = @DBUserName OR @DBUserName IS NULL))
	ORDER BY Users.UserName
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- THIS CODE IS JUST FOR AN EXAMPLE: comment this section and customize "INSERT HERE YOUR CUSTOM T-SQL" section below
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_StoreGroupsTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_StoreGroupsTable](
	[StoreGroupId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[objectSid] [varbinary](85) NOT NULL,
	[Name] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Description] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[LDapQuery] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NULL,
	[GroupType] [tinyint] NOT NULL,
 CONSTRAINT [PK_StoreGroups] PRIMARY KEY CLUSTERED 
(
	[StoreGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]') AND name = N'IX_StoreGroups')
CREATE NONCLUSTERED INDEX [IX_StoreGroups] ON [dbo].[netsqlazman_StoreGroupsTable] 
(
	[StoreId] ASC,
	[objectSid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]') AND name = N'StoreGroups_StoreId_Name_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [StoreGroups_StoreId_Name_Unique_Index] ON [dbo].[netsqlazman_StoreGroupsTable] 
(
	[StoreId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StoreGroups]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroups]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_StoreGroups] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_StoreGroupsTable].*
	FROM         dbo.[netsqlazman_Stores]() Stores INNER JOIN
	                      dbo.[netsqlazman_StoreGroupsTable] ON Stores.StoreId = dbo.[netsqlazman_StoreGroupsTable].StoreId
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_GetNameFromSid]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetNameFromSid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Andrea Ferendeles
-- Create date: 13/04/2006
-- Description:	Get Name From Sid
-- =============================================
CREATE FUNCTION [dbo].[netsqlazman_GetNameFromSid] (@StoreName nvarchar(255), @ApplicationName nvarchar(255), @sid varbinary(85), @SidWhereDefined tinyint)
RETURNS nvarchar(255)
AS
BEGIN

DECLARE @Name nvarchar(255)
SET @Name = NULL

IF (@SidWhereDefined=0) --Store
BEGIN
SET @Name = (SELECT TOP 1 Name FROM dbo.[netsqlazman_StoreGroups]() WHERE objectSid = @sid)
END
ELSE IF (@SidWhereDefined=1) --Application 
BEGIN
SET @Name = (SELECT TOP 1 Name FROM dbo.[netsqlazman_ApplicationGroups]() WHERE objectSid = @sid)
END
ELSE IF (@SidWhereDefined=2 OR @SidWhereDefined=3) --LDAP or LOCAL
BEGIN
SET @Name = (SELECT Suser_Sname(@sid))
END
ELSE IF (@SidWhereDefined=4) --Database
BEGIN
SET @Name = (SELECT DBUserName FROM dbo.[netsqlazman_GetDBUsers](@StoreName, @ApplicationName, @sid, NULL))
END
IF (@Name IS NULL)
BEGIN
	SET @Name = @sid
END
RETURN @Name
END
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_AuthorizationView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_AuthorizationView]
AS
SELECT     [netsqlazman_Authorizations].AuthorizationId, [netsqlazman_Authorizations].ItemId, dbo.[netsqlazman_GetNameFromSid]([netsqlazman_Stores].Name, [netsqlazman_Applications].Name, [netsqlazman_Authorizations].ownerSid, 
                      [netsqlazman_Authorizations].ownerSidWhereDefined) AS Owner, dbo.[netsqlazman_GetNameFromSid]([netsqlazman_Stores].Name, [netsqlazman_Applications].Name, [netsqlazman_Authorizations].objectSid, 
                      [netsqlazman_Authorizations].objectSidWhereDefined) AS Name, [netsqlazman_Authorizations].objectSid, 
                      CASE objectSidWhereDefined WHEN 0 THEN ''Store'' WHEN 1 THEN ''Application'' WHEN 2 THEN ''LDAP'' WHEN 3 THEN ''Local'' WHEN 4 THEN ''DATABASE'' END AS SidWhereDefined,
                       CASE AuthorizationType WHEN 0 THEN ''NEUTRAL'' WHEN 1 THEN ''ALLOW'' WHEN 2 THEN ''DENY'' WHEN 3 THEN ''ALLOWWITHDELEGATION'' END AS AuthorizationType,
                       [netsqlazman_Authorizations].ValidFrom, [netsqlazman_Authorizations].ValidTo
FROM         dbo.[netsqlazman_Authorizations]() [netsqlazman_Authorizations] INNER JOIN
                      dbo.[netsqlazman_Items]() [netsqlazman_Items] ON [netsqlazman_Authorizations].ItemId = [netsqlazman_Items].ItemId INNER JOIN
                      dbo.[netsqlazman_Applications]() [netsqlazman_Applications] ON [netsqlazman_Items].ApplicationId = [netsqlazman_Applications].ApplicationId INNER JOIN
                      dbo.[netsqlazman_Stores]() [netsqlazman_Stores] ON [netsqlazman_Applications].StoreId = [netsqlazman_Stores].StoreId
'
GO
/****** Object:  View [dbo].[netsqlazman_AuthorizationAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_AuthorizationAttributesView]
AS
SELECT     dbo.[netsqlazman_AuthorizationView].AuthorizationId, dbo.[netsqlazman_AuthorizationView].ItemId, dbo.[netsqlazman_AuthorizationView].Owner, dbo.[netsqlazman_AuthorizationView].Name, dbo.[netsqlazman_AuthorizationView].objectSid, 
                      dbo.[netsqlazman_AuthorizationView].SidWhereDefined, dbo.[netsqlazman_AuthorizationView].AuthorizationType, dbo.[netsqlazman_AuthorizationView].ValidFrom, dbo.[netsqlazman_AuthorizationView].ValidTo, 
                      [netsqlazman_AuthorizationAttributes].AuthorizationAttributeId, [netsqlazman_AuthorizationAttributes].AttributeKey, [netsqlazman_AuthorizationAttributes].AttributeValue
FROM         dbo.[netsqlazman_AuthorizationView] INNER JOIN
                      dbo.[netsqlazman_AuthorizationAttributes]() [netsqlazman_AuthorizationAttributes] ON dbo.[netsqlazman_AuthorizationView].AuthorizationId = [netsqlazman_AuthorizationAttributes].AuthorizationId
'
GO
/****** Object:  Table [dbo].[netsqlazman_ItemsHierarchyTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ItemsHierarchyTable](
	[ItemId] [int] NOT NULL,
	[MemberOfItemId] [int] NOT NULL,
 CONSTRAINT [PK_ItemsHierarchy] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC,
	[MemberOfItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyTable]') AND name = N'IX_ItemsHierarchy')
CREATE NONCLUSTERED INDEX [IX_ItemsHierarchy] ON [dbo].[netsqlazman_ItemsHierarchyTable] 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyTable]') AND name = N'IX_ItemsHierarchy_1')
CREATE NONCLUSTERED INDEX [IX_ItemsHierarchy_1] ON [dbo].[netsqlazman_ItemsHierarchyTable] 
(
	[MemberOfItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (246, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (249, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (268, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (274, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (303, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (304, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (305, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (307, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (308, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (309, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (311, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (312, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (313, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (314, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (315, 244)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (245, 246)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (251, 246)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (253, 246)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (275, 246)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (246, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (271, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (272, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (307, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (308, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (309, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (311, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (312, 248)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (250, 249)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (256, 249)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (260, 249)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (261, 249)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (307, 258)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (308, 258)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (311, 258)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (246, 267)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (307, 267)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (309, 267)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (312, 267)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (325, 267)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (330, 267)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (257, 268)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (259, 268)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (262, 268)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (263, 271)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (298, 271)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (288, 272)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (293, 272)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (295, 272)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (265, 274)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (273, 274)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (246, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (274, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (303, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (304, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (305, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (307, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (308, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (309, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (311, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (312, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (313, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (314, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (315, 286)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (303, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (304, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (305, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (307, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (308, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (309, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (311, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (312, 296)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (301, 303)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (302, 303)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (251, 304)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (252, 304)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (253, 304)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (254, 304)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (255, 304)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (279, 304)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (276, 305)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (280, 305)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (283, 305)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (326, 305)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (277, 307)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (281, 307)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (289, 308)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (290, 308)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (291, 308)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (292, 308)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (277, 311)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (285, 311)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (299, 312)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (300, 312)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (243, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (280, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (316, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (318, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (320, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (321, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (323, 313)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (328, 313)
GO
print 'Processed 100 total records'
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (263, 314)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (270, 314)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (297, 314)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (287, 315)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (293, 315)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (294, 315)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (243, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (317, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (319, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (320, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (322, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (324, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (327, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (329, 325)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (276, 330)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (283, 330)
INSERT [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (327, 330)
/****** Object:  Trigger [ItemsHierarchyTrigger]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ItemsHierarchyTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[ItemsHierarchyTrigger] ON [dbo].[netsqlazman_ItemsHierarchyTable] 
FOR INSERT, UPDATE
AS
DECLARE @INSERTEDITEMID int
DECLARE @INSERTEDMEMBEROFITEMID int

DECLARE itemhierarchy_cur CURSOR FAST_FORWARD FOR SELECT ItemId, MemberOfItemId FROM inserted
OPEN itemhierarchy_cur
FETCH NEXT from itemhierarchy_cur INTO @INSERTEDITEMID, @INSERTEDMEMBEROFITEMID
WHILE @@FETCH_STATUS = 0
BEGIN
	IF UPDATE(ItemId) AND NOT EXISTS (SELECT ItemId FROM dbo.[netsqlazman_ItemsTable] WHERE [netsqlazman_ItemsTable].ItemId = @INSERTEDITEMID) 
	 BEGIN
	  RAISERROR (''ItemId NOT FOUND into dbo.ItemsTable'', 16, 1)
	  ROLLBACK TRANSACTION
	 END
	
	IF UPDATE(MemberOfItemId) AND NOT EXISTS (SELECT ItemId FROM dbo.[netsqlazman_ItemsTable] WHERE [netsqlazman_ItemsTable].ItemId = @INSERTEDMEMBEROFITEMID)
	 BEGIN
	  RAISERROR (''MemberOfItemId NOT FOUND into dbo.ItemsTable'', 16, 1)
	  ROLLBACK TRANSACTION
	 END
	FETCH NEXT from itemhierarchy_cur INTO @INSERTEDITEMID, @INSERTEDMEMBEROFITEMID
END
CLOSE itemhierarchy_cur
DEALLOCATE itemhierarchy_cur
'
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ItemsHierarchy]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchy]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_ItemsHierarchy] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_ItemsHierarchyTable].*
	FROM         dbo.[netsqlazman_ItemsHierarchyTable] INNER JOIN
	                      dbo.[netsqlazman_Items]() Items ON dbo.[netsqlazman_ItemsHierarchyTable].ItemId = Items.ItemId
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_ItemAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ItemAttributesTable](
	[ItemAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NOT NULL,
	[AttributeKey] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[AttributeValue] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_ItemAttributes] PRIMARY KEY CLUSTERED 
(
	[ItemAttributeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]') AND name = N'ItemAttributes_AuhorizationId_AttributeKey_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [ItemAttributes_AuhorizationId_AttributeKey_Unique_Index] ON [dbo].[netsqlazman_ItemAttributesTable] 
(
	[ItemId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]') AND name = N'IX_ItemAttributes')
CREATE NONCLUSTERED INDEX [IX_ItemAttributes] ON [dbo].[netsqlazman_ItemAttributesTable] 
(
	[ItemId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
SET IDENTITY_INSERT [dbo].[netsqlazman_ItemAttributesTable] ON
INSERT [dbo].[netsqlazman_ItemAttributesTable] ([ItemAttributeId], [ItemId], [AttributeKey], [AttributeValue]) VALUES (15, 258, N'Description', N'CPR-служба')
INSERT [dbo].[netsqlazman_ItemAttributesTable] ([ItemAttributeId], [ItemId], [AttributeKey], [AttributeValue]) VALUES (16, 286, N'Description', N'Оператор')
INSERT [dbo].[netsqlazman_ItemAttributesTable] ([ItemAttributeId], [ItemId], [AttributeKey], [AttributeValue]) VALUES (17, 296, N'Description', N'Супервизор')
SET IDENTITY_INSERT [dbo].[netsqlazman_ItemAttributesTable] OFF
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ItemAttributes]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_ItemAttributes] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_ItemAttributesTable].*
	FROM         dbo.[netsqlazman_ItemAttributesTable] INNER JOIN
	                      dbo.[netsqlazman_Items]() Items ON dbo.[netsqlazman_ItemAttributesTable].ItemId = Items.ItemId
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_ItemAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_ItemAttributesView]
AS
SELECT     [netsqlazman_Items].ItemId, [netsqlazman_Items].ApplicationId, [netsqlazman_Items].Name, [netsqlazman_Items].Description, 
                      CASE [netsqlazman_Items].ItemType WHEN 0 THEN ''Role'' WHEN 1 THEN ''Task'' WHEN 2 THEN ''Operation'' END AS ItemType, [netsqlazman_ItemAttributes].ItemAttributeId, 
                      [netsqlazman_ItemAttributes].AttributeKey, [netsqlazman_ItemAttributes].AttributeValue
FROM         dbo.[netsqlazman_Items]() [netsqlazman_Items] INNER JOIN
                      dbo.[netsqlazman_ItemAttributes]() [netsqlazman_ItemAttributes] ON [netsqlazman_Items].ItemId = [netsqlazman_ItemAttributes].ItemId
'
GO
/****** Object:  Table [dbo].[netsqlazman_StoreGroupMembersTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_StoreGroupMembersTable](
	[StoreGroupMemberId] [int] IDENTITY(1,1) NOT NULL,
	[StoreGroupId] [int] NOT NULL,
	[objectSid] [varbinary](85) NOT NULL,
	[WhereDefined] [tinyint] NOT NULL,
	[IsMember] [bit] NOT NULL,
 CONSTRAINT [PK_StoreGroupMembers] PRIMARY KEY CLUSTERED 
(
	[StoreGroupMemberId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]') AND name = N'IX_StoreGroupMembers')
CREATE NONCLUSTERED INDEX [IX_StoreGroupMembers] ON [dbo].[netsqlazman_StoreGroupMembersTable] 
(
	[StoreGroupId] ASC,
	[objectSid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]') AND name = N'StoreGroupMembers_StoreGroupId_ObjectSid_IsMember_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [StoreGroupMembers_StoreGroupId_ObjectSid_IsMember_Unique_Index] ON [dbo].[netsqlazman_StoreGroupMembersTable] 
(
	[StoreGroupId] ASC,
	[objectSid] ASC,
	[IsMember] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StoreGroupMembers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembers]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_StoreGroupMembers] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_StoreGroupMembersTable].*
	FROM         dbo.[netsqlazman_StoreGroupMembersTable] INNER JOIN
	                      dbo.[netsqlazman_StoreGroups]() StoreGroups ON dbo.[netsqlazman_StoreGroupMembersTable].StoreGroupId = StoreGroups.StoreGroupId
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ExecuteLDAPQuery]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ExecuteLDAPQuery]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ExecuteLDAPQuery](@LDAPPATH NVARCHAR(4000), @LDAPQUERY NVARCHAR(4000), @members_cur CURSOR VARYING OUTPUT)
AS
-- REMEMBER !!!
-- BEFORE executing ExecuteLDAPQuery procedure ... a Linked Server named ''ADSI'' must be added:
-- --sp_addlinkedserver ''ADSI'', ''Active Directory Service Interfaces'', ''ADSDSOObject'', ''adsdatasource''
CREATE TABLE #temp (objectSid VARBINARY(85))
IF @LDAPQUERY IS NULL OR RTRIM(LTRIM(@LDAPQUERY))='''' OR @LDAPPATH IS NULL OR RTRIM(LTRIM(@LDAPPATH))=''''
BEGIN
SET @members_cur = CURSOR STATIC FORWARD_ONLY FOR SELECT * FROM #temp
OPEN @members_cur
DROP TABLE #temp
RETURN
END
SET @LDAPPATH = REPLACE(@LDAPPATH, N'''''''', N'''''''''''')
SET @LDAPQUERY = REPLACE(@LDAPQUERY, N'''''''', N'''''''''''')
DECLARE @QUERY nvarchar(4000)
DECLARE @LDAPROOTDSEPART nvarchar(4000)
DECLARE @LDAPQUERYPART nvarchar(4000)
SET @LDAPROOTDSEPART = LTRIM(@LDAPQUERY)
IF CHARINDEX(''[RootDSE:'', @LDAPROOTDSEPART)=1
BEGIN
	SET @LDAPROOTDSEPART = SUBSTRING(@LDAPROOTDSEPART, 10, CHARINDEX('']'', @LDAPROOTDSEPART)-10)
	SET @LDAPQUERYPART = SUBSTRING(@LDAPQUERY, CHARINDEX( '']'', @LDAPQUERY)+1, 4000)
END
ELSE
BEGIN
	SET @LDAPROOTDSEPART = @LDAPPATH
	SET @LDAPQUERYPART = @LDAPQUERY
END
SET @QUERY = CHAR(39) + ''<'' + ''LDAP://''+ @LDAPROOTDSEPART + ''>;(&(!(objectClass=computer))(&(|(objectClass=user)(objectClass=group)))'' + @LDAPQUERYPART + '');objectSid;subtree'' + CHAR(39) 
DECLARE @OPENQUERY nvarchar(4000)
SET @OPENQUERY = ''SELECT * FROM OPENQUERY(ADSI, '' + @QUERY + '')''
INSERT INTO #temp EXEC (@OPENQUERY)
SET @members_cur = CURSOR STATIC FORWARD_ONLY FOR SELECT * FROM #temp
OPEN @members_cur
DROP TABLE #temp
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GetStoreGroupSidMembers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetStoreGroupSidMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_GetStoreGroupSidMembers](@ISMEMBER BIT, @GROUPOBJECTSID VARBINARY(85), @NETSQLAZMANMODE bit, @LDAPPATH nvarchar(4000), @member_cur CURSOR VARYING OUTPUT)
AS
DECLARE @RESULT TABLE (objectSid VARBINARY(85))
DECLARE @GROUPID INT
DECLARE @GROUPTYPE TINYINT
DECLARE @LDAPQUERY nvarchar(4000)
DECLARE @sub_members_cur CURSOR
DECLARE @OBJECTSID VARBINARY(85)
SELECT @GROUPID = StoreGroupId, @GROUPTYPE = GroupType, @LDAPQUERY = LDapQuery FROM dbo.[netsqlazman_StoreGroups]() WHERE objectSid = @GROUPOBJECTSID
IF @GROUPTYPE = 0 -- BASIC
BEGIN
	--memo: WhereDefined can be:0 - Store; 1 - Application; 2 - LDAP; 3 - Local; 4 - Database
	-- Windows SIDs
	INSERT INTO @RESULT (objectSid) 
	SELECT objectSid 
	FROM dbo.[netsqlazman_StoreGroupMembersTable]
	WHERE 
	StoreGroupId = @GROUPID AND IsMember = @ISMEMBER AND
	((@NETSQLAZMANMODE = 0 AND (WhereDefined = 2 OR WhereDefined = 4)) OR (@NETSQLAZMANMODE = 1 AND WhereDefined BETWEEN 2 AND 4))
	-- Store Groups Members
	DECLARE @MemberObjectSid VARBINARY(85)
	DECLARE @MemberType bit
	DECLARE @NotMemberType bit
	DECLARE nested_Store_groups_cur CURSOR LOCAL FAST_FORWARD FOR
		SELECT objectSid, IsMember FROM dbo.[netsqlazman_StoreGroupMembersTable] WHERE StoreGroupId = @GROUPID AND WhereDefined = 0
	
	OPEN nested_Store_groups_cur
	FETCH NEXT FROM nested_Store_groups_cur INTO @MemberObjectSid, @MemberType
	WHILE @@FETCH_STATUS = 0
	BEGIN
	        -- recursive call
		IF @ISMEMBER = 1
		BEGIN
			IF @MemberType = 0 
				SET @NotMemberType = 0
			ELSE
				SET @NotMemberType = 1
		END
		ELSE
		BEGIN
			IF @MemberType = 0 
				SET @NotMemberType = 1
			ELSE
				SET @NotMemberType = 0
		END
		EXEC dbo.[netsqlazman_GetStoreGroupSidMembers] @NotMemberType, @MemberObjectSid, @NETSQLAZMANMODE, @LDAPPATH, @sub_members_cur OUTPUT
		FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
		WHILE @@FETCH_STATUS=0
		BEGIN
			INSERT INTO @RESULT VALUES (@OBJECTSID)
			FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
		END		
		CLOSE @sub_members_cur
		DEALLOCATE @sub_members_cur	

		FETCH NEXT FROM nested_Store_groups_cur INTO @MemberObjectSid, @MemberType
	END
	CLOSE nested_Store_groups_cur
	DEALLOCATE nested_Store_groups_cur
END
ELSE IF @GROUPTYPE = 1 AND @ISMEMBER = 1 -- LDAP QUERY
BEGIN
	EXEC dbo.[netsqlazman_ExecuteLDAPQuery] @LDAPPATH, @LDAPQUERY, @sub_members_cur OUTPUT
	FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
	WHILE @@FETCH_STATUS=0
	BEGIN
		INSERT INTO @RESULT (objectSid) VALUES (@OBJECTSID)
		FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
	END
	CLOSE @sub_members_cur
	DEALLOCATE @sub_members_cur
END
SET @member_cur = CURSOR STATIC FORWARD_ONLY FOR SELECT * FROM @RESULT
OPEN @member_cur
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GetApplicationGroupSidMembers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GetApplicationGroupSidMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_GetApplicationGroupSidMembers](@ISMEMBER BIT, @GROUPOBJECTSID VARBINARY(85), @NETSQLAZMANMODE bit, @LDAPPATH nvarchar(4000), @member_cur CURSOR VARYING OUTPUT)
AS
DECLARE @RESULT TABLE (objectSid VARBINARY(85))
DECLARE @GROUPID INT
DECLARE @GROUPTYPE TINYINT
DECLARE @LDAPQUERY nvarchar(4000)
DECLARE @sub_members_cur CURSOR
DECLARE @OBJECTSID VARBINARY(85)
SELECT @GROUPID = ApplicationGroupId, @GROUPTYPE = GroupType, @LDAPQUERY = LDapQuery FROM [netsqlazman_ApplicationGroupsTable] WHERE objectSid = @GROUPOBJECTSID
IF @GROUPTYPE = 0 -- BASIC
BEGIN
	--memo: WhereDefined can be:0 - Store; 1 - Application; 2 - LDAP; 3 - Local; 4 - Database
	-- Windows SIDs
	INSERT INTO @RESULT (objectSid) 
	SELECT objectSid 
	FROM dbo.[netsqlazman_ApplicationGroupMembersTable]
	WHERE 
	ApplicationGroupId = @GROUPID AND IsMember = @ISMEMBER AND
	((@NETSQLAZMANMODE = 0 AND (WhereDefined = 2 OR WhereDefined = 4)) OR (@NETSQLAZMANMODE = 1 AND WhereDefined BETWEEN 2 AND 4))
	-- Store Groups Members
	DECLARE @MemberObjectSid VARBINARY(85)
	DECLARE @MemberType bit
	DECLARE @NotMemberType bit
	DECLARE nested_Store_groups_cur CURSOR LOCAL FAST_FORWARD FOR
		SELECT objectSid, IsMember FROM dbo.[netsqlazman_ApplicationGroupMembersTable] WHERE ApplicationGroupId = @GROUPID AND WhereDefined = 0
	
	OPEN nested_Store_groups_cur
	FETCH NEXT FROM nested_Store_groups_cur INTO @MemberObjectSid, @MemberType
	WHILE @@FETCH_STATUS = 0
	BEGIN
	        -- recursive call
		IF @ISMEMBER = 1
		BEGIN
			IF @MemberType = 0 
				SET @NotMemberType = 0
			ELSE
				SET @NotMemberType = 1
		END
		ELSE
		BEGIN
			IF @MemberType = 0 
				SET @NotMemberType = 1
			ELSE
				SET @NotMemberType = 0
		END
		EXEC dbo.[netsqlazman_GetStoreGroupSidMembers] @NotMemberType, @MemberObjectSid, @NETSQLAZMANMODE, @LDAPPATH, @sub_members_cur OUTPUT
		FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
		WHILE @@FETCH_STATUS=0
		BEGIN
			INSERT INTO @RESULT VALUES (@OBJECTSID)
			FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
		END		
		CLOSE @sub_members_cur
		DEALLOCATE @sub_members_cur			

		FETCH NEXT FROM nested_Store_groups_cur INTO @MemberObjectSid, @MemberType
	END
	CLOSE nested_Store_groups_cur
	DEALLOCATE nested_Store_groups_cur
	
	-- Application Groups Members
	DECLARE nested_Application_groups_cur CURSOR LOCAL FAST_FORWARD FOR
		SELECT objectSid, IsMember FROM dbo.[netsqlazman_ApplicationGroupMembersTable] WHERE ApplicationGroupId = @GROUPID AND WhereDefined = 1
	
	OPEN nested_Application_groups_cur
	FETCH NEXT FROM nested_Application_groups_cur INTO @MemberObjectSid, @MemberType
	WHILE @@FETCH_STATUS = 0
	BEGIN
	        -- recursive call
		IF @ISMEMBER = 1
		BEGIN
			IF @MemberType = 0 
				SET @NotMemberType = 0
			ELSE
				SET @NotMemberType = 1
		END
		ELSE
		BEGIN
			IF @MemberType = 0 
				SET @NotMemberType = 1
			ELSE
				SET @NotMemberType = 0
		END
		EXEC dbo.[netsqlazman_GetApplicationGroupSidMembers] @NotMemberType, @MemberObjectSid, @NETSQLAZMANMODE, @LDAPPATH, @sub_members_cur OUTPUT
		FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
		WHILE @@FETCH_STATUS=0
		BEGIN
			INSERT INTO @RESULT VALUES (@OBJECTSID)
			FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
		END		
		CLOSE @sub_members_cur
		DEALLOCATE @sub_members_cur	

		FETCH NEXT FROM nested_Application_groups_cur INTO @MemberObjectSid, @MemberType
	END
	CLOSE nested_Application_groups_cur
	DEALLOCATE nested_Application_groups_cur
	END
ELSE IF @GROUPTYPE = 1 AND @ISMEMBER = 1 -- LDAP QUERY
BEGIN
	EXEC dbo.[netsqlazman_ExecuteLDAPQuery] @LDAPPATH, @LDAPQUERY, @sub_members_cur OUTPUT
	FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
	WHILE @@FETCH_STATUS=0
	BEGIN
		INSERT INTO @RESULT (objectSid) VALUES (@OBJECTSID)
		FETCH NEXT FROM @sub_members_cur INTO @OBJECTSID
	END
	CLOSE @sub_members_cur
	DEALLOCATE @sub_members_cur
END
SET @member_cur = CURSOR STATIC FORWARD_ONLY FOR SELECT * FROM @RESULT
OPEN @member_cur
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_IsAMemberOfGroup]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_IsAMemberOfGroup]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_IsAMemberOfGroup](@GROUPTYPE bit, @GROUPOBJECTSID VARBINARY(85), @NETSQLAZMANMODE bit, @LDAPPATH nvarchar(4000), @TOKEN IMAGE, @USERGROUPSCOUNT INT)  
AS  
DECLARE @member_cur CURSOR
DECLARE @MemberSid VARBINARY(85)
DECLARE @USERSID VARBINARY(85)
DECLARE @USERGROUPS TABLE(objectSid VARBINARY(85))
DECLARE @I INT
DECLARE @INDEX INT
DECLARE @APP VARBINARY(85)
DECLARE @COUNT int

-- Get User Sid
IF @USERGROUPSCOUNT>0
BEGIN
	SET @USERSID = SUBSTRING(@TOKEN,1,85)
	SET @I = CHARINDEX(0x01, @USERSID)
	SET @USERSID = SUBSTRING(@USERSID, @I, 85-@I+1)
	-- Get User Groups Sid
	SET @I = 0
	WHILE (@I<@USERGROUPSCOUNT)
	BEGIN
		SET @APP = SUBSTRING(@TOKEN,(@I+1)*85+1,85) --GET USER GROUP TOKEN PORTION
		SET @INDEX = CHARINDEX(0x01, @APP) -- FIND TOKEN START (0x01)
		SET @APP = SUBSTRING(@APP, @INDEX, 85-@INDEX+1) -- EXTRACT USER GROUP SID
		INSERT INTO @USERGROUPS (objectSid) VALUES (@APP)
		SET @I = @I + 1
	END
END
ELSE
BEGIN
	SET @USERSID = @TOKEN
END

-- CHECK IF IS A NON-MEMBER
IF @GROUPTYPE = 0 -- STORE GROUP
	EXEC dbo.[netsqlazman_GetStoreGroupSidMembers] 0, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @member_cur OUTPUT
ELSE -- APPLICATON GROUP
	EXEC dbo.[netsqlazman_GetApplicationGroupSidMembers] 0, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @member_cur OUTPUT

FETCH NEXT FROM @member_cur INTO @MemberSid
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @MemberSid = @USERSID
	BEGIN
		CLOSE @member_cur
		DEALLOCATE @member_cur
		SELECT CONVERT(bit, 0) -- true
		RETURN
	END		
	SELECT @COUNT =  COUNT(*)  FROM @USERGROUPS WHERE objectSid = @MemberSid
	IF @COUNT>0
	BEGIN
		CLOSE @member_cur
		DEALLOCATE @member_cur
		SELECT CONVERT(bit, 0) -- true
		RETURN
	END		
	FETCH NEXT FROM @member_cur INTO @MemberSid
END
CLOSE @member_cur
DEALLOCATE @member_cur

-- CHECK IF IS A MEMBER
IF @GROUPTYPE = 0 -- STORE GROUP
	EXEC dbo.[netsqlazman_GetStoreGroupSidMembers] 1, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @member_cur OUTPUT
ELSE -- APPLICATON GROUP
	EXEC dbo.[netsqlazman_GetApplicationGroupSidMembers] 1, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @member_cur OUTPUT

FETCH NEXT FROM @member_cur INTO @MemberSid
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @MemberSid = @USERSID
	BEGIN
		CLOSE @member_cur
		DEALLOCATE @member_cur
		SELECT CONVERT(bit,1) -- true
		RETURN
	END		
	SELECT @COUNT =  COUNT(*)  FROM @USERGROUPS WHERE objectSid = @MemberSid
	IF @COUNT>0
	BEGIN
		CLOSE @member_cur
		DEALLOCATE @member_cur
		SELECT CONVERT(bit, 1) -- true
		RETURN
	END		
	FETCH NEXT FROM @member_cur INTO @MemberSid
END
CLOSE @member_cur
DEALLOCATE @member_cur

SELECT CONVERT(bit, 0) -- true
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_BizRules]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRules]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_BizRules]()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_BizRulesTable].*
	FROM         dbo.[netsqlazman_BizRulesTable] INNER JOIN
	                      dbo.[netsqlazman_Items]() Items ON dbo.[netsqlazman_BizRulesTable].BizRuleId = Items.BizRuleId
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_BizRuleView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_BizRuleView]
AS
SELECT     [netsqlazman_Items].ItemId, [netsqlazman_Items].ApplicationId, [netsqlazman_Items].Name, [netsqlazman_Items].Description, [netsqlazman_Items].ItemType, [netsqlazman_BizRules].BizRuleSource, [netsqlazman_BizRules].BizRuleLanguage, 
                      [netsqlazman_BizRules].CompiledAssembly
FROM         dbo.[netsqlazman_Items]() [netsqlazman_Items] INNER JOIN
                      dbo.[netsqlazman_BizRules]() [netsqlazman_BizRules] ON [netsqlazman_Items].BizRuleId = [netsqlazman_BizRules].BizRuleId
'
GO
/****** Object:  Trigger [StoreGroupDeleteTrigger]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[StoreGroupDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[StoreGroupDeleteTrigger] ON [dbo].[netsqlazman_StoreGroupsTable] 
FOR DELETE 
AS
DECLARE @DELETEDOBJECTSID int
DECLARE storegroups_cur CURSOR FAST_FORWARD FOR SELECT objectSid FROM deleted
OPEN storegroups_cur
FETCH NEXT from storegroups_cur INTO @DELETEDOBJECTSID
WHILE @@FETCH_STATUS = 0
BEGIN
	DELETE FROM dbo.[netsqlazman_StoreGroupMembersTable] WHERE objectSid = @DELETEDOBJECTSID AND WhereDefined = 0
	DELETE FROM dbo.[netsqlazman_ApplicationGroupMembersTable] WHERE objectSid = @DELETEDOBJECTSID AND WhereDefined = 0
	DELETE FROM dbo.[netsqlazman_AuthorizationsTable] WHERE objectSid = @DELETEDOBJECTSID AND objectSidWhereDefined = 0
	FETCH NEXT from storegroups_cur INTO @DELETEDOBJECTSID
END
CLOSE storegroups_cur
DEALLOCATE storegroups_cur
'
GO
/****** Object:  Trigger [ApplicationGroupDeleteTrigger]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ApplicationGroupDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[ApplicationGroupDeleteTrigger] ON [dbo].[netsqlazman_ApplicationGroupsTable] 
FOR DELETE 
AS
DECLARE @DELETEDOBJECTSID int
DECLARE applicationgroups_cur CURSOR FAST_FORWARD FOR SELECT objectSid FROM deleted
OPEN applicationgroups_cur
FETCH NEXT from applicationgroups_cur INTO @DELETEDOBJECTSID
WHILE @@FETCH_STATUS = 0
BEGIN
	DELETE FROM dbo.[netsqlazman_ApplicationGroupMembersTable] WHERE objectSid = @DELETEDOBJECTSID AND WhereDefined = 1
	DELETE FROM dbo.[netsqlazman_AuthorizationsTable] WHERE objectSid = @DELETEDOBJECTSID AND objectSidWhereDefined = 1
	FETCH NEXT from applicationgroups_cur INTO @DELETEDOBJECTSID
END
CLOSE applicationgroups_cur
DEALLOCATE applicationgroups_cur
'
GO
/****** Object:  Table [dbo].[UsersDemo]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersDemo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UsersDemo](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Password] [varbinary](50) NULL,
	[FullName] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[OtherFields] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NULL,
 CONSTRAINT [PK_UsersDemo] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Trigger [ItemDeleteTrigger]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ItemDeleteTrigger]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[ItemDeleteTrigger] ON [dbo].[netsqlazman_ItemsTable] 
FOR DELETE 
AS
DECLARE @DELETEDITEMID int
DECLARE @BIZRULEID int
DECLARE items_cur CURSOR FAST_FORWARD FOR SELECT ItemId, BizRuleId FROM deleted
OPEN items_cur
FETCH NEXT from items_cur INTO @DELETEDITEMID, @BIZRULEID
WHILE @@FETCH_STATUS = 0
BEGIN
	DELETE FROM dbo.[netsqlazman_ItemsHierarchyTable] WHERE ItemId = @DELETEDITEMID OR MemberOfItemId = @DELETEDITEMID
	IF @BIZRULEID IS NOT NULL
		DELETE FROM dbo.[netsqlazman_BizRulesTable] WHERE BizRuleId = @BIZRULEID
	FETCH NEXT from items_cur INTO @DELETEDITEMID, @BIZRULEID
END
CLOSE items_cur
DEALLOCATE items_cur
'
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_DBVersion]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DBVersion]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_DBVersion] ()  
RETURNS nvarchar(200) AS  
BEGIN 
	return ''3.6.0.x''
END
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_MergeAuthorizations]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_MergeAuthorizations]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_MergeAuthorizations](@AUTH1 tinyint, @AUTH2 tinyint)
RETURNS tinyint
AS
BEGIN
-- 0 Neutral 1 Allow 2 Deny 3 AllowWithDelegation
DECLARE @RESULT tinyint
IF @AUTH1 IS NULL 
BEGIN
	SET @RESULT = @AUTH2
END
ELSE 
IF @AUTH2 IS NULL 
BEGIN
SET @RESULT = @AUTH1
END
ELSE
BEGIN
	IF @AUTH1 = 2 SET @AUTH1 = 4 -- DENY WINS
	ELSE
	IF @AUTH2 = 2 SET @AUTH2 = 4 -- DENY WINS
	IF @AUTH1 >= @AUTH2
                	SET @RESULT = @AUTH1
	ELSE
	IF @AUTH1 < @AUTH2
		SET @RESULT = @AUTH2
	IF @RESULT = 4 SET @RESULT = 2
END
RETURN @RESULT
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_CheckAccess]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CheckAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_CheckAccess] (@ITEMID INT, @USERSID VARBINARY(85), @VALIDFOR DATETIME, @LDAPPATH nvarchar(4000), @AUTHORIZATION_TYPE TINYINT OUTPUT, @NETSQLAZMANMODE BIT, @RETRIEVEATTRIBUTES BIT) 
AS
---------------------------------------------------
-- VARIABLES DECLARATION
-- 0 - Neutral; 1 - Allow; 2 - Deny; 3 - AllowWithDelegation
SET NOCOUNT ON
DECLARE @PARENTITEMID INT
DECLARE @PKID INT
DECLARE @PARENTRESULT TINYINT
DECLARE @APP VARBINARY(85)
DECLARE @members_cur CURSOR
DECLARE @OBJECTSID VARBINARY(85)
DECLARE @ITEM_AUTHORIZATION_TYPE TINYINT
---------------------------------------------------
-- INITIALIZE VARIABLES
SET @ITEM_AUTHORIZATION_TYPE = 0 -- Neutral
SET @AUTHORIZATION_TYPE = 0 -- Neutral
------------------------------------------------------
-- CHECK ACCESS ON PARENTS
-- Get Items Where Item is A Member
DECLARE ItemsWhereIAmAMember_cur CURSOR LOCAL FAST_FORWARD READ_ONLY FOR SELECT MemberOfItemId FROM dbo.[netsqlazman_ItemsHierarchyTable] WHERE ItemId = @ITEMID
OPEN ItemsWhereIAmAMember_cur
FETCH NEXT FROM ItemsWhereIAmAMember_cur INTO @PARENTITEMID
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Recursive Call
	EXEC dbo.[netsqlazman_CheckAccess] @PARENTITEMID, @USERSID, @VALIDFOR, @LDAPPATH, @PARENTRESULT OUTPUT, @NETSQLAZMANMODE, @RETRIEVEATTRIBUTES
	SELECT @AUTHORIZATION_TYPE = dbo.[netsqlazman_MergeAuthorizations](@AUTHORIZATION_TYPE, @PARENTRESULT)
	FETCH NEXT FROM ItemsWhereIAmAMember_cur INTO @PARENTITEMID
END
CLOSE ItemsWhereIAmAMember_cur
DEALLOCATE ItemsWhereIAmAMember_cur

IF @AUTHORIZATION_TYPE = 3 
BEGIN
	SET @AUTHORIZATION_TYPE = 1 -- AllowWithDelegation becomes Just Allow (if comes from parents)
END
---------------------------------------------
-- GET ITEM ATTRIBUTES
---------------------------------------------
IF @RETRIEVEATTRIBUTES = 1
	INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, @ITEMID FROM dbo.[netsqlazman_ItemAttributesTable] WHERE ItemId = @ITEMID
---------------------------------------------
-- CHECK ACCESS ON ITEM
-- AuthorizationType can be:  0 - Neutral; 1 - Allow; 2 - Deny; 3 - AllowWithDelegation
-- objectSidWhereDefined can be:0 - Store; 1 - Application; 2 - LDAP; 3 - Local; 4 - Database
DECLARE @PARTIAL_RESULT TINYINT
--CHECK ACCESS FOR USER AUTHORIZATIONS
DECLARE checkaccessonitem_cur CURSOR  LOCAL FAST_FORWARD READ_ONLY FOR 
	SELECT AuthorizationType, AuthorizationId
	FROM dbo.[netsqlazman_AuthorizationsTable] WHERE 
	ItemId = @ITEMID AND
	objectSid = @USERSID AND
	(ValidFrom IS NULL AND ValidTo IS NULL OR
	@VALIDFOR >= ValidFrom  AND ValidTo IS NULL OR
	@VALIDFOR <= ValidTo AND ValidFrom IS NULL OR
	@VALIDFOR BETWEEN ValidFrom AND ValidTo) AND
        AuthorizationType<>0 AND
	((@NETSQLAZMANMODE = 0 AND (objectSidWhereDefined=2 OR objectSidWhereDefined=4)) OR (@NETSQLAZMANMODE = 1 AND objectSidWhereDefined BETWEEN 2 AND 4)) -- if Mode = Administrator SKIP CHECK for local Authorizations

OPEN checkaccessonitem_cur
FETCH NEXT FROM checkaccessonitem_cur INTO @PARTIAL_RESULT, @PKID
WHILE @@FETCH_STATUS = 0
BEGIN
	--CHECK FOR DENY
	IF @PARTIAL_RESULT IS NOT NULL
	BEGIN
		SELECT @AUTHORIZATION_TYPE = dbo.[netsqlazman_MergeAuthorizations](@AUTHORIZATION_TYPE, @PARTIAL_RESULT)
		SELECT @ITEM_AUTHORIZATION_TYPE  = dbo.[netsqlazman_MergeAuthorizations](@ITEM_AUTHORIZATION_TYPE, @PARTIAL_RESULT)
		IF @RETRIEVEATTRIBUTES = 1 
			INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, NULL FROM dbo.[netsqlazman_AuthorizationAttributesTable] WHERE AuthorizationId = @PKID
	END
	ELSE
	BEGIN
		SET @PARTIAL_RESULT = 0 -- NEUTRAL
	END
	FETCH NEXT FROM checkaccessonitem_cur INTO @PARTIAL_RESULT, @PKID
END

CLOSE checkaccessonitem_cur
DEALLOCATE checkaccessonitem_cur

--CHECK ACCESS FOR USER GROUPS AUTHORIZATIONS
DECLARE usergroupsauthz_cur CURSOR LOCAL FAST_FORWARD READ_ONLY FOR 
	SELECT AuthorizationType, AuthorizationID
	FROM dbo.[netsqlazman_AuthorizationsTable] Authorizations INNER JOIN #USERGROUPS as usergroups
	ON Authorizations.objectSid = usergroups.objectSid WHERE 
	ItemId = @ITEMID AND
	(ValidFrom IS NULL AND ValidTo IS NULL OR
	@VALIDFOR >= ValidFrom  AND ValidTo IS NULL OR
	@VALIDFOR <= ValidTo AND ValidFrom IS NULL OR
	@VALIDFOR BETWEEN ValidFrom AND ValidTo) AND
        AuthorizationType<>0 AND
	((@NETSQLAZMANMODE = 0 AND (objectSidWhereDefined=2 OR objectSidWhereDefined=4)) OR (@NETSQLAZMANMODE = 1 AND objectSidWhereDefined BETWEEN 2 AND 4)) -- if Mode = Administrator SKIP CHECK for local Authorizations

OPEN usergroupsauthz_cur
FETCH NEXT FROM usergroupsauthz_cur INTO @PARTIAL_RESULT, @PKID
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @PARTIAL_RESULT IS NOT NULL
	BEGIN
		SELECT @AUTHORIZATION_TYPE = dbo.[netsqlazman_MergeAuthorizations](@AUTHORIZATION_TYPE, @PARTIAL_RESULT)
		SELECT @ITEM_AUTHORIZATION_TYPE = dbo.[netsqlazman_MergeAuthorizations](@ITEM_AUTHORIZATION_TYPE, @PARTIAL_RESULT)
		IF @RETRIEVEATTRIBUTES = 1
			INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, NULL FROM dbo.[netsqlazman_AuthorizationAttributesTable] WHERE AuthorizationId = @PKID
	END
	ELSE
	BEGIN
		SET @PARTIAL_RESULT = 0 -- NEUTRAL
	END
	FETCH NEXT FROM usergroupsauthz_cur INTO @PARTIAL_RESULT, @PKID
END

CLOSE usergroupsauthz_cur
DEALLOCATE usergroupsauthz_cur

--CHECK ACCESS FOR STORE/APPLICATION GROUPS AUTHORIZATIONS
DECLARE @GROUPOBJECTSID VARBINARY(85)
DECLARE @GROUPWHEREDEFINED TINYINT
DECLARE @GROUPSIDMEMBERS table (objectSid VARBINARY(85))
DECLARE @ISMEMBER BIT
SET @ISMEMBER = 1
DECLARE groups_authorizations_cur CURSOR LOCAL FAST_FORWARD READ_ONLY 
FOR 	SELECT objectSid, objectSidWhereDefined, AuthorizationType, AuthorizationId FROM dbo.[netsqlazman_AuthorizationsTable]
	WHERE ItemId = @ITEMID AND objectSidWhereDefined BETWEEN 0 AND 1 AND
        AuthorizationType<>0 AND
	(ValidFrom IS NULL AND ValidTo IS NULL OR
	@VALIDFOR >= ValidFrom  AND ValidTo IS NULL OR
	@VALIDFOR <= ValidTo AND ValidFrom IS NULL OR
	@VALIDFOR BETWEEN ValidFrom AND ValidTo)

OPEN groups_authorizations_cur
FETCH NEXT FROM groups_authorizations_cur INTO @GROUPOBJECTSID, @GROUPWHEREDEFINED, @PARTIAL_RESULT, @PKID
WHILE @@FETCH_STATUS=0
BEGIN
SET @ISMEMBER = 1
--check if user is a non-member
IF @GROUPWHEREDEFINED = 0 -- store group members
BEGIN
--store groups members of type ''non-member''
	DELETE FROM @GROUPSIDMEMBERS

	EXEC dbo.[netsqlazman_GetStoreGroupSidMembers] 0, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @members_cur OUTPUT -- non-members
	FETCH NEXT FROM @members_cur INTO @OBJECTSID
	WHILE @@FETCH_STATUS=0
	BEGIN
		INSERT INTO @GROUPSIDMEMBERS VALUES (@OBJECTSID)
		FETCH NEXT FROM @members_cur INTO @OBJECTSID
	END
	CLOSE @members_cur
	DEALLOCATE @members_cur

	IF EXISTS(SELECT * FROM @GROUPSIDMEMBERS WHERE objectSid = @USERSID) OR
	     EXISTS(SELECT * FROM @GROUPSIDMEMBERS groupsidmembers INNER JOIN #USERGROUPS as usergroups ON groupsidmembers.objectSid = usergroups.objectSid)
	BEGIN
	-- user is a non-member
	SET @ISMEMBER = 0
	END
	IF @ISMEMBER = 1
	BEGIN
		DELETE FROM @GROUPSIDMEMBERS

		EXEC dbo.[netsqlazman_GetStoreGroupSidMembers] 1, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @members_cur OUTPUT -- members
		FETCH NEXT FROM @members_cur INTO @OBJECTSID
		WHILE @@FETCH_STATUS=0
		BEGIN
			INSERT INTO @GROUPSIDMEMBERS VALUES (@OBJECTSID)
			FETCH NEXT FROM @members_cur INTO @OBJECTSID
		END
		CLOSE @members_cur
		DEALLOCATE @members_cur

		IF EXISTS (SELECT * FROM @GROUPSIDMEMBERS WHERE objectSid = @USERSID) OR
		     EXISTS (SELECT * FROM @GROUPSIDMEMBERS groupsidmembers INNER JOIN #USERGROUPS usergroups ON groupsidmembers.objectSid = usergroups.ObjectSId)
		BEGIN
		-- user is a member
		SET @ISMEMBER = 1
		END
		ELSE
		BEGIN
		-- user is not present
		SET @ISMEMBER = 0
		END
	END
	-- if a member ... get authorization
	IF @ISMEMBER = 1
	BEGIN
		SET @AUTHORIZATION_TYPE = (SELECT dbo.[netsqlazman_MergeAuthorizations](@AUTHORIZATION_TYPE, @PARTIAL_RESULT))
		SET @ITEM_AUTHORIZATION_TYPE = (SELECT dbo.[netsqlazman_MergeAuthorizations](@ITEM_AUTHORIZATION_TYPE, @PARTIAL_RESULT))
		IF @PKID IS NOT NULL AND @RETRIEVEATTRIBUTES = 1
			INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, NULL FROM dbo.[netsqlazman_AuthorizationAttributesTable] WHERE AuthorizationId = @PKID
	END
END
	ELSE
IF @GROUPWHEREDEFINED = 1 -- application group members
BEGIN
	--application groups members of type ''non-member''
	DELETE FROM @GROUPSIDMEMBERS

	EXEC dbo.[netsqlazman_GetApplicationGroupSidMembers] 0, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @members_cur OUTPUT -- non-members
	FETCH NEXT FROM @members_cur INTO @OBJECTSID
	WHILE @@FETCH_STATUS=0
	BEGIN
		INSERT INTO @GROUPSIDMEMBERS VALUES (@OBJECTSID)
		FETCH NEXT FROM @members_cur INTO @OBJECTSID
	END
	CLOSE @members_cur
	DEALLOCATE @members_cur

	IF EXISTS(SELECT * FROM @GROUPSIDMEMBERS WHERE objectSid = @USERSID) OR
	     EXISTS (SELECT* FROM @GROUPSIDMEMBERS groupsidmembers INNER JOIN #USERGROUPS as usergroups ON groupsidmembers.objectSid = usergroups.objectSid)
	BEGIN	-- user is a non-member
	SET @ISMEMBER = 0
	END
	IF @ISMEMBER = 1 
	BEGIN
		DELETE FROM @GROUPSIDMEMBERS

		EXEC dbo.[netsqlazman_GetApplicationGroupSidMembers] 1, @GROUPOBJECTSID, @NETSQLAZMANMODE, @LDAPPATH, @members_cur OUTPUT -- members
		FETCH NEXT FROM @members_cur INTO @OBJECTSID
		WHILE @@FETCH_STATUS=0
		BEGIN
			INSERT INTO @GROUPSIDMEMBERS VALUES (@OBJECTSID)
			FETCH NEXT FROM @members_cur INTO @OBJECTSID
		END
		CLOSE @members_cur
		DEALLOCATE @members_cur

		IF EXISTS(SELECT * FROM @GROUPSIDMEMBERS WHERE objectSid = @USERSID) OR
		     EXISTS (SELECT * FROM @GROUPSIDMEMBERS groupsidmembers INNER JOIN #USERGROUPS as usergroups ON groupsidmembers.objectSid = usergroups.objectSid)
		BEGIN
		-- user is a member
		SET @ISMEMBER = 1
		END
		ELSE
		BEGIN
		-- user is not present
		SET @ISMEMBER = 0
		END
	END
	-- if a member ... get authorization
	IF @ISMEMBER = 1
	BEGIN
		SET @AUTHORIZATION_TYPE = (SELECT dbo.[netsqlazman_MergeAuthorizations](@AUTHORIZATION_TYPE, @PARTIAL_RESULT))
		SET @ITEM_AUTHORIZATION_TYPE = (SELECT dbo.[netsqlazman_MergeAuthorizations](@ITEM_AUTHORIZATION_TYPE, @PARTIAL_RESULT))
		IF @PKID IS NOT NULL AND @RETRIEVEATTRIBUTES = 1 
			INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, NULL FROM dbo.[netsqlazman_AuthorizationAttributesTable] WHERE AuthorizationId = @PKID
	END
END
	FETCH NEXT FROM groups_authorizations_cur INTO @GROUPOBJECTSID, @GROUPWHEREDEFINED, @PARTIAL_RESULT, @PKID
END
CLOSE groups_authorizations_cur
DEALLOCATE groups_authorizations_cur

-- PREPARE RESULTSET FOR BIZ RULE CHECKING
----------------------------------------------------------------------------------------
INSERT INTO #PARTIAL_RESULTS_TABLE 
SELECT     Items.ItemId, Items.Name, Items.ItemType, @ITEM_AUTHORIZATION_TYPE,BizRules.BizRuleId, BizRules.BizRuleSource, BizRules.BizRuleLanguage
FROM         dbo.[netsqlazman_ItemsTable] Items LEFT OUTER JOIN
                      dbo.[netsqlazman_BizRulesTable] BizRules ON Items.BizRuleId = BizRules.BizRuleId WHERE Items.ItemId = @ITEMID
SET NOCOUNT OFF
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_LogTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_LogTable](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[LogDateTime] [datetime] NOT NULL,
	[WindowsIdentity] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[SqlIdentity] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[MachineName] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[InstanceGuid] [uniqueidentifier] NOT NULL,
	[TransactionGuid] [uniqueidentifier] NULL,
	[OperationCounter] [int] NOT NULL,
	[ENSType] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[ENSDescription] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[LogType] [char](1) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY NONCLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]') AND name = N'IX_Log_2')
CREATE CLUSTERED INDEX [IX_Log_2] ON [dbo].[netsqlazman_LogTable] 
(
	[LogDateTime] DESC,
	[InstanceGuid] ASC,
	[OperationCounter] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]') AND name = N'IX_Log')
CREATE NONCLUSTERED INDEX [IX_Log] ON [dbo].[netsqlazman_LogTable] 
(
	[WindowsIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]') AND name = N'IX_Log_1')
CREATE NONCLUSTERED INDEX [IX_Log_1] ON [dbo].[netsqlazman_LogTable] 
(
	[SqlIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[netsqlazman_Settings]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_Settings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_Settings](
	[SettingName] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[SettingValue] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[SettingName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[netsqlazman_Settings] ([SettingName], [SettingValue]) VALUES (N'LogErrors', N'False')
INSERT [dbo].[netsqlazman_Settings] ([SettingName], [SettingValue]) VALUES (N'LogInformations', N'False')
INSERT [dbo].[netsqlazman_Settings] ([SettingName], [SettingValue]) VALUES (N'LogOnDb', N'False')
INSERT [dbo].[netsqlazman_Settings] ([SettingName], [SettingValue]) VALUES (N'LogOnEventLog', N'False')
INSERT [dbo].[netsqlazman_Settings] ([SettingName], [SettingValue]) VALUES (N'LogWarnings', N'False')
INSERT [dbo].[netsqlazman_Settings] ([SettingName], [SettingValue]) VALUES (N'Mode', N'Developer')
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_IAmAdmin]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_IAmAdmin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_IAmAdmin] ()  
RETURNS bit AS  
BEGIN 
DECLARE @result bit
IF IS_MEMBER(''db_owner'')=1 OR IS_MEMBER(''NetSqlAzMan_Administrators'')=1
	SET @result = 1
ELSE
	SET @result = 0
RETURN @result
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_helplogins]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_helplogins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_helplogins](@rolename nvarchar(128))
AS

CREATE TABLE #temptable (
	[DBRole] sysname NOT NULL ,
	[MemberName] sysname NOT NULL ,
	[MemberSid] varbinary(85) NULL
	)

IF @rolename = ''NetSqlAzMan_Managers''
BEGIN
	INSERT INTO #temptable EXEC sp_helprolemember ''NetSqlAzMan_Managers''
END

IF @rolename = ''NetSqlAzMan_Users'' 
BEGIN
	INSERT INTO #temptable EXEC sp_helprolemember ''NetSqlAzMan_Managers''
	INSERT INTO #temptable EXEC sp_helprolemember ''NetSqlAzMan_Users''
END

IF @rolename = ''NetSqlAzMan_Readers'' 
BEGIN
	INSERT INTO #temptable EXEC sp_helprolemember ''NetSqlAzMan_Managers''
	INSERT INTO #temptable EXEC sp_helprolemember ''NetSqlAzMan_Users''
	INSERT INTO #temptable EXEC sp_helprolemember ''NetSqlAzMan_Readers''
END

SELECT DISTINCT SUSER_SNAME(MemberSid) SqlUserOrRole, CASE MemberSid WHEN NULL THEN 1 ELSE 0 END AS IsSqlRole
FROM #temptable
WHERE MemberName NOT IN (''NetSqlAzMan_Administrators'', ''NetSqlAzMan_Managers'', ''NetSqlAzMan_Users'', ''NetSqlAzMan_Readers'')
AND SUSER_SNAME(MemberSid) IS NOT NULL
ORDER BY SUSER_SNAME(MemberSid)

DROP TABLE #temptable
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + '' '' + @action + '' on '' + @object + '' TO ['' + @grantee + '']''
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = ''EXEC sp_droprolemember '' + '''''''' + @name + '''''', '''''' + USER_NAME(@user_id) + ''''''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END
' 
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
 CONSTRAINT [PK__aspnet_S__5A1E6BC130C33EC3] PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BizRuleInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_BizRuleInsert]
(
	@BizRuleSource text,
	@BizRuleLanguage tinyint,
	@CompiledAssembly image
)
AS
INSERT INTO [dbo].[netsqlazman_BizRulesTable] ([BizRuleSource], [BizRuleLanguage], [CompiledAssembly]) VALUES (@BizRuleSource, @BizRuleLanguage, @CompiledAssembly);
RETURN SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_StoreAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_StoreAttributesTable](
	[StoreAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NOT NULL,
	[AttributeKey] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[AttributeValue] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_StoreAttributes] PRIMARY KEY CLUSTERED 
(
	[StoreAttributeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]') AND name = N'IX_StoreAttributes')
CREATE NONCLUSTERED INDEX [IX_StoreAttributes] ON [dbo].[netsqlazman_StoreAttributesTable] 
(
	[StoreId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]') AND name = N'StoreAttributes_AuhorizationId_AttributeKey_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [StoreAttributes_AuhorizationId_AttributeKey_Unique_Index] ON [dbo].[netsqlazman_StoreAttributesTable] 
(
	[StoreId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
  '
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreInsert]
(
	@Name nvarchar(255),
	@Description nvarchar(1024)
)
AS
INSERT INTO [dbo].[netsqlazman_StoresTable] ([Name], [Description]) VALUES (@Name, @Description);
RETURN SCOPE_IDENTITY()
' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END
' 
END
GO
/****** Object:  Table [dbo].[netsqlazman_ApplicationAttributesTable]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[netsqlazman_ApplicationAttributesTable](
	[ApplicationAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[AttributeKey] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[AttributeValue] [nvarchar](4000) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_ApplicationAttributes] PRIMARY KEY CLUSTERED 
(
	[ApplicationAttributeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]') AND name = N'ApplicationAttributes_AuhorizationId_AttributeKey_Unique_Index')
CREATE UNIQUE NONCLUSTERED INDEX [ApplicationAttributes_AuhorizationId_AttributeKey_Unique_Index] ON [dbo].[netsqlazman_ApplicationAttributesTable] 
(
	[ApplicationId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]') AND name = N'IX_ApplicationAttributes')
CREATE NONCLUSTERED INDEX [IX_ApplicationAttributes] ON [dbo].[netsqlazman_ApplicationAttributesTable] 
(
	[ApplicationId] ASC,
	[AttributeKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_UsersInRoles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N''aspnet_Membership''
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Roles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N''aspnet_Roles''
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N''aspnet_Profile''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N''aspnet_PersonalizationPerUser''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''aspnet_WebEvent_LogEvent'') AND (type = ''P''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N''aspnet_WebEvent_Events''
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N''aspnet_Users''
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N''aspnet_Applications''
            RETURN
        END
    END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
    WHERE @UserId = UserId

    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreGroupUpdate]
(
	@StoreId int,
	@objectSid varbinary(85),
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@LDapQuery nvarchar(4000),
	@GroupType tinyint,
	@Original_StoreGroupId int
)
AS
IF dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	UPDATE [dbo].[netsqlazman_StoreGroupsTable] SET [objectSid] = @objectSid, [Name] = @Name, [Description] = @Description, [LDapQuery] = @LDapQuery, [GroupType] = @GroupType WHERE [StoreGroupId] = @Original_StoreGroupId AND StoreId = @StoreId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupMemberUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMemberUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreGroupMemberUpdate]
(
	@StoreId int,
	@StoreGroupId int,
	@objectSid varbinary(85),
	@WhereDefined tinyint,
	@IsMember bit,
	@Original_StoreGroupMemberId int
)
AS
IF EXISTS(SELECT StoreGroupMemberId FROM dbo.[netsqlazman_StoreGroupMembers]() WHERE StoreGroupMemberId = @Original_StoreGroupMemberId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	UPDATE [dbo].[netsqlazman_StoreGroupMembersTable] SET [StoreGroupId] = @StoreGroupId, [objectSid] = @objectSid, [WhereDefined] = @WhereDefined, [IsMember] = @IsMember WHERE [StoreGroupMemberId] = @Original_StoreGroupMemberId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupMemberInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMemberInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreGroupMemberInsert]
(
	@StoreId int,
	@StoreGroupId int,
	@objectSid varbinary(85),
	@WhereDefined tinyint,
	@IsMember bit
)
AS
IF EXISTS(SELECT StoreGroupId FROM dbo.[netsqlazman_StoreGroups]() WHERE StoreGroupId = @StoreGroupId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_StoreGroupMembersTable] ([StoreGroupId], [objectSid], [WhereDefined], [IsMember]) VALUES (@StoreGroupId, @objectSid, @WhereDefined, @IsMember)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupMemberDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMemberDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreGroupMemberDelete]
(
	@StoreId int,
	@StoreGroupMemberId int
)
AS
IF EXISTS(SELECT StoreGroupMemberId FROM dbo.[netsqlazman_StoreGroupMembers]() WHERE StoreGroupMemberId = @StoreGroupMemberId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_StoreGroupMembersTable] WHERE [StoreGroupMemberId] = @StoreGroupMemberId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreGroupInsert]
(
	@StoreId int,
	@objectSid varbinary(85),
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@LDapQuery nvarchar(4000),
	@GroupType tinyint
)
AS
IF dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_StoreGroupsTable] ([StoreId], [objectSid], [Name], [Description], [LDapQuery], [GroupType]) VALUES (@StoreId, @objectSid, @Name, @Description, @LDapQuery, @GroupType);
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreGroupDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreGroupDelete]
(
	@Original_StoreGroupId int,
	@StoreId int
)
AS
IF dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_StoreGroupsTable] WHERE [StoreGroupId] = @Original_StoreGroupId AND [StoreId] = @StoreId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ReloadBizRule]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ReloadBizRule]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ReloadBizRule]
(
	@ItemId int,
	@BizRuleId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ItemsTable] SET BizRuleId = @BizRuleId WHERE [ItemId] = @ItemId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemUpdate]
(
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@ItemType tinyint,
	@Original_ItemId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @Original_ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ItemsTable] SET [Name] = @Name, [Description] = @Description, [ItemType] = @ItemType WHERE [ItemId] = @Original_ItemId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemsHierarchyInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemsHierarchyInsert]
(
	@ItemId int,
	@MemberOfItemId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ItemsHierarchyTable] ([ItemId], [MemberOfItemId]) VALUES (@ItemId, @MemberOfItemId)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemsHierarchyDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemsHierarchyDelete]
(
	@ItemId int,
	@MemberOfItemId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ItemsHierarchyTable] WHERE [ItemId] = @ItemId AND [MemberOfItemId] = @MemberOfItemId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemDelete]
(
	@ItemId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ItemsTable] WHERE [ItemId] = @ItemId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributeUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemAttributeUpdate]
(
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000),
	@Original_ItemAttributeId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemAttributeId FROM dbo.[netsqlazman_ItemAttributes]() WHERE ItemAttributeId = @Original_ItemAttributeId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ItemAttributesTable] SET [AttributeKey] = @AttributeKey, [AttributeValue] = @AttributeValue WHERE [ItemAttributeId] = @Original_ItemAttributeId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributeInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemAttributeInsert]
(
	@ItemId int,
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000),
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ItemAttributesTable] ([ItemId], [AttributeKey], [AttributeValue]) VALUES (@ItemId, @AttributeKey, @AttributeValue)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributeDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemAttributeDelete]
(
	@ItemAttributeId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemAttributeId FROM dbo.[netsqlazman_ItemAttributes]() WHERE ItemAttributeId = @ItemAttributeId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ItemAttributesTable] WHERE [ItemAttributeId] = @ItemAttributeId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_DeleteDelegate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DeleteDelegate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_DeleteDelegate](@AUTHORIZATIONID INT, @OWNERSID VARBINARY(85))
AS
DECLARE @ApplicationId int
SELECT @ApplicationId = Items.ApplicationId FROM dbo.[netsqlazman_Items]() Items INNER JOIN dbo.[netsqlazman_Authorizations]() Authorizations ON Items.ItemId = Authorizations.ItemId WHERE Authorizations.AuthorizationId = @AUTHORIZATIONID
IF @ApplicationId IS NOT NULL AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 1) = 1
	DELETE FROM dbo.[netsqlazman_AuthorizationsTable] WHERE AuthorizationId = @AUTHORIZATIONID AND ownerSid = @OWNERSID
ELSE
	RAISERROR (''Item NOT Found or Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_CreateDelegate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_CreateDelegate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  Stored Procedure dbo.CreateDelegate    Script Date: 19/05/2006 19.11.19 ******/
CREATE PROCEDURE [dbo].[netsqlazman_CreateDelegate](@ITEMID INT, @OWNERSID VARBINARY(85), @OWNERSIDWHEREDEFINED TINYINT, @DELEGATEDUSERSID VARBINARY(85), @SIDWHEREDEFINED TINYINT, @AUTHORIZATIONTYPE TINYINT, @VALIDFROM DATETIME, @VALIDTO DATETIME, @AUTHORIZATIONID INT OUTPUT)
AS
DECLARE @ApplicationId int
SELECT @ApplicationId = ApplicationId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId
IF @ApplicationId IS NOT NULL AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 1) = 1
BEGIN
	INSERT INTO dbo.[netsqlazman_AuthorizationsTable] (ItemId, ownerSid, ownerSidWhereDefined, objectSid, objectSidWhereDefined, AuthorizationType, ValidFrom, ValidTo)
		VALUES (@ITEMID, @OWNERSID, @OWNERSIDWHEREDEFINED, @DELEGATEDUSERSID, @SIDWHEREDEFINED, @AUTHORIZATIONTYPE, @VALIDFROM, @VALIDTO)
	SET @AUTHORIZATIONID = SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Item NOT Found or Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ClearBizRule]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ClearBizRule]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ClearBizRule]
(
	@ItemId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ItemsTable] SET BizRuleId = NULL WHERE [ItemId] = @ItemId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BizRuleUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_BizRuleUpdate]
(
	@BizRuleSource text,
	@BizRuleLanguage tinyint,
	@CompiledAssembly image,
	@Original_BizRuleId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT BizRuleId FROM dbo.[netsqlazman_BizRules]() WHERE BizRuleId = @Original_BizRuleId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_BizRulesTable] SET [BizRuleSource] = @BizRuleSource, [BizRuleLanguage] = @BizRuleLanguage, [CompiledAssembly] = @CompiledAssembly WHERE [BizRuleId] = @Original_BizRuleId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_AuthorizationUpdate]
(
	@ItemId int,
	@ownerSid varbinary(85),
	@ownerSidWhereDefined tinyint,
	@objectSid varbinary(85),
	@objectSidWhereDefined tinyint,
	@AuthorizationType tinyint,
	@ValidFrom datetime,
	@ValidTo datetime,
	@Original_AuthorizationId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT AuthorizationId FROM dbo.[netsqlazman_Authorizations]() WHERE AuthorizationId = @Original_AuthorizationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_AuthorizationsTable] SET [ownerSid] = @ownerSid, [ownerSidWhereDefined] = @ownerSidWhereDefined, [objectSid] = @objectSid, [objectSidWhereDefined] = @objectSidWhereDefined, [AuthorizationType] = @AuthorizationType, [ValidFrom] = @ValidFrom, [ValidTo] = @ValidTo WHERE [AuthorizationId] = @Original_AuthorizationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_AuthorizationInsert]
(
	@ItemId int,
	@ownerSid varbinary(85),
	@ownerSidWhereDefined tinyint,
	@objectSid varbinary(85),
	@objectSidWhereDefined tinyint,
	@AuthorizationType tinyint,
	@ValidFrom datetime,
	@ValidTo datetime,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ItemId FROM dbo.[netsqlazman_Items]() WHERE ItemId = @ItemId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_AuthorizationsTable] ([ItemId], [ownerSid], [ownerSidWhereDefined], [objectSid], [objectSidWhereDefined], [AuthorizationType], [ValidFrom], [ValidTo]) VALUES (@ItemId, @ownerSid, @ownerSidWhereDefined, @objectSid, @objectSidWhereDefined, @AuthorizationType, @ValidFrom, @ValidTo)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_AuthorizationDelete]
(
	@AuthorizationId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT AuthorizationId FROM dbo.[netsqlazman_Authorizations]() WHERE AuthorizationId = @AuthorizationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_AuthorizationsTable] WHERE [AuthorizationId] = @AuthorizationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributeUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_AuthorizationAttributeUpdate]
(
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000),
	@Original_AuthorizationAttributeId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT AuthorizationAttributeId FROM dbo.[netsqlazman_AuthorizationAttributes]() WHERE AuthorizationAttributeId = @Original_AuthorizationAttributeId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 1) = 1
	UPDATE [dbo].[netsqlazman_AuthorizationAttributesTable] SET [AttributeKey] = @AttributeKey, [AttributeValue] = @AttributeValue WHERE [AuthorizationAttributeId] = @Original_AuthorizationAttributeId
ELSE
	RAISERROR (''Application permission denied.'', 16 ,1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributeInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_AuthorizationAttributeInsert]
(
	@AuthorizationId int,
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000),
	@ApplicationId int
)
AS
IF EXISTS(SELECT AuthorizationId FROM dbo.[netsqlazman_Authorizations]() WHERE AuthorizationId = @AuthorizationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 1) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_AuthorizationAttributesTable] ([AuthorizationId], [AttributeKey], [AttributeValue]) VALUES (@AuthorizationId, @AttributeKey, @AttributeValue)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_AuthorizationAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributeDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_AuthorizationAttributeDelete]
(
	@AuthorizationAttributeId int,
	@ApplicationId int
)
AS
IF  EXISTS(SELECT AuthorizationAttributeId FROM dbo.[netsqlazman_AuthorizationAttributes]() WHERE AuthorizationAttributeId = @AuthorizationAttributeId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 1) = 1
	DELETE FROM [dbo].[netsqlazman_AuthorizationAttributesTable] WHERE [AuthorizationAttributeId] = @AuthorizationAttributeId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationGroupUpdate]
(
	@objectSid varbinary(85),
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@LDapQuery nvarchar(4000),
	@GroupType tinyint,
	@Original_ApplicationGroupId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationGroupId FROM dbo.[netsqlazman_ApplicationGroups]() WHERE ApplicationGroupId = @Original_ApplicationGroupId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ApplicationGroupsTable] SET [objectSid] = @objectSid, [Name] = @Name, [Description] = @Description, [LDapQuery] = @LDapQuery, [GroupType] = @GroupType WHERE [ApplicationGroupId] = @Original_ApplicationGroupId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupMemberUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMemberUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationGroupMemberUpdate]
(
	@ApplicationGroupId int,
	@objectSid varbinary(85),
	@WhereDefined tinyint,
	@IsMember bit,
	@Original_ApplicationGroupMemberId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationGroupMemberId FROM dbo.[netsqlazman_ApplicationGroupMembers]() WHERE ApplicationGroupMemberId = @Original_ApplicationGroupMemberId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ApplicationGroupMembersTable] SET [objectSid] = @objectSid, [WhereDefined] = @WhereDefined, [IsMember] = @IsMember WHERE [ApplicationGroupMemberId] = @Original_ApplicationGroupMemberId
ELSE	
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupMemberInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMemberInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationGroupMemberInsert]
(
	@ApplicationGroupId int,
	@objectSid varbinary(85),
	@WhereDefined tinyint,
	@IsMember bit,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationGroupId FROM dbo.[netsqlazman_ApplicationGroups]() WHERE ApplicationGroupId = @ApplicationGroupId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ApplicationGroupMembersTable] ([ApplicationGroupId], [objectSid], [WhereDefined], [IsMember]) VALUES (@ApplicationGroupId, @objectSid, @WhereDefined, @IsMember)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupMemberDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMemberDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationGroupMemberDelete]
(
	@ApplicationGroupMemberId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationGroupMemberId FROM dbo.[netsqlazman_ApplicationGroupMembers]() WHERE ApplicationGroupMemberId = @ApplicationGroupMemberId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ApplicationGroupMembersTable] WHERE [ApplicationGroupMemberId] = @ApplicationGroupMemberId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationGroupDelete]
(
	@ApplicationGroupId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationGroupId FROM dbo.[netsqlazman_ApplicationGroups]() WHERE ApplicationGroupId = @ApplicationGroupId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ApplicationGroupsTable] WHERE [ApplicationGroupId] = @ApplicationGroupId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_DatabaseUsers]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DatabaseUsers]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_DatabaseUsers]
AS
SELECT     *
FROM         dbo.[netsqlazman_GetDBUsers](NULL, NULL, DEFAULT, DEFAULT) GetDBUsers
'
GO
/****** Object:  View [dbo].[netsqlazman_StoreGroupMembersView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_StoreGroupMembersView]
AS
SELECT     StoreGroupMembers.StoreGroupMemberId, StoreGroupMembers.StoreGroupId, StoreGroups.Name AS StoreGroup, dbo.[netsqlazman_GetNameFromSid](Stores.Name, NULL, 
                      StoreGroupMembers.objectSid, StoreGroupMembers.WhereDefined) AS Name, StoreGroupMembers.objectSid, 
                      CASE WhereDefined WHEN 0 THEN ''Store'' WHEN 1 THEN ''Application'' WHEN 2 THEN ''LDap'' WHEN 3 THEN ''Local'' WHEN 4 THEN ''DATABASE'' END AS WhereDefined,
                       CASE IsMember WHEN 1 THEN ''Member'' WHEN 0 THEN ''Non-Member'' END AS MemberType
FROM         dbo.[netsqlazman_StoreGroupMembers]() StoreGroupMembers INNER JOIN
                      dbo.[netsqlazman_StoreGroups]() StoreGroups ON StoreGroupMembers.StoreGroupId = StoreGroups.StoreGroupId INNER JOIN
                      dbo.[netsqlazman_Stores]() Stores ON StoreGroups.StoreId = Stores.StoreId
'
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_RevokeStoreAccess]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_RevokeStoreAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_RevokeStoreAccess] (
	@StoreId int,
	@SqlUserOrRole sysname,
	@NetSqlAzManFixedServerRole tinyint)
AS
IF EXISTS(SELECT StoreId FROM dbo.[netsqlazman_StoresTable] WHERE StoreId = @StoreId) AND (dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1 AND @NetSqlAzManFixedServerRole BETWEEN 0 AND 1 OR (IS_MEMBER(''db_owner'')=1 OR IS_MEMBER(''NetSqlAzMan_Administrators'')=1) AND @NetSqlAzManFixedServerRole = 2)
BEGIN
	IF EXISTS(SELECT * FROM dbo.[netsqlazman_StorePermissionsTable] WHERE StoreId = @StoreId AND SqlUserOrRole = @SqlUserOrRole AND NetSqlAzManFixedServerRole = @NetSqlAzManFixedServerRole)
		DELETE FROM dbo.[netsqlazman_StorePermissionsTable] WHERE StoreId = @StoreId AND SqlUserOrRole = @SqlUserOrRole AND NetSqlAzManFixedServerRole = @NetSqlAzManFixedServerRole
	ELSE
		RAISERROR (''Permission not found. Revoke Store Access ignored.'', -1, -1)
END
ELSE
	RAISERROR (''Store NOT Found or Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BizRuleDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BizRuleDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_BizRuleDelete]
(
	@BizRuleId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT BizRuleId FROM dbo.[netsqlazman_BizRulesTable] WHERE BizRuleId = @BizRuleId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_BizRulesTable] WHERE [BizRuleId] = @BizRuleId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GrantStoreAccess]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GrantStoreAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_GrantStoreAccess] (
	@StoreId int,
	@SqlUserOrRole sysname,
	@NetSqlAzManFixedServerRole tinyint)
AS
IF EXISTS(SELECT StoreId FROM dbo.[netsqlazman_StoresTable] WHERE StoreId = @StoreId) AND (dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1 AND @NetSqlAzManFixedServerRole BETWEEN 0 AND 1 OR (IS_MEMBER(''db_owner'')=1 OR IS_MEMBER(''NetSqlAzMan_Administrators'')=1) AND @NetSqlAzManFixedServerRole = 2)
BEGIN
	DECLARE @MEMBERUID int
	IF NOT (@NetSqlAzManFixedServerRole BETWEEN 0 AND 2)
	BEGIN
		RAISERROR (''NetSqlAzManFixedServerRole must be 0, 1 or 2 (Reader, User, Manager).'', 16, 1)
		RETURN -1
	END
	 -- CHECK MEMBER NAME (ATTEMPT ADDING IMPLICIT ROW FOR NT NAME) --
	DECLARE @IsSqlRoleInt int
	DECLARE @IsNtGroupInt bit
	DECLARE @IsSqlRole bit
	SELECT @MEMBERUID = uid, @IsSqlRoleInt = issqlrole, @IsNtGroupInt = isntgroup  from sysusers where sid = SUSER_SID(@SqlUserOrRole) and isaliased = 0
	IF @IsSqlRoleInt = 1 OR @IsNtGroupInt = 1
		SET @IsSqlRole = 1
	ELSE
		SET @IsSqlRole = 0
	IF @MEMBERUID IS NULL
	BEGIN
		RAISERROR (''Sql User/Role Not Found. Grant Store Access ignored.'', -1, -1)
		RETURN 0
	END
	IF EXISTS(SELECT * FROM dbo.[netsqlazman_StorePermissionsTable] WHERE StoreId = @StoreId AND SqlUserOrRole = @SqlUserOrRole AND NetSqlAzManFixedServerRole = @NetSqlAzManFixedServerRole)
		BEGIN
		RAISERROR (''NetSqlAzManFixedServerRole updated.'', -1, -1)
		RETURN 0
		END
	ELSE
		BEGIN
		INSERT INTO dbo.[netsqlazman_StorePermissionsTable] (StoreId, SqlUserOrRole, IsSqlRole, NetSqlAzManFixedServerRole) VALUES (@StoreId, @SqlUserOrRole, @IsSqlRole, @NetSqlAzManFixedServerRole)
		RETURN SCOPE_IDENTITY()
		END
END
ELSE
	RAISERROR (''Store NOT Found or Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_ItemsHierarchyView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsHierarchyView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_ItemsHierarchyView]
AS
SELECT     [netsqlazman_Items].ItemId, [netsqlazman_Items].ApplicationId, [netsqlazman_Items].Name, [netsqlazman_Items].Description, 
                      CASE [netsqlazman_Items].ItemType WHEN 0 THEN ''Role'' WHEN 1 THEN ''Task'' WHEN 2 THEN ''Operation'' END AS ItemType, Items_1.ItemId AS MemberItemId, 
                      Items_1.ApplicationId AS MemberApplicationId, Items_1.Name AS MemberName, Items_1.Description AS MemberDescription, 
                      CASE Items_1.ItemType WHEN 0 THEN ''Role'' WHEN 1 THEN ''Task'' WHEN 2 THEN ''Operation'' END AS MemberType
FROM         dbo.[netsqlazman_Items]() Items_1 INNER JOIN
                      dbo.[netsqlazman_ItemsHierarchy]() [netsqlazman_ItemsHierarchy] ON Items_1.ItemId = [netsqlazman_ItemsHierarchy].ItemId INNER JOIN
                      dbo.[netsqlazman_Items]() [netsqlazman_Items] ON [netsqlazman_ItemsHierarchy].MemberOfItemId = [netsqlazman_Items].ItemId INNER JOIN
                      dbo.[netsqlazman_Applications]() [netsqlazman_Applications] ON [netsqlazman_Items].ApplicationId = [netsqlazman_Applications].ApplicationId
'
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ItemInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ItemInsert]
(
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@ItemType tinyint,
	@BizRuleId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ItemsTable] ([ApplicationId], [Name], [Description], [ItemType], [BizRuleId]) VALUES (@ApplicationId, @Name, @Description, @ItemType, @BizRuleId)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_DirectCheckAccess]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_DirectCheckAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_DirectCheckAccess] (@STORENAME nvarchar(255), @APPLICATIONNAME nvarchar(255), @ITEMNAME nvarchar(255), @OPERATIONSONLY BIT, @TOKEN IMAGE, @USERGROUPSCOUNT INT, @VALIDFOR DATETIME, @LDAPPATH nvarchar(4000), @AUTHORIZATION_TYPE TINYINT OUTPUT, @RETRIEVEATTRIBUTES BIT) 
AS
--Memo: 0 - Role; 1 - Task; 2 - Operation
SET NOCOUNT ON
DECLARE @STOREID int
DECLARE @ApplicationId int
DECLARE @ITEMID INT

-- CHECK STORE EXISTANCE/PERMISSIONS
Select @STOREID = StoreId FROM dbo.[netsqlazman_Stores]() WHERE Name = @STORENAME
IF @STOREID IS NULL
	BEGIN
	RAISERROR (''Store not found or Store permission denied.'', 16, 1)
	RETURN 1
	END
-- CHECK APPLICATION EXISTANCE/PERMISSIONS
Select @ApplicationId = ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE Name = @APPLICATIONNAME And StoreId = @STOREID
IF @ApplicationId IS NULL
	BEGIN
	RAISERROR (''Application not found or Application permission denied.'', 16, 1)
	RETURN 1
	END

SELECT @ITEMID = Items.ItemId
	FROM         dbo.[netsqlazman_Applications]() Applications INNER JOIN
	                      dbo.[netsqlazman_Items]() Items ON Applications.ApplicationId = Items.ApplicationId INNER JOIN
	                      dbo.[netsqlazman_Stores]() Stores ON Applications.StoreId = Stores.StoreId
	WHERE     (Stores.StoreId = @STOREID) AND (Applications.ApplicationId = @ApplicationId) AND (Items.Name = @ITEMNAME) AND (@OPERATIONSONLY = 1 AND Items.ItemType=2 OR @OPERATIONSONLY = 0)
IF @ITEMID IS NULL
	BEGIN
	RAISERROR (''Item not found.'', 16, 1)
	RETURN 1
	END
-- PREPARE RESULTSET FOR BIZ RULE CHECKING
CREATE TABLE #PARTIAL_RESULTS_TABLE 
	(   
		[ItemId] [int] NOT NULL ,
		[ItemName] [nvarchar] (255)  NOT NULL ,
		[ItemType] [tinyint] NOT NULL,
		[AuthorizationType] TINYINT,
		[BizRuleId] [int] NULL ,
		[BizRuleSource] TEXT,
		[BizRuleLanguage] TINYINT
	)
-- PREPARE RESULTSET FOR ATTRIBUTES
IF @RETRIEVEATTRIBUTES = 1
BEGIN
	CREATE TABLE #ATTRIBUTES_TABLE 
	(   
		[AttributeKey] [nvarchar] (255)  NOT NULL,
		[AttributeValue] [nvarchar] (4000)  NOT NULL,
		[ItemId] INT NULL
	)
--------------------------------------------------------------------------------
-- GET STORE AND APPLICATION ATTRIBUTES
--------------------------------------------------------------------------------
	INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, NULL FROM dbo.[netsqlazman_StoreAttributesTable] StoreAttributes INNER JOIN dbo.[netsqlazman_StoresTable] Stores ON StoreAttributes.StoreId = Stores.StoreId WHERE Stores.StoreId = @STOREID
	INSERT INTO #ATTRIBUTES_TABLE SELECT AttributeKey, AttributeValue, NULL FROM dbo.[netsqlazman_ApplicationAttributesTable] ApplicationAttributes INNER JOIN dbo.[netsqlazman_ApplicationsTable] Applications ON ApplicationAttributes.ApplicationId = Applications.ApplicationId WHERE Applications.ApplicationId = @ApplicationId
END
--------------------------------------------------------------------------------
DECLARE @USERSID varbinary(85)
DECLARE @I INT
DECLARE @INDEX INT
DECLARE @APP VARBINARY(85)
DECLARE @SETTINGVALUE nvarchar(255)
DECLARE @NETSQLAZMANMODE bit

SELECT @SETTINGVALUE = SettingValue FROM dbo.[netsqlazman_Settings] WHERE SettingName = ''Mode''
IF @SETTINGVALUE = ''Developer'' 
BEGIN
	SET @NETSQLAZMANMODE = 1 
END
ELSE 
BEGIN
	SET @NETSQLAZMANMODE = 0
END

CREATE TABLE #USERGROUPS (objectSid VARBINARY(85))
-- Get User Sid
IF @USERGROUPSCOUNT>0
BEGIN
	SET @USERSID = SUBSTRING(@TOKEN,1,85)
	SET @I = CHARINDEX(0x01, @USERSID)
	SET @USERSID = SUBSTRING(@USERSID, @I, 85-@I+1)
	-- Get User Groups Sid
	SET @I = 0
	WHILE (@I<@USERGROUPSCOUNT)
	BEGIN
		SET @APP = SUBSTRING(@TOKEN,(@I+1)*85+1,85) --GET USER GROUP TOKEN PORTION
		SET @INDEX = CHARINDEX(0x01, @APP) -- FIND TOKEN START (0x01)
		SET @APP = SUBSTRING(@APP, @INDEX, 85-@INDEX+1) -- EXTRACT USER GROUP SID
		INSERT INTO #USERGROUPS (objectSid) VALUES (@APP)
		SET @I = @I + 1
	END
END
ELSE
BEGIN
	SET @USERSID = @TOKEN
END

EXEC dbo.[netsqlazman_CheckAccess] @ITEMID, @USERSID, @VALIDFOR, @LDAPPATH, @AUTHORIZATION_TYPE OUTPUT, @NETSQLAZMANMODE, @RETRIEVEATTRIBUTES
SELECT * FROM #PARTIAL_RESULTS_TABLE
IF @RETRIEVEATTRIBUTES = 1
	SELECT * FROM #ATTRIBUTES_TABLE
DROP TABLE #PARTIAL_RESULTS_TABLE
IF @RETRIEVEATTRIBUTES = 1
	DROP TABLE #ATTRIBUTES_TABLE
DROP TABLE #USERGROUPS
SET NOCOUNT OFF
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_BuildUserPermissionCache]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_BuildUserPermissionCache]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_BuildUserPermissionCache](@STORENAME nvarchar(255), @APPLICATIONNAME nvarchar(255))
AS 
-- Hierarchy
SET NOCOUNT ON
SELECT     Items.Name AS ItemName, Items_1.Name AS ParentItemName
FROM         dbo.[netsqlazman_Items]() Items_1 INNER JOIN
                      dbo.[netsqlazman_ItemsHierarchy]() ItemsHierarchy ON Items_1.ItemId = ItemsHierarchy.MemberOfItemId RIGHT OUTER JOIN
                      dbo.[netsqlazman_Applications]() Applications INNER JOIN
                      dbo.[netsqlazman_Stores]() Stores ON Applications.StoreId = Stores.StoreId INNER JOIN
                      dbo.[netsqlazman_Items]() Items ON Applications.ApplicationId = Items.ApplicationId ON ItemsHierarchy.ItemId = Items.ItemId
WHERE     (Stores.Name = @STORENAME) AND (Applications.Name = @APPLICATIONNAME)

-- Item Authorizations
SELECT DISTINCT Items.Name AS ItemName, Authorizations.ValidFrom, Authorizations.ValidTo
FROM         dbo.[netsqlazman_Authorizations]() Authorizations INNER JOIN
                      dbo.[netsqlazman_Items]() Items ON Authorizations.ItemId = Items.ItemId INNER JOIN
                      dbo.[netsqlazman_Stores]() Stores INNER JOIN
                      dbo.[netsqlazman_Applications]() Applications ON Stores.StoreId = Applications.StoreId ON Items.ApplicationId = Applications.ApplicationId
WHERE     (Authorizations.AuthorizationType <> 0) AND (Stores.Name = @STORENAME) AND (Applications.Name = @APPLICATIONNAME)
UNION
SELECT DISTINCT Items.Name AS ItemName, NULL ValidFrom, NULL ValidTo
FROM         dbo.[netsqlazman_Items]() Items INNER JOIN
                      dbo.[netsqlazman_Stores]() Stores INNER JOIN
                      dbo.[netsqlazman_Applications]() Applications ON Stores.StoreId = Applications.StoreId ON Items.ApplicationId = Applications.ApplicationId
WHERE     (Stores.Name = @STORENAME) AND (Applications.Name = @APPLICATIONNAME) AND Items.BizRuleId IS NOT NULL
SET NOCOUNT OFF
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_ApplicationGroupMembersView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_ApplicationGroupMembersView]
AS
SELECT     [netsqlazman_Stores].StoreId, [netsqlazman_Applications].ApplicationId, [netsqlazman_ApplicationGroupMembers].ApplicationGroupMemberId, [netsqlazman_ApplicationGroupMembers].ApplicationGroupId, 
                      [netsqlazman_ApplicationGroups].Name AS ApplicationGroup, dbo.[netsqlazman_GetNameFromSid]([netsqlazman_Stores].Name, [netsqlazman_Applications].Name, [netsqlazman_ApplicationGroupMembers].objectSid, 
                      [netsqlazman_ApplicationGroupMembers].WhereDefined) AS Name, [netsqlazman_ApplicationGroupMembers].objectSid, 
                      CASE WhereDefined WHEN 0 THEN ''Store'' WHEN 1 THEN ''Application'' WHEN 2 THEN ''LDap'' WHEN 3 THEN ''Local'' WHEN 4 THEN ''DATABASE'' END AS WhereDefined,
                       CASE IsMember WHEN 1 THEN ''Member'' WHEN 0 THEN ''Non-Member'' END AS MemberType
FROM         dbo.[netsqlazman_ApplicationGroupMembers]() [netsqlazman_ApplicationGroupMembers] INNER JOIN
                      dbo.[netsqlazman_ApplicationGroups]() [netsqlazman_ApplicationGroups] ON [netsqlazman_ApplicationGroupMembers].ApplicationGroupId = [netsqlazman_ApplicationGroups].ApplicationGroupId INNER JOIN
                      dbo.[netsqlazman_Applications]() [netsqlazman_Applications] ON [netsqlazman_ApplicationGroups].ApplicationId = [netsqlazman_Applications].ApplicationId INNER JOIN
                      dbo.[netsqlazman_Stores]() [netsqlazman_Stores] ON [netsqlazman_Applications].StoreId = [netsqlazman_Stores].StoreId
'
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationGroupInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationGroupInsert]
(
	@ApplicationId int,
	@objectSid varbinary(85),
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@LDapQuery nvarchar(4000),
	@GroupType tinyint
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ApplicationGroupsTable] ([ApplicationId], [objectSid], [Name], [Description], [LDapQuery], [GroupType]) VALUES (@ApplicationId, @objectSid, @Name, @Description, @LDapQuery, @GroupType)
	RETURN SCOPE_IDENTITY()
END
ELSE	
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationDelete]
(
	@StoreId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ApplicationsTable] WHERE [ApplicationId] = @ApplicationId AND [StoreId] = @StoreId
ELSE
	RAISERROR (''Store permission denied'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_GrantApplicationAccess]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_GrantApplicationAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_GrantApplicationAccess] (
	@ApplicationId int,
	@SqlUserOrRole sysname,
	@NetSqlAzManFixedServerRole tinyint)
AS
DECLARE @StoreId int
SET @StoreId = (SELECT StoreId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId)
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_ApplicationsTable] WHERE ApplicationId = @ApplicationId) AND (dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1 AND @NetSqlAzManFixedServerRole BETWEEN 0 AND 1 OR dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1 AND @NetSqlAzManFixedServerRole = 2)
BEGIN
	DECLARE @MEMBERUID int
	IF NOT (@NetSqlAzManFixedServerRole BETWEEN 0 AND 2)
	BEGIN
		RAISERROR (''NetSqlAzManFixedServerRole must be 0, 1 or 2 (Reader, User, Manager).'', 16, 1)
		RETURN -1
	END
	 -- CHECK MEMBER NAME (ATTEMPT ADDING IMPLICIT ROW FOR NT NAME) --
	DECLARE @IsSqlRoleInt int
	DECLARE @IsNtGroupInt bit
	DECLARE @IsSqlRole bit
	SELECT @MEMBERUID = uid, @IsSqlRoleInt = issqlrole, @IsNtGroupInt = isntgroup  from sysusers where sid = SUSER_SID(@SqlUserOrRole) and isaliased = 0
	IF @IsSqlRoleInt = 1 OR @IsNtGroupInt = 1
		SET @IsSqlRole = 1
	ELSE
		SET @IsSqlRole = 0
	IF @MEMBERUID IS NULL
	BEGIN
		RAISERROR (''Sql User/Role Not Found. Grant Store Access ignored.'', -1, -1)
		RETURN 0
	END
	IF EXISTS(SELECT * FROM dbo.[netsqlazman_ApplicationPermissionsTable] WHERE ApplicationId = @ApplicationId AND SqlUserOrRole = @SqlUserOrRole AND NetSqlAzManFixedServerRole = @NetSqlAzManFixedServerRole)
		BEGIN
		RAISERROR (''NetSqlAzManFixedServerRole updated.'', -1, -1)
		RETURN 0
		END
	ELSE
		BEGIN
		INSERT INTO dbo.[netsqlazman_ApplicationPermissionsTable] (ApplicationId, SqlUserOrRole, IsSqlRole, NetSqlAzManFixedServerRole) VALUES (@ApplicationId, @SqlUserOrRole, @IsSqlRole, @NetSqlAzManFixedServerRole)
		RETURN SCOPE_IDENTITY()
		END
END
ELSE
	RAISERROR (''Application NOT Found or Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationUpdate]
(
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@Original_ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @Original_ApplicationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@Original_ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ApplicationsTable] SET [Name] = @Name, [Description] = @Description WHERE [ApplicationId] = @Original_ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_ApplicationsView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_ApplicationsView]
AS
SELECT     [netsqlazman_Stores].StoreId, [netsqlazman_Stores].Name AS StoreName, [netsqlazman_Stores].Description AS StoreDescription, [netsqlazman_Applications].ApplicationId, [netsqlazman_Applications].Name AS ApplicationName, 
                      [netsqlazman_Applications].Description AS ApplicationDescription
FROM         dbo.[netsqlazman_Applications]() [netsqlazman_Applications] INNER JOIN
                      dbo.[netsqlazman_Stores]() [netsqlazman_Stores] ON [netsqlazman_Applications].StoreId = [netsqlazman_Stores].StoreId
'
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationPermissions]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_ApplicationPermissions]()
RETURNS TABLE 
AS  
RETURN
	SELECT     dbo.[netsqlazman_ApplicationPermissionsTable].*
	FROM         dbo.[netsqlazman_ApplicationPermissionsTable] INNER JOIN
	                      dbo.[netsqlazman_Applications]() Applications ON dbo.[netsqlazman_ApplicationPermissionsTable].ApplicationId = Applications.ApplicationId
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationPermissionInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationPermissionInsert]
(
	@ApplicationId int,
	@SqlUserOrRole nvarchar(128),
	@IsSqlRole bit,
	@NetSqlAzManFixedServerRole tinyint
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO dbo.[netsqlazman_ApplicationPermissionsTable] (ApplicationId, SqlUserOrRole, IsSqlRole, NetSqlAzManFixedServerRole) VALUES (@ApplicationId, @SqlUserOrRole, @IsSqlRole, @NetSqlAzManFixedServerRole)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationPermissionDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationPermissionDelete]
(
	@ApplicationPermissionId int,
	@ApplicationId int
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM dbo.[netsqlazman_ApplicationPermissionsTable] WHERE ApplicationPermissionId = @ApplicationPermissionId AND ApplicationId = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationInsert]
(
	@StoreId int,
	@Name nvarchar(255),
	@Description nvarchar(1024)
)
AS
IF EXISTS(SELECT StoreId FROM dbo.[netsqlazman_Stores]() WHERE StoreId = @StoreId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ApplicationsTable] ([StoreId], [Name], [Description]) VALUES (@StoreId, @Name, @Description);
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_ApplicationAttributes]    Script Date: 12/04/2012 16:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_ApplicationAttributes] ()
RETURNS TABLE
AS
RETURN 
	SELECT     dbo.[netsqlazman_ApplicationAttributesTable].*
	FROM         dbo.[netsqlazman_ApplicationAttributesTable] INNER JOIN
	                      dbo.[netsqlazman_Applications]() Applications ON dbo.[netsqlazman_ApplicationAttributesTable].ApplicationId = Applications.ApplicationId
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributeInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationAttributeInsert]
(
	@ApplicationId int,
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000)
)
AS
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_ApplicationAttributesTable] ([ApplicationId], [AttributeKey], [AttributeValue]) VALUES (@ApplicationId, @AttributeKey, @AttributeValue)
	RETURN SCOPE_IDENTITY()
END
ELSE
BEGIN
	RAISERROR (''Application Permission denied.'', 16, 1)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreUpdate]
(
	@Name nvarchar(255),
	@Description nvarchar(1024),
	@Original_StoreId int
)
AS
IF EXISTS(Select StoreId FROM dbo.[netsqlazman_Stores]() WHERE StoreId = @Original_StoreId) AND dbo.[netsqlazman_CheckStorePermissions](@Original_StoreId, 2) = 1
	UPDATE [dbo].[netsqlazman_StoresTable] SET [Name] = @Name, [Description] = @Description WHERE [StoreId] = @Original_StoreId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_RevokeApplicationAccess]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_RevokeApplicationAccess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_RevokeApplicationAccess] (
	@ApplicationId int,
	@SqlUserOrRole sysname,
	@NetSqlAzManFixedServerRole tinyint)
AS
DECLARE @StoreId int
SET @StoreId = (SELECT StoreId FROM dbo.[netsqlazman_Applications]() WHERE ApplicationId = @ApplicationId)
IF EXISTS(SELECT ApplicationId FROM dbo.[netsqlazman_ApplicationsTable] WHERE ApplicationId = @ApplicationId) AND (dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1 AND @NetSqlAzManFixedServerRole BETWEEN 0 AND 1 OR dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1 AND @NetSqlAzManFixedServerRole = 2)
BEGIN
	IF EXISTS(SELECT * FROM dbo.[netsqlazman_ApplicationPermissionsTable] WHERE ApplicationId = @ApplicationId AND SqlUserOrRole = @SqlUserOrRole AND NetSqlAzManFixedServerRole = @NetSqlAzManFixedServerRole)
		DELETE FROM dbo.[netsqlazman_ApplicationPermissionsTable] WHERE ApplicationId = @ApplicationId AND SqlUserOrRole = @SqlUserOrRole AND NetSqlAzManFixedServerRole = @NetSqlAzManFixedServerRole
	ELSE
		RAISERROR (''Permission not found. Revoke Application Access ignored.'', -1, -1)
END
ELSE
	RAISERROR (''Application NOT Found or Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreDelete]
(
	@Original_StoreId int
)
AS
IF EXISTS(Select StoreId FROM dbo.[netsqlazman_Stores]() WHERE StoreId = @Original_StoreId) AND dbo.[netsqlazman_CheckStorePermissions](@Original_StoreId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_StoresTable] WHERE [StoreId] = @Original_StoreId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StorePermissions]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissions]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_StorePermissions]()
RETURNS TABLE 
AS  
RETURN
	SELECT     dbo.[netsqlazman_StorePermissionsTable].*
	FROM         dbo.[netsqlazman_StorePermissionsTable] INNER JOIN
	                      dbo.[netsqlazman_Stores]() Stores ON dbo.[netsqlazman_StorePermissionsTable].StoreId = Stores.StoreId
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StorePermissionInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StorePermissionInsert]
(
	@StoreId int,
	@SqlUserOrRole nvarchar(128),
	@IsSqlRole bit,
	@NetSqlAzManFixedServerRole tinyint
)
AS
IF EXISTS(SELECT StoreId FROM dbo.[netsqlazman_Stores]() WHERE StoreId = @StoreId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
BEGIN
	INSERT INTO dbo.[netsqlazman_StorePermissionsTable] (StoreId, SqlUserOrRole, IsSqlRole, NetSqlAzManFixedServerRole) VALUES (@StoreId, @SqlUserOrRole, @IsSqlRole, @NetSqlAzManFixedServerRole)
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StorePermissionDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StorePermissionDelete]
(
	@StorePermissionId int,
	@StoreId int
)
AS
IF EXISTS(SELECT StoreId FROM dbo.[netsqlazman_Stores]() WHERE StoreId = @StoreId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	DELETE FROM dbo.[netsqlazman_StorePermissionsTable] WHERE StorePermissionId = @StorePermissionId AND StoreId = @StoreId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[netsqlazman_StoreAttributes]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[netsqlazman_StoreAttributes] ()
RETURNS TABLE
AS
RETURN
	SELECT     dbo.[netsqlazman_StoreAttributesTable].*
	FROM         dbo.[netsqlazman_StoreAttributesTable] INNER JOIN
	                      dbo.[netsqlazman_Stores]() Stores ON dbo.[netsqlazman_StoreAttributesTable].StoreId = Stores.StoreId
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreAttributeInsert]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributeInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreAttributeInsert]
(
	@StoreId int,
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000)
)
AS
IF EXISTS(Select StoreId FROM dbo.[netsqlazman_Stores]() WHERE StoreId = @StoreId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
BEGIN
	INSERT INTO [dbo].[netsqlazman_StoreAttributesTable] ([StoreId], [AttributeKey], [AttributeValue]) VALUES (@StoreId, @AttributeKey, @AttributeValue);
	RETURN SCOPE_IDENTITY()
END
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributeDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreAttributeDelete]
(
	@StoreId int,
	@StoreAttributeId int
)
AS
IF EXISTS(Select StoreAttributeId FROM dbo.[netsqlazman_StoreAttributes]() WHERE StoreAttributeId = @StoreAttributeId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_StoreAttributesTable] WHERE [StoreAttributeId] = @StoreAttributeId AND [StoreId] = @StoreId
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_StoreAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributeUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_StoreAttributeUpdate]
(
	@StoreId int,
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000),
	@Original_StoreAttributeId int
)
AS
IF EXISTS(Select StoreAttributeId FROM dbo.[netsqlazman_StoreAttributes]() WHERE StoreAttributeId = @Original_StoreAttributeId) AND dbo.[netsqlazman_CheckStorePermissions](@StoreId, 2) = 1
	UPDATE [dbo].[netsqlazman_StoreAttributesTable] SET [AttributeKey] = @AttributeKey, [AttributeValue] = @AttributeValue WHERE [StoreAttributeId] = @Original_StoreAttributeId AND [StoreId] = @StoreId 
ELSE
	RAISERROR (''Store permission denied.'', 16, 1)
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_StoreAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_StoreAttributesView]
AS
SELECT     [netsqlazman_Stores].StoreId, [netsqlazman_Stores].Name, [netsqlazman_Stores].Description, [netsqlazman_StoreAttributes].StoreAttributeId, [netsqlazman_StoreAttributes].AttributeKey, [netsqlazman_StoreAttributes].AttributeValue
FROM         dbo.[netsqlazman_Stores]() [netsqlazman_Stores] INNER JOIN
                      dbo.[netsqlazman_StoreAttributes]() [netsqlazman_StoreAttributes] ON [netsqlazman_Stores].StoreId = [netsqlazman_StoreAttributes].StoreId
'
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationAttributeDelete]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributeDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationAttributeDelete]
(
	@ApplicationId int,
	@ApplicationAttributeId int
)
AS
IF EXISTS(SELECT ApplicationAttributeId FROM dbo.[netsqlazman_ApplicationAttributes]() WHERE ApplicationAttributeId = @ApplicationAttributeId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	DELETE FROM [dbo].[netsqlazman_ApplicationAttributesTable] WHERE [ApplicationAttributeId] = @ApplicationAttributeId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Application permission denied.'', 16, 1)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[netsqlazman_ApplicationAttributeUpdate]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributeUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[netsqlazman_ApplicationAttributeUpdate]
(
	@ApplicationId int,
	@AttributeKey nvarchar(255),
	@AttributeValue nvarchar(4000),
	@Original_ApplicationAttributeId int
)
AS
IF EXISTS(SELECT ApplicationAttributeId FROM dbo.[netsqlazman_ApplicationAttributes]() WHERE ApplicationAttributeId = @Original_ApplicationAttributeId) AND dbo.[netsqlazman_CheckApplicationPermissions](@ApplicationId, 2) = 1
	UPDATE [dbo].[netsqlazman_ApplicationAttributesTable] SET [AttributeKey] = @AttributeKey, [AttributeValue] = @AttributeValue WHERE [ApplicationAttributeId] = @Original_ApplicationAttributeId AND [ApplicationId] = @ApplicationId
ELSE
	RAISERROR (''Applicaction Permission denied.'', 16, 1)
' 
END
GO
/****** Object:  View [dbo].[netsqlazman_ApplicationAttributesView]    Script Date: 12/04/2012 16:38:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[netsqlazman_ApplicationAttributesView]
AS
SELECT     [netsqlazman_Applications].ApplicationId, [netsqlazman_Applications].StoreId, [netsqlazman_Applications].Name, [netsqlazman_Applications].Description, ApplicationAttributes.ApplicationAttributeId, 
                      ApplicationAttributes.AttributeKey, ApplicationAttributes.AttributeValue
FROM         dbo.[netsqlazman_Applications]() [netsqlazman_Applications] INNER JOIN
                      dbo.[netsqlazman_ApplicationAttributes]() ApplicationAttributes ON [netsqlazman_Applications].ApplicationId = ApplicationAttributes.ApplicationId
'
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__2645B050]    Script Date: 12/04/2012 16:38:49 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__2645B050]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ap__Appli__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] ADD  CONSTRAINT [DF__aspnet_Ap__Appli__2645B050]  DEFAULT (newid()) FOR [ApplicationId]
END


End
GO
/****** Object:  Default [DF__aspnet_Me__Passw__40F9A68C]    Script Date: 12/04/2012 16:38:49 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__40F9A68C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Me__Passw__40F9A68C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] ADD  CONSTRAINT [DF__aspnet_Me__Passw__40F9A68C]  DEFAULT ((0)) FOR [PasswordFormat]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__UserI__2BFE89A6]    Script Date: 12/04/2012 16:38:49 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__2BFE89A6]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__UserI__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__UserI__2BFE89A6]  DEFAULT (newid()) FOR [UserId]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__2CF2ADDF]    Script Date: 12/04/2012 16:38:49 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__2CF2ADDF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__Mobil__2CF2ADDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__Mobil__2CF2ADDF]  DEFAULT (NULL) FOR [MobileAlias]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__2DE6D218]    Script Date: 12/04/2012 16:38:49 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__2DE6D218]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__IsAno__2DE6D218]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__IsAno__2DE6D218]  DEFAULT ((0)) FOR [IsAnonymous]
END


End
GO
/****** Object:  Default [DF_Log_SqlIdentity]    Script Date: 12/04/2012 16:38:49 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Log_SqlIdentity]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Log_SqlIdentity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[netsqlazman_LogTable] ADD  CONSTRAINT [DF_Log_SqlIdentity]  DEFAULT (suser_sname()) FOR [SqlIdentity]
END


End
GO
/****** Object:  Check [CK_WhereDefinedNotValid]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedNotValid]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable]  WITH CHECK ADD  CONSTRAINT [CK_WhereDefinedNotValid] CHECK  (([WhereDefined]>=(0) AND [WhereDefined]<=(4)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedNotValid]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable] CHECK CONSTRAINT [CK_WhereDefinedNotValid]
GO
/****** Object:  Check [CK_ApplicationGroups_GroupType_Check]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupsTable]  WITH CHECK ADD  CONSTRAINT [CK_ApplicationGroups_GroupType_Check] CHECK  (([GroupType]>=(0) AND [GroupType]<=(1)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupsTable] CHECK CONSTRAINT [CK_ApplicationGroups_GroupType_Check]
GO
/****** Object:  Check [CK_ApplicationPermissions]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationPermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationPermissionsTable]  WITH CHECK ADD  CONSTRAINT [CK_ApplicationPermissions] CHECK  (([NetSqlAzManFixedServerRole]>=(0) AND [NetSqlAzManFixedServerRole]<=(2)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ApplicationPermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationPermissionsTable] CHECK CONSTRAINT [CK_ApplicationPermissions]
GO
/****** Object:  Check [CK_AuthorizationTypeCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuthorizationTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable]  WITH CHECK ADD  CONSTRAINT [CK_AuthorizationTypeCheck] CHECK  (([AuthorizationType]>=(0) AND [AuthorizationType]<=(3)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_AuthorizationTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] CHECK CONSTRAINT [CK_AuthorizationTypeCheck]
GO
/****** Object:  Check [CK_objectSidWhereDefinedCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_objectSidWhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable]  WITH CHECK ADD  CONSTRAINT [CK_objectSidWhereDefinedCheck] CHECK  (([objectSidWhereDefined]>=(0) AND [objectSidWhereDefined]<=(4)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_objectSidWhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] CHECK CONSTRAINT [CK_objectSidWhereDefinedCheck]
GO
/****** Object:  Check [CK_ownerSidWhereDefined]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ownerSidWhereDefined]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable]  WITH CHECK ADD  CONSTRAINT [CK_ownerSidWhereDefined] CHECK  (([ownerSidWhereDefined]>=(2) AND [ownerSidWhereDefined]<=(4)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ownerSidWhereDefined]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] CHECK CONSTRAINT [CK_ownerSidWhereDefined]
GO
/****** Object:  Check [CK_ValidFromToCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ValidFromToCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable]  WITH CHECK ADD  CONSTRAINT [CK_ValidFromToCheck] CHECK  (([ValidFrom] IS NULL OR [ValidTo] IS NULL OR [ValidFrom]<=[ValidTo]))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_ValidFromToCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] CHECK CONSTRAINT [CK_ValidFromToCheck]
GO
/****** Object:  Check [CK_Items_ItemTypeCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Items_ItemTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable]  WITH CHECK ADD  CONSTRAINT [CK_Items_ItemTypeCheck] CHECK  (([ItemType]>=(0) AND [ItemType]<=(2)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Items_ItemTypeCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable] CHECK CONSTRAINT [CK_Items_ItemTypeCheck]
GO
/****** Object:  Check [CK_Log]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Log]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]'))
ALTER TABLE [dbo].[netsqlazman_LogTable]  WITH CHECK ADD  CONSTRAINT [CK_Log] CHECK  (([LogType]='I' OR [LogType]='W' OR [LogType]='E'))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Log]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_LogTable]'))
ALTER TABLE [dbo].[netsqlazman_LogTable] CHECK CONSTRAINT [CK_Log]
GO
/****** Object:  Check [CK_Settings]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Settings]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_Settings]'))
ALTER TABLE [dbo].[netsqlazman_Settings]  WITH CHECK ADD  CONSTRAINT [CK_Settings] CHECK  (([SettingName]='Mode' AND ([SettingValue]='Developer' OR [SettingValue]='Administrator') OR [SettingName]='LogErrors' AND ([SettingValue]='True' OR [SettingValue]='False') OR [SettingName]='LogWarnings' AND ([SettingValue]='True' OR [SettingValue]='False') OR [SettingName]='LogInformations' AND ([SettingValue]='True' OR [SettingValue]='False') OR [SettingName]='LogOnEventLog' AND ([SettingValue]='True' OR [SettingValue]='False') OR [SettingName]='LogOnDb' AND ([SettingValue]='True' OR [SettingValue]='False')))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Settings]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_Settings]'))
ALTER TABLE [dbo].[netsqlazman_Settings] CHECK CONSTRAINT [CK_Settings]
GO
/****** Object:  Check [CK_WhereDefinedCheck]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupMembersTable]  WITH CHECK ADD  CONSTRAINT [CK_WhereDefinedCheck] CHECK  (([WhereDefined]=(0) OR [WhereDefined]>=(2) AND [WhereDefined]<=(4)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_WhereDefinedCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupMembersTable] CHECK CONSTRAINT [CK_WhereDefinedCheck]
GO
/****** Object:  Check [CK_StoreGroups_GroupType_Check]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StoreGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupsTable]  WITH CHECK ADD  CONSTRAINT [CK_StoreGroups_GroupType_Check] CHECK  (([GroupType]>=(0) AND [GroupType]<=(1)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StoreGroups_GroupType_Check]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupsTable] CHECK CONSTRAINT [CK_StoreGroups_GroupType_Check]
GO
/****** Object:  Check [CK_StorePermissions]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StorePermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_StorePermissionsTable]  WITH CHECK ADD  CONSTRAINT [CK_StorePermissions] CHECK  (([NetSqlAzManFixedServerRole]>=(0) AND [NetSqlAzManFixedServerRole]<=(2)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_StorePermissions]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_StorePermissionsTable] CHECK CONSTRAINT [CK_StorePermissions]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__3F115E1A]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__3F115E1A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Me__Appli__3F115E1A] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__3F115E1A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__Appli__3F115E1A]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__2B0A656D]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__Appli__2B0A656D] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] CHECK CONSTRAINT [FK__aspnet_Us__Appli__2B0A656D]
GO
/****** Object:  ForeignKey [FK_ApplicationAttributes_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationAttributes_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationAttributesTable]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationAttributes_Applications] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[netsqlazman_ApplicationsTable] ([ApplicationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationAttributes_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationAttributesTable] CHECK CONSTRAINT [FK_ApplicationAttributes_Applications]
GO
/****** Object:  ForeignKey [FK_ApplicationGroupMembers_ApplicationGroup]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationGroupMembers_ApplicationGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationGroupMembers_ApplicationGroup] FOREIGN KEY([ApplicationGroupId])
REFERENCES [dbo].[netsqlazman_ApplicationGroupsTable] ([ApplicationGroupId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationGroupMembers_ApplicationGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupMembersTable] CHECK CONSTRAINT [FK_ApplicationGroupMembers_ApplicationGroup]
GO
/****** Object:  ForeignKey [FK_ApplicationGroups_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationGroups_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupsTable]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationGroups_Applications] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[netsqlazman_ApplicationsTable] ([ApplicationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationGroups_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationGroupsTable] CHECK CONSTRAINT [FK_ApplicationGroups_Applications]
GO
/****** Object:  ForeignKey [FK_ApplicationPermissions_ApplicationsTable]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationPermissions_ApplicationsTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationPermissionsTable]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationPermissions_ApplicationsTable] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[netsqlazman_ApplicationsTable] ([ApplicationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ApplicationPermissions_ApplicationsTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationPermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationPermissionsTable] CHECK CONSTRAINT [FK_ApplicationPermissions_ApplicationsTable]
GO
/****** Object:  ForeignKey [FK_Applications_Stores]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationsTable]  WITH CHECK ADD  CONSTRAINT [FK_Applications_Stores] FOREIGN KEY([StoreId])
REFERENCES [dbo].[netsqlazman_StoresTable] ([StoreId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ApplicationsTable]'))
ALTER TABLE [dbo].[netsqlazman_ApplicationsTable] CHECK CONSTRAINT [FK_Applications_Stores]
GO
/****** Object:  ForeignKey [FK_AuthorizationAttributes_Authorizations]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuthorizationAttributes_Authorizations]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationAttributesTable]  WITH CHECK ADD  CONSTRAINT [FK_AuthorizationAttributes_Authorizations] FOREIGN KEY([AuthorizationId])
REFERENCES [dbo].[netsqlazman_AuthorizationsTable] ([AuthorizationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AuthorizationAttributes_Authorizations]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationAttributesTable] CHECK CONSTRAINT [FK_AuthorizationAttributes_Authorizations]
GO
/****** Object:  ForeignKey [FK_Authorizations_Items]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Authorizations_Items]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable]  WITH CHECK ADD  CONSTRAINT [FK_Authorizations_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[netsqlazman_ItemsTable] ([ItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Authorizations_Items]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_AuthorizationsTable]'))
ALTER TABLE [dbo].[netsqlazman_AuthorizationsTable] CHECK CONSTRAINT [FK_Authorizations_Items]
GO
/****** Object:  ForeignKey [FK_ItemAttributes_Items]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItemAttributes_Items]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemAttributesTable]  WITH CHECK ADD  CONSTRAINT [FK_ItemAttributes_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[netsqlazman_ItemsTable] ([ItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItemAttributes_Items]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemAttributesTable] CHECK CONSTRAINT [FK_ItemAttributes_Items]
GO
/****** Object:  ForeignKey [FK_Items_Applications]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Items_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable]  WITH CHECK ADD  CONSTRAINT [FK_Items_Applications] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[netsqlazman_ApplicationsTable] ([ApplicationId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Items_Applications]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable] CHECK CONSTRAINT [FK_Items_Applications]
GO
/****** Object:  ForeignKey [FK_Items_BizRules]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Items_BizRules]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable]  WITH CHECK ADD  CONSTRAINT [FK_Items_BizRules] FOREIGN KEY([BizRuleId])
REFERENCES [dbo].[netsqlazman_BizRulesTable] ([BizRuleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Items_BizRules]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_ItemsTable]'))
ALTER TABLE [dbo].[netsqlazman_ItemsTable] CHECK CONSTRAINT [FK_Items_BizRules]
GO
/****** Object:  ForeignKey [FK_StoreAttributes_Stores]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreAttributes_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreAttributesTable]  WITH CHECK ADD  CONSTRAINT [FK_StoreAttributes_Stores] FOREIGN KEY([StoreId])
REFERENCES [dbo].[netsqlazman_StoresTable] ([StoreId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreAttributes_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreAttributesTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreAttributesTable] CHECK CONSTRAINT [FK_StoreAttributes_Stores]
GO
/****** Object:  ForeignKey [FK_StoreGroupMembers_StoreGroup]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreGroupMembers_StoreGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupMembersTable]  WITH CHECK ADD  CONSTRAINT [FK_StoreGroupMembers_StoreGroup] FOREIGN KEY([StoreGroupId])
REFERENCES [dbo].[netsqlazman_StoreGroupsTable] ([StoreGroupId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreGroupMembers_StoreGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupMembersTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupMembersTable] CHECK CONSTRAINT [FK_StoreGroupMembers_StoreGroup]
GO
/****** Object:  ForeignKey [FK_StoreGroups_Stores]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreGroups_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupsTable]  WITH CHECK ADD  CONSTRAINT [FK_StoreGroups_Stores] FOREIGN KEY([StoreId])
REFERENCES [dbo].[netsqlazman_StoresTable] ([StoreId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoreGroups_Stores]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StoreGroupsTable]'))
ALTER TABLE [dbo].[netsqlazman_StoreGroupsTable] CHECK CONSTRAINT [FK_StoreGroups_Stores]
GO
/****** Object:  ForeignKey [FK_StorePermissions_StoresTable]    Script Date: 12/04/2012 16:38:49 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StorePermissions_StoresTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_StorePermissionsTable]  WITH CHECK ADD  CONSTRAINT [FK_StorePermissions_StoresTable] FOREIGN KEY([StoreId])
REFERENCES [dbo].[netsqlazman_StoresTable] ([StoreId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StorePermissions_StoresTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[netsqlazman_StorePermissionsTable]'))
ALTER TABLE [dbo].[netsqlazman_StorePermissionsTable] CHECK CONSTRAINT [FK_StorePermissions_StoresTable]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationPermissionsTable] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT INSERT ON [dbo].[netsqlazman_LogTable] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_LogTable] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT DELETE ON [dbo].[netsqlazman_Settings] TO [NetSqlAzMan_Administrators] AS [dbo]
GO
GRANT INSERT ON [dbo].[netsqlazman_Settings] TO [NetSqlAzMan_Administrators] AS [dbo]
GO
GRANT UPDATE ON [dbo].[netsqlazman_Settings] TO [NetSqlAzMan_Administrators] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_Settings] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StorePermissionsTable] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[UsersDemo] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationAttributes] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationGroupMembers] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationGroups] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationPermissions] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_Applications] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_AuthorizationAttributes] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_Authorizations] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_BizRules] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_CheckApplicationPermissions] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_CheckStorePermissions] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_DBVersion] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_GetDBUsers] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_IAmAdmin] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ItemAttributes] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_Items] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ItemsHierarchy] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StoreAttributes] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StoreGroupMembers] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StoreGroups] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StorePermissions] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_Stores] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_CreateUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_FindUsersByEmail] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_FindUsersByName] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetAllUsers] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetPassword] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetPasswordWithFormat] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_ResetPassword] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_SetPassword] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UnlockUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UpdateUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UpdateUserInfo] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Users_DeleteUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationAttributeDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationAttributeInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationAttributeUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationGroupDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationGroupInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationGroupMemberDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationGroupMemberInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationGroupMemberUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationGroupUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationPermissionDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationPermissionInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ApplicationUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_AuthorizationAttributeDelete] TO [NetSqlAzMan_Users] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_AuthorizationAttributeInsert] TO [NetSqlAzMan_Users] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_AuthorizationAttributeUpdate] TO [NetSqlAzMan_Users] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_AuthorizationDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_AuthorizationInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_AuthorizationUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_BizRuleDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_BizRuleInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_BizRuleUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_BuildUserPermissionCache] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ClearBizRule] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_CreateDelegate] TO [NetSqlAzMan_Users] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_DeleteDelegate] TO [NetSqlAzMan_Users] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_DirectCheckAccess] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_GrantApplicationAccess] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_GrantStoreAccess] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_helplogins] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_IsAMemberOfGroup] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemAttributeDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemAttributeInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemAttributeUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemsHierarchyDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemsHierarchyInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ItemUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_ReloadBizRule] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_RevokeApplicationAccess] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_RevokeStoreAccess] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreAttributeDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreAttributeInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreAttributeUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreGroupDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreGroupInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreGroupMemberDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreGroupMemberInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreGroupMemberUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreGroupUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreInsert] TO [NetSqlAzMan_Administrators] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StorePermissionDelete] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StorePermissionInsert] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[netsqlazman_StoreUpdate] TO [NetSqlAzMan_Managers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationAttributesView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationGroupMembersView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ApplicationsView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_AuthorizationAttributesView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_AuthorizationView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_BizRuleView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_DatabaseUsers] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ItemAttributesView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_ItemsHierarchyView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StoreAttributesView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[netsqlazman_StoreGroupMembersView] TO [NetSqlAzMan_Readers] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_MembershipUsers] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
