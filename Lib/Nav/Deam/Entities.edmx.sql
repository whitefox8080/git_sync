
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/25/2019 19:06:25
-- Generated from EDMX file: C:\Projects\USSD Navigator\Development\Main\Servers\DEAMServer\Sources\EntityRepository\Schema.mssql\Entities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [USSDNavigator];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_RevisionEntryPoint]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Revisions] DROP CONSTRAINT [FK_RevisionEntryPoint];
GO
IF OBJECT_ID(N'[dbo].[FK_PrjWf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Workflows] DROP CONSTRAINT [FK_PrjWf];
GO
IF OBJECT_ID(N'[dbo].[FK_EntryPointServiceCallNumber]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServiceCallNumbers] DROP CONSTRAINT [FK_EntryPointServiceCallNumber];
GO
IF OBJECT_ID(N'[dbo].[FK_RevisionProject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Revisions] DROP CONSTRAINT [FK_RevisionProject];
GO
IF OBJECT_ID(N'[dbo].[FK_RevisionChangeSet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Revisions] DROP CONSTRAINT [FK_RevisionChangeSet];
GO
IF OBJECT_ID(N'[dbo].[FK_ActiveRevision]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EntryPoints] DROP CONSTRAINT [FK_ActiveRevision];
GO
IF OBJECT_ID(N'[dbo].[FK_TestRevision]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EntryPoints] DROP CONSTRAINT [FK_TestRevision];
GO
IF OBJECT_ID(N'[dbo].[FK_RevisionRevisionStatusTransition]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RevisionStatusTransitions] DROP CONSTRAINT [FK_RevisionRevisionStatusTransition];
GO
IF OBJECT_ID(N'[dbo].[FK_CategoryEntryPointCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EntryPointCategories] DROP CONSTRAINT [FK_CategoryEntryPointCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_EpCat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EntryPointCategories] DROP CONSTRAINT [FK_EpCat];
GO
IF OBJECT_ID(N'[dbo].[FK_Aic]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ApplicationCategories] DROP CONSTRAINT [FK_Aic];
GO
IF OBJECT_ID(N'[dbo].[FK_CategoryApplicationCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ApplicationCategories] DROP CONSTRAINT [FK_CategoryApplicationCategory];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[EntryPoints]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EntryPoints];
GO
IF OBJECT_ID(N'[dbo].[Revisions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Revisions];
GO
IF OBJECT_ID(N'[dbo].[ServiceCallNumbers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServiceCallNumbers];
GO
IF OBJECT_ID(N'[dbo].[ChangeSets]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChangeSets];
GO
IF OBJECT_ID(N'[dbo].[Workflows]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Workflows];
GO
IF OBJECT_ID(N'[dbo].[Projects]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Projects];
GO
IF OBJECT_ID(N'[dbo].[ApplicationInfoes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ApplicationInfoes];
GO
IF OBJECT_ID(N'[dbo].[RevisionStatusTransitions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RevisionStatusTransitions];
GO
IF OBJECT_ID(N'[dbo].[Categories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Categories];
GO
IF OBJECT_ID(N'[dbo].[EntryPointCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EntryPointCategories];
GO
IF OBJECT_ID(N'[dbo].[ApplicationCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ApplicationCategories];
GO
IF OBJECT_ID(N'[dbo].[SystemProperties]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SystemProperties];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'EntryPoints'
CREATE TABLE [dbo].[EntryPoints] (
    [Id] uniqueidentifier  NOT NULL,
    [DateTime] datetime  NOT NULL,
    [ChangeDateTime] datetime  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Description] nvarchar(2000)  NULL,
    [WorkingRevisionId] uniqueidentifier  NULL,
    [TestingRevisionId] uniqueidentifier  NULL,
    [ProjectDeveloperId] nvarchar(max)  NULL,
    [AllowCallProcessing] bit  NOT NULL,
    [StorageServiceType] int  NOT NULL,
    [LazyLoadingProfile] bit  NULL,
    [Restorable] bit  NULL,
    [ResponseTimeout] int  NULL,
    [CallResponseTimeout] int  NULL,
    [Group] nvarchar(256)  NULL
);
GO

-- Creating table 'Revisions'
CREATE TABLE [dbo].[Revisions] (
    [Id] uniqueidentifier  NOT NULL,
    [EntryPointId] uniqueidentifier  NOT NULL,
    [Number] bigint  NOT NULL,
    [ValidationResult] nvarchar(max)  NULL,
    [ChangeSetId] uniqueidentifier  NOT NULL,
    [IntRevisionStatus] int  NOT NULL
);
GO

-- Creating table 'ServiceCallNumbers'
CREATE TABLE [dbo].[ServiceCallNumbers] (
    [Id] uniqueidentifier  NOT NULL,
    [EntryPointId] uniqueidentifier  NULL,
    [Number] nvarchar(15)  NOT NULL,
    [Description] nvarchar(2000)  NULL,
    [DateTime] datetime  NOT NULL,
    [ChangeDateTime] datetime  NOT NULL,
    [AllowCallProcessing] bit  NOT NULL,
    [StorageServiceType] int  NOT NULL
);
GO

-- Creating table 'ChangeSets'
CREATE TABLE [dbo].[ChangeSets] (
    [Id] uniqueidentifier  NOT NULL,
    [Number] bigint  NOT NULL,
    [DateTime] datetime  NOT NULL,
    [Comment] nvarchar(2000)  NULL,
    [PublishedBy] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Workflows'
CREATE TABLE [dbo].[Workflows] (
    [Id] uniqueidentifier  NOT NULL,
    [WorkflowPath] nchar(255)  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [WorflowData] varbinary(max)  NOT NULL,
    [RowId] uniqueidentifier  NOT NULL,
    [Resources] varbinary(max)  NULL,
    [PrjWf_Workflow_RevisionId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Projects'
CREATE TABLE [dbo].[Projects] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [RevisionId] uniqueidentifier  NOT NULL,
    [StartupWorkflowId] uniqueidentifier  NOT NULL,
    [EntryPointName] nvarchar(100)  NOT NULL,
    [ConfigurationPath] nchar(255)  NULL,
    [ConfigurationData] varbinary(max)  NULL,
    [SerializedSupportedLanguages] nvarchar(max)  NULL,
    [ExpressionEditor] nvarchar(128)  NULL
);
GO

-- Creating table 'ApplicationInfoes'
CREATE TABLE [dbo].[ApplicationInfoes] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Comment] nvarchar(256)  NULL,
    [CreateDateTime] datetime  NOT NULL,
    [ChangeDateTime] datetime  NOT NULL,
    [OnlineTestDateTime] datetime  NULL,
    [StoreStatus] int  NOT NULL,
    [StoreContour] int  NOT NULL,
    [IsReserved] bit  NOT NULL,
    [EndpointAddress] nvarchar(128)  NOT NULL,
    [AllowDistribution] bit  NOT NULL,
    [AllowGetFrame] bit  NULL,
    [StoreNotificationMode] int  NULL,
    [DistrTimeUnit] time  NULL,
    [DistrCallsPerTimeUnit] int  NULL,
    [Group] nvarchar(256)  NULL,
    [SettingsData] varbinary(max)  NULL
);
GO

-- Creating table 'RevisionStatusTransitions'
CREATE TABLE [dbo].[RevisionStatusTransitions] (
    [Id] uniqueidentifier  NOT NULL,
    [TransitionDateTime] datetime  NULL,
    [StorageTargetRevisionStatus] int  NOT NULL,
    [StorageInitialRevisionStatus] int  NULL,
    [Description] nvarchar(2000)  NULL,
    [DateTime] datetime  NOT NULL,
    [ChangeDateTime] datetime  NOT NULL,
    [PlannedTransitionDateTime] datetime  NOT NULL,
    [StorageStatus] int  NOT NULL,
    [RevisionId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Categories'
CREATE TABLE [dbo].[Categories] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(64)  NOT NULL,
    [Description] nvarchar(2000)  NULL,
    [StorageServiceType] int  NOT NULL
);
GO

-- Creating table 'EntryPointCategories'
CREATE TABLE [dbo].[EntryPointCategories] (
    [Id] uniqueidentifier  NOT NULL,
    [CategoryId] uniqueidentifier  NOT NULL,
    [EpCat_EntryPointCategory_Id] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'ApplicationCategories'
CREATE TABLE [dbo].[ApplicationCategories] (
    [Id] uniqueidentifier  NOT NULL,
    [CategoryId] uniqueidentifier  NOT NULL,
    [Aic_ApplicationCategory_Id] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'SystemProperties'
CREATE TABLE [dbo].[SystemProperties] (
    [Id] uniqueidentifier  NOT NULL,
    [Code] int  NOT NULL,
    [Value] nvarchar(2000)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'EntryPoints'
ALTER TABLE [dbo].[EntryPoints]
ADD CONSTRAINT [PK_EntryPoints]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Revisions'
ALTER TABLE [dbo].[Revisions]
ADD CONSTRAINT [PK_Revisions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ServiceCallNumbers'
ALTER TABLE [dbo].[ServiceCallNumbers]
ADD CONSTRAINT [PK_ServiceCallNumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChangeSets'
ALTER TABLE [dbo].[ChangeSets]
ADD CONSTRAINT [PK_ChangeSets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [RowId] in table 'Workflows'
ALTER TABLE [dbo].[Workflows]
ADD CONSTRAINT [PK_Workflows]
    PRIMARY KEY CLUSTERED ([RowId] ASC);
GO

-- Creating primary key on [RevisionId] in table 'Projects'
ALTER TABLE [dbo].[Projects]
ADD CONSTRAINT [PK_Projects]
    PRIMARY KEY CLUSTERED ([RevisionId] ASC);
GO

-- Creating primary key on [Id] in table 'ApplicationInfoes'
ALTER TABLE [dbo].[ApplicationInfoes]
ADD CONSTRAINT [PK_ApplicationInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RevisionStatusTransitions'
ALTER TABLE [dbo].[RevisionStatusTransitions]
ADD CONSTRAINT [PK_RevisionStatusTransitions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Categories'
ALTER TABLE [dbo].[Categories]
ADD CONSTRAINT [PK_Categories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EntryPointCategories'
ALTER TABLE [dbo].[EntryPointCategories]
ADD CONSTRAINT [PK_EntryPointCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ApplicationCategories'
ALTER TABLE [dbo].[ApplicationCategories]
ADD CONSTRAINT [PK_ApplicationCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SystemProperties'
ALTER TABLE [dbo].[SystemProperties]
ADD CONSTRAINT [PK_SystemProperties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [EntryPointId] in table 'Revisions'
ALTER TABLE [dbo].[Revisions]
ADD CONSTRAINT [FK_RevisionEntryPoint]
    FOREIGN KEY ([EntryPointId])
    REFERENCES [dbo].[EntryPoints]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RevisionEntryPoint'
CREATE INDEX [IX_FK_RevisionEntryPoint]
ON [dbo].[Revisions]
    ([EntryPointId]);
GO

-- Creating foreign key on [PrjWf_Workflow_RevisionId] in table 'Workflows'
ALTER TABLE [dbo].[Workflows]
ADD CONSTRAINT [FK_PrjWf]
    FOREIGN KEY ([PrjWf_Workflow_RevisionId])
    REFERENCES [dbo].[Projects]
        ([RevisionId])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PrjWf'
CREATE INDEX [IX_FK_PrjWf]
ON [dbo].[Workflows]
    ([PrjWf_Workflow_RevisionId]);
GO

-- Creating foreign key on [EntryPointId] in table 'ServiceCallNumbers'
ALTER TABLE [dbo].[ServiceCallNumbers]
ADD CONSTRAINT [FK_EntryPointServiceCallNumber]
    FOREIGN KEY ([EntryPointId])
    REFERENCES [dbo].[EntryPoints]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EntryPointServiceCallNumber'
CREATE INDEX [IX_FK_EntryPointServiceCallNumber]
ON [dbo].[ServiceCallNumbers]
    ([EntryPointId]);
GO

-- Creating foreign key on [Id] in table 'Revisions'
ALTER TABLE [dbo].[Revisions]
ADD CONSTRAINT [FK_RevisionProject]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Projects]
        ([RevisionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ChangeSetId] in table 'Revisions'
ALTER TABLE [dbo].[Revisions]
ADD CONSTRAINT [FK_RevisionChangeSet]
    FOREIGN KEY ([ChangeSetId])
    REFERENCES [dbo].[ChangeSets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RevisionChangeSet'
CREATE INDEX [IX_FK_RevisionChangeSet]
ON [dbo].[Revisions]
    ([ChangeSetId]);
GO

-- Creating foreign key on [WorkingRevisionId] in table 'EntryPoints'
ALTER TABLE [dbo].[EntryPoints]
ADD CONSTRAINT [FK_ActiveRevision]
    FOREIGN KEY ([WorkingRevisionId])
    REFERENCES [dbo].[Revisions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ActiveRevision'
CREATE INDEX [IX_FK_ActiveRevision]
ON [dbo].[EntryPoints]
    ([WorkingRevisionId]);
GO

-- Creating foreign key on [TestingRevisionId] in table 'EntryPoints'
ALTER TABLE [dbo].[EntryPoints]
ADD CONSTRAINT [FK_TestRevision]
    FOREIGN KEY ([TestingRevisionId])
    REFERENCES [dbo].[Revisions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TestRevision'
CREATE INDEX [IX_FK_TestRevision]
ON [dbo].[EntryPoints]
    ([TestingRevisionId]);
GO

-- Creating foreign key on [RevisionId] in table 'RevisionStatusTransitions'
ALTER TABLE [dbo].[RevisionStatusTransitions]
ADD CONSTRAINT [FK_RevisionRevisionStatusTransition]
    FOREIGN KEY ([RevisionId])
    REFERENCES [dbo].[Revisions]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RevisionRevisionStatusTransition'
CREATE INDEX [IX_FK_RevisionRevisionStatusTransition]
ON [dbo].[RevisionStatusTransitions]
    ([RevisionId]);
GO

-- Creating foreign key on [CategoryId] in table 'EntryPointCategories'
ALTER TABLE [dbo].[EntryPointCategories]
ADD CONSTRAINT [FK_CategoryEntryPointCategory]
    FOREIGN KEY ([CategoryId])
    REFERENCES [dbo].[Categories]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoryEntryPointCategory'
CREATE INDEX [IX_FK_CategoryEntryPointCategory]
ON [dbo].[EntryPointCategories]
    ([CategoryId]);
GO

-- Creating foreign key on [EpCat_EntryPointCategory_Id] in table 'EntryPointCategories'
ALTER TABLE [dbo].[EntryPointCategories]
ADD CONSTRAINT [FK_EpCat]
    FOREIGN KEY ([EpCat_EntryPointCategory_Id])
    REFERENCES [dbo].[EntryPoints]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EpCat'
CREATE INDEX [IX_FK_EpCat]
ON [dbo].[EntryPointCategories]
    ([EpCat_EntryPointCategory_Id]);
GO

-- Creating foreign key on [Aic_ApplicationCategory_Id] in table 'ApplicationCategories'
ALTER TABLE [dbo].[ApplicationCategories]
ADD CONSTRAINT [FK_Aic]
    FOREIGN KEY ([Aic_ApplicationCategory_Id])
    REFERENCES [dbo].[ApplicationInfoes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Aic'
CREATE INDEX [IX_FK_Aic]
ON [dbo].[ApplicationCategories]
    ([Aic_ApplicationCategory_Id]);
GO

-- Creating foreign key on [CategoryId] in table 'ApplicationCategories'
ALTER TABLE [dbo].[ApplicationCategories]
ADD CONSTRAINT [FK_CategoryApplicationCategory]
    FOREIGN KEY ([CategoryId])
    REFERENCES [dbo].[Categories]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoryApplicationCategory'
CREATE INDEX [IX_FK_CategoryApplicationCategory]
ON [dbo].[ApplicationCategories]
    ([CategoryId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------